
==================
WhiteRabbit Device
==================

.. uml:: wr_device_class_diagram.uml

.. automodule:: ska_sat_lmc.whiterabbit.wr_device
   :members:
