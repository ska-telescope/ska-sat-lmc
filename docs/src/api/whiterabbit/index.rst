======================
WhiteRabbit subpackage
======================

.. automodule:: ska_sat_lmc.whiterabbit


.. toctree::

  WhiteRabbit device<wr_device>
  WhiteRabbit health model<wr_health_model>
