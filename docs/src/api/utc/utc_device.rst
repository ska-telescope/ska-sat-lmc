
==========
UTC Device
==========

.. uml:: utc_device_class_diagram.uml

.. automodule:: ska_sat_lmc.utc.utc_device
   :members:
