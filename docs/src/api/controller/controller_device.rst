
=================
Controller Device
=================

.. uml:: controller_device_class_diagram.uml

.. automodule:: ska_sat_lmc.controller.controller_device
   :members:
