=====================
Controller subpackage
=====================

.. automodule:: ska_sat_lmc.controller


.. toctree::

  Controller device<controller_device>
  Controller health model<controller_health_model>
