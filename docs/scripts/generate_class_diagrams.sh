#!/bin/bash

SOURCE_CODE_DIR=../../src/ska_sat_lmc
DOCS_SOURCE_DIR=../src/api

FILES="controller/controller_device.py"

convert () {
	file=$1
	base=`basename "${file}"`
	echo $base 
    uml_file=${base/.py/_class_diagram.uml}
    echo $uml_file
    python3 generate_uml_class_diagrams.py -n -o ${DOCS_SOURCE_DIR}/${uml_file} ${SOURCE_CODE_DIR}/${file}
} 

for f in ${FILES}; do convert $f; done
