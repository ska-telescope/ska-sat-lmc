# ska-sat-lmc

The SKA SAT LMC repository provides a python package supporting the functionality of
all hardwarw device required by the SAT timing system.

## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-sat-lmc/badge/?version=latest)](https://developer.skao.int/projects/ska-sat-lmc/en/latest/?badge=latest)

* [ska-sat-lmc documentation](https://developer.skatelescope.org/projects/ska-sat-lmc/en/latest/index.html "SKA Developer Portal: ska-sat-lmc documentation")