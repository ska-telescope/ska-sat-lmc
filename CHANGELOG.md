# Version History

## Unreleased

## Version 0.4.0

WOM-550: Refactor UTC to use ska-control-model health rollup
WOM-639: added extra attributes

## Version 0.3.0

LOW-1105: Reinstate helm CI steps
LOW-1169 Pull helmfile into shape
WOM-553 Create unit tests for UTC aggregator
WOM-549 Create helm charts for aggregator and controller
LOW-1169 Consistent SNMP auth keys
WOM-544 Add models property to white rabbit
WOM-547 Create clocks and utc simulators 
WOM-582 Fix readthedocs configuration 
WOM-533 restore phasemicrostepper 

## Version 0.2.0

WOM-531 controller update
MCCS-2268 controller
LOW-1104: Enable helm-build in CI
MCCS-2314 Improve HealthState creation
MCCS-2283 Pull telmodel data using Helm for white rabbit and PDU deployment on low
MCCS-2262 Add helm file deployment to WR for JBO, low-itf, AA0.5
MCCS-2185 functional tests
MCCS-2297 redundancy
MCCS-1825 use telmodel
MCCS-2075 without maser functional tests
MCCS-942 Refactor phasemicrostepper code to use ska-tango-base pollling
MCCS-943 Use ska-tango-base polling
MCCS-1715 helm chart
MCCS-1532 Refactor white rabbit
MCCS-1636 Use ska-ser-sphinx-theme for documentation
MCCS-1477 Test White Rabbit Hardware on JBO Server
MCCS-1421 SNMP Tango Device core for White Rabbit

## Version 0.1.3

MCCS-929 Update the sat.lmc chart data files

## Version 0.1.2

MCCS-927 Add Helm dependencies to pipeline

## Version 0.1.1

MCCS-913 Apply the CI pipeline template to SAT.LMC repository
MCCS-722 Refactor the SAT.LMC SatPhaseMicroStepper Tango device
MCCS-724 Refactor the SAT.LMC SatMaster Tango device

## Version 0.1.0

MCCS-717 setting up repository
Initial release
