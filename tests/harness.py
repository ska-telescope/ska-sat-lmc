# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module provides a flexible test harness for testing Tango devices."""

from __future__ import annotations

import unittest.mock
from types import TracebackType
from typing import Any

import tango
from ska_control_model import LoggingLevel
from ska_tango_testing.harness import TangoTestHarness, TangoTestHarnessContext
from tango.server import Device


def get_controller_trl() -> str:
    """
    Return the Controller TRL.

    :return: the Controller TRL
    """
    return "low-sat/ctrl/ctrl"


def get_utc_trl() -> str:
    """
    Return the utc TRL.

    :return: the utc TRL
    """
    return "low-sat/utc/1"


class SatTangoTestHarnessContext:
    """Handle for the Sat test harness context."""

    def __init__(
        self: SatTangoTestHarnessContext, tango_context: TangoTestHarnessContext
    ):
        """
        Initialise a new instance.

        :param tango_context: handle for the underlying test harness
            context.
        """
        self._tango_context = tango_context

    def get_controller_device(self: SatTangoTestHarnessContext) -> tango.DeviceProxy:
        """
        Get a proxy to the controller device.

        :returns: a proxy to the controller Tango device.
        """
        return self._tango_context.get_device(get_controller_trl())

    def get_utc_device(self: SatTangoTestHarnessContext) -> tango.DeviceProxy:
        """
        Get a proxy to the controller device.

        :returns: a proxy to the controller Tango device.
        """
        return self._tango_context.get_device(get_utc_trl())


class SatTangoTestHarness:
    """A test harness for testing monitoring and control of Sat hardware."""

    def __init__(self: SatTangoTestHarness) -> None:
        """Initialise a new test harness instance."""
        self._tango_test_harness = TangoTestHarness()

    def add_controller_device(  # pylint: disable=too-many-arguments
        self: SatTangoTestHarness,
        clock_trl: str,
        frq_trl: str,
        utc_trl: str,
        logging_level: int = int(LoggingLevel.DEBUG),
        device_class: type[Device] | str = "ska_sat_lmc.SatController",
    ) -> None:
        """
        Set the test harness controller device.

        :param clock_trl: TRL of clock
        :param frq_trl: TRL of frq
        :param utc_trl: TRL of utc
        :param logging_level: default level for the device to log at
        :param device_class: class of the device
        """
        kwargs: dict[str, Any] = {"LoggingLevelDefault": logging_level}
        kwargs["SatClockTrl"] = clock_trl
        kwargs["SatFrqTrl"] = frq_trl
        kwargs["SatUtcTrl"] = utc_trl
        self._tango_test_harness.add_device(
            get_controller_trl(),
            device_class,
            **kwargs,
        )

    def add_utc_device(
        self: SatTangoTestHarness,
        whiterabbit_trls: list[str],
        logging_level: int = int(LoggingLevel.DEBUG),
        device_class: type[Device] | str = "ska_sat_lmc.SatUtc",
    ) -> None:
        """
        Add utc to test harness.

        :param whiterabbit_trls: the TRL's of all whiterabbits
        :param logging_level: default level for the device to log at
        :param device_class: class of the device
        """
        kwargs: dict[str, Any] = {"LoggingLevelDefault": logging_level}
        kwargs["SatWhiteRabbitTrls"] = whiterabbit_trls
        self._tango_test_harness.add_device(
            get_utc_trl(),
            device_class,
            **kwargs,
        )

    def add_mock_utc_device(
        self: SatTangoTestHarness,
        mock: unittest.mock.Mock,
    ) -> None:
        """
        Add a mock utc Tango device to this test harness.

        :param mock: the mock to be used as a mock device.
        """
        self._tango_test_harness.add_mock_device(get_utc_trl(), mock)

    def add_clock_device(
        self: SatTangoTestHarness,
        clock_trl: str,
        logging_level: int = int(LoggingLevel.DEBUG),
        device_class: type[Device] | str = "ska_sat_lmc.SatClock",
    ) -> None:
        """
        Add clock to test harness.

        :param clock_trl: TRL of the clock.
        :param logging_level: default level for the device to log at
        :param device_class: class of the device
        """
        self._tango_test_harness.add_device(
            clock_trl,
            device_class,
            LoggingLevelDefault=logging_level,
        )

    def add_mock_clock_device(
        self: SatTangoTestHarness,
        clock_trl: str,
        mock: unittest.mock.Mock,
    ) -> None:
        """
        Add a mock clock Tango device to this test harness.

        :param clock_trl: TRL for the mock clock.
        :param mock: the mock to be used as a mock device.
        """
        self._tango_test_harness.add_mock_device(clock_trl, mock)

    def add_mock_whiterabbit_device(
        self: SatTangoTestHarness,
        whiterabbit_trl: str,
        mock: unittest.mock.Mock,
    ) -> None:
        """
        Add a mock whiterabbit Tango device to this test harness.

        :param whiterabbit_trl: TRL for the mock whiterabbit.


        :param mock: the mock to be used as a mock whiterabbit device.
        """
        self._tango_test_harness.add_mock_device(whiterabbit_trl, mock)

    def __enter__(
        self: SatTangoTestHarness,
    ) -> SatTangoTestHarnessContext:
        """
        Enter the context.

        :return: the entered context.
        """
        return SatTangoTestHarnessContext(self._tango_test_harness.__enter__())

    def __exit__(
        self: SatTangoTestHarness,
        exc_type: type[BaseException] | None,
        exception: BaseException | None,
        trace: TracebackType | None,
    ) -> bool | None:
        """
        Exit the context.

        :param exc_type: the type of exception thrown in the with block,
            if any.
        :param exception: the exception thrown in the with block, if
            any.
        :param trace: the exception traceback, if any,

        :return: whether the exception (if any) has been fully handled
            by this method and should be swallowed i.e. not re-
            raised
        """
        return self._tango_test_harness.__exit__(exc_type, exception, trace)
