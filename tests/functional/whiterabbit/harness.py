# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module provides a flexible test harness for testing SAT Tango devices."""
from __future__ import annotations

from types import TracebackType

import tango
from ska_control_model import LoggingLevel
from ska_tango_testing.harness import TangoTestHarness, TangoTestHarnessContext
from tango.server import Device

DEFAULT_DEVICE_LABEL = "1"  # device 1


def get_white_rabbit_name(device_label: str | None = None) -> str:
    """
    Return the White Rabbit Tango device name.

    :param device_label: name of the device under test.
        Defaults to None, in which case the module default is used.

    :return: the White Rabbit Tango device name
    """
    return f"low-sat/wrswitch/{device_label or DEFAULT_DEVICE_LABEL}"


class SatTangoTestHarnessContext:
    """Handle for the White Rabbit test harness context."""

    def __init__(
        self: SatTangoTestHarnessContext,
        tango_context: TangoTestHarnessContext,
        device_label: str,
    ) -> None:
        """
        Initialise a new instance.

        :param tango_context: handle for the underlying test harness
            context.
        :param device_label: name of the device under test.
        """
        self._device_label = device_label
        self._tango_context = tango_context

    def get_white_rabbit_device(
        self: SatTangoTestHarnessContext,
    ) -> tango.DeviceProxy:
        """
        Get a proxy to the White Rabbit Tango device.

        :returns: a proxy to the White Rabbit Tango device.
        """
        return self._tango_context.get_device(get_white_rabbit_name(self._device_label))


class SatTangoTestHarness:
    """A test harness for testing monitoring and control of SAT hardware."""

    def __init__(self: SatTangoTestHarness, device_label: str | None = None) -> None:
        """
        Initialise a new test harness instance.

        :param device_label: name of the device under test.
            Defaults to None, in which case "1" is used.
        """
        self._device_label = device_label or DEFAULT_DEVICE_LABEL
        self._tango_test_harness = TangoTestHarness()

    def add_white_rabbit_device(  # pylint: disable=too-many-arguments
        self: SatTangoTestHarness,
        model: str,
        host: str,
        port: int,
        community_name: str,
        refresh_rate: float = 3.0,
        logging_level: int = int(LoggingLevel.DEBUG),
        device_class: type[Device] | str = "ska_sat_lmc.SatWhiteRabbit",
    ) -> None:
        """
        Add the White Rabbit Tango device to the test harness.

        This test harness currently only permits one White Rabbit device.

        :param model: white rabbit model
        :param host: the hostname of the White Rabbit
        :param port: the port of the White Rabbit
        :param community_name: SNMP V2 community name
        :param refresh_rate: update rate for SNMP polling
        :param logging_level: the Tango device's default logging level.
        :param device_class: The device class to use.
            This may be used to override the usual device class,
            for example with a patched subclass.
        """
        self._tango_test_harness.add_device(
            get_white_rabbit_name(self._device_label),
            device_class,
            Host=host,
            Port=port,
            RefreshRate=refresh_rate,
            Model=model,
            V2Community=community_name,
            LoggingLevelDefault=logging_level,
        )

    def add_white_rabbit_simulator(
        self: SatTangoTestHarness,
        data_directory: str | None,
        simulator_user: str | None,
        device_class: type[Device] | str = "ska_sat_lmc.WhiteRabbitSimulator",
    ) -> None:
        """
        And a white rabbit simulator to the test harness.

        :param device_class: the simulator to be added to the harness.
        :param data_directory: directory where simuations are stored
        :param simulator_user: user+goup for user.
        """
        from ska_sat_lmc.whiterabbit.simulator import WhiteRabbitSimulator  # noqa: F401

        self._tango_test_harness.add_device(
            "low-sat/wrsimulator/1",
            device_class,
            DataDir=data_directory,
            SimUser=simulator_user,
        )

    def __enter__(self: SatTangoTestHarness) -> SatTangoTestHarnessContext:
        """
        Enter the context.

        :return: the entered context.DEVICE
        """
        return SatTangoTestHarnessContext(
            self._tango_test_harness.__enter__(), self._device_label
        )

    def __exit__(
        self: SatTangoTestHarness,
        exc_type: type[BaseException] | None,
        exception: BaseException | None,
        trace: TracebackType | None,
    ) -> bool | None:
        """
        Exit the context.

        :param exc_type: the type of exception thrown in the with block,
            if any.
        :param exception: the exception thrown in the with block, if
            any.
        :param trace: the exception traceback, if any,

        :return: whether the exception (if any) has been fully handled
            by this method and should be swallowed i.e. not re-
            raised
        """
        return self._tango_test_harness.__exit__(exc_type, exception, trace)
