# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module contains a simple tests of the SAT WhiteRabitNode Tango device."""
from __future__ import annotations

import time

import tango
from pytest_bdd import given, scenario, then, when
from ska_control_model import AdminMode, HealthState


@scenario(
    "features/wr_monitor.feature",
    "Monitor WhiteRabbit Switch",
)
def test_wr_monitor() -> None:
    """
    Run a basic test scenario.

    Any code in this scenario method is run at the *end* of the
    scenario.
    """


@given("a SatWhiteRabbit Tango device")
def check_white_rabbit_device(
    white_rabbit_device: tango.DeviceProxy,
) -> None:
    """
    Check that the white rabbit device is responsive.

    :param white_rabbit_device: proxy to the white rabbit device.
    """
    white_rabbit_device.ping()


@given("the SatWhiteRabbit is in the DISABLE state")
def check_white_rabbit_is_disabled(
    white_rabbit_device: tango.DeviceProxy,
) -> None:
    """
    Check that the white rabbit device is in disable state.

    :param white_rabbit_device: proxy to the white rabbit device.
    """
    if white_rabbit_device.adminMode != AdminMode.OFFLINE:
        white_rabbit_device.adminMode = AdminMode.OFFLINE
    if white_rabbit_device.state() != tango.DevState.DISABLE:
        white_rabbit_device.set_state(tango.DevState.DISABLE)


@given("the SatWhiteRabbit is in health state UNKNOWN")
def check_white_rabbit_is_unknown_health(
    white_rabbit_device: tango.DeviceProxy,
) -> None:
    """
    Check that white rabbit is in health mode unknown.

    :param white_rabbit_device: proxy to the white rabbit device.
    """
    assert white_rabbit_device.healthState == HealthState.UNKNOWN


@given("the SatWhiteRabbit is in adminMode OFFLINE")
def check_white_rabbit_is_in_admin_mode_offline(
    white_rabbit_device: tango.DeviceProxy,
) -> None:
    """
    Assert that white rabbit is in admin mode OFFLINE.

    :param white_rabbit_device: proxy to the white rabbit device.
    """
    assert white_rabbit_device.adminMode == AdminMode.OFFLINE


@when("I set adminMode to ONLINE")
def set_admin_mode_online(
    white_rabbit_device: tango.DeviceProxy,
) -> None:
    """
    Set this white_rabbit_device's adminMode to ONLINE.

    :param white_rabbit_device: proxy to the white rabbit device.
    """
    white_rabbit_device.adminMode = AdminMode.ONLINE
    assert white_rabbit_device.adminMode == AdminMode.ONLINE


@then("the SatWhiteRabbit is in the ON state")
def check_white_rabbit_is_on(
    white_rabbit_device: tango.DeviceProxy,
) -> None:
    """
    Check that the white rabbit is ON.

    :param white_rabbit_device: proxy to the white rabbit device.
    """
    for i in range(10):
        time.sleep(1)
    assert white_rabbit_device.State() == tango.DevState.ON


@then("the SatWhiteRabbit is in health state OK")
def check_white_rabbit_health_is_ok(
    white_rabbit_device: tango.DeviceProxy,
) -> None:
    """
    Check that the white rabbit is healthy.

    :param white_rabbit_device: proxy to the white rabbit device.
    """
    is_healthy = False
    for i in range(20):
        if white_rabbit_device.healthState == HealthState.OK:
            is_healthy = True
            break
        time.sleep(1)
    assert is_healthy
