# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT LMC project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains pytest-specific test harness for SAT functional tests."""

import os
from typing import Iterator

import pytest
import tango

from tests.functional.whiterabbit.harness import (
    SatTangoTestHarness,
    SatTangoTestHarnessContext,
)


@pytest.fixture(name="true_context", scope="session")
def true_context_fixture(request: pytest.FixtureRequest) -> bool:
    """
    Return whether to test against an existing Tango deployment.

    If True, then Tango is already deployed, and the tests will be run
    against that deployment.

    If False, then Tango is not deployed, so the test harness will stand
    up a test context and run the tests against that.

    :param request: A pytest object giving access to the requesting test
        context.

    :return: whether to test against an existing Tango deployment
    """
    if request.config.getoption("--true-context"):
        return True
    if os.getenv("TRUE_TANGO_CONTEXT", None):
        return True
    return False


@pytest.fixture(name="simulator_user", scope="session")
def simulator_user_fixture(request: pytest.FixtureRequest) -> bool:
    """
    Return the simulator user.

    :param request: A pytest object giving access to the requesting test
        context.

    :return: the simulator user and group
    """
    return request.config.getoption("--sim-user")


@pytest.fixture(name="data_directory", scope="session")
def data_directory_fixture(request: pytest.FixtureRequest) -> str:
    """
    Return the data directory.

    :param request: A pytest object giving access to the requesting test
        context.

    :return: the data directory
    """
    return "tests/functional/whiterabbit/snmpsim_data/switch"


@pytest.fixture(name="white_rabbit_url", scope="module")
def white_rabbit_url_fixture() -> tuple[str, int] | None:
    """
    Return the White Rabbit URL.

    If a real hardware White Rabbit is present,
    or there is a pre-existing simulator,
    then this fixture returns its URL e.g. 10.9.99.165:161.
    If there is no pre-existing White Rabbit or simulator,
    then this fixture returns None, indicating that the test harness
    should stand up a White Rabbit simulator server itself
    (but that's not implemented yet).

    :return: the URL of the White Rabbit,
        or None if no White Rabbit (genuine or simulated) is available.
    """
    url_var = "WHITE_RABBIT_URL"
    if url_var in os.environ:
        [host, port_str] = os.environ[url_var].split(":")
        return host, int(port_str)
    return None


@pytest.fixture(name="white_rabbit_model", scope="module")
def white_rabbit_model_fixture() -> str | None:
    """
    Return the model name for the White Rabbit.

    :return: the device model name ,
    """
    # return os.environ.get("WHITE_RABBIT_MODEL")
    return "SWITCH"


@pytest.fixture(name="white_rabbit_v2community", scope="module")
def white_rabbit_v2community_fixture() -> str | None:
    """
    Return the SNMP V2 community name for authenticating with the White Rabbit.

    :return: the community name for authenticating with the White Rabbit,
        or None if no White Rabbit (genuine or simulated) is available.
    """
    # return os.environ.get("WHITE_RABBIT_V2COMMUNITY")
    return "switch"


@pytest.fixture(name="device_label", scope="session")
def device_label_fixture() -> str | None:
    """
    Return the name of the device under test.

    :return: the name of the station under test.
    """
    return os.environ.get("DEVICE_LABEL")


@pytest.fixture(name="functional_test_context", scope="module")
def functional_test_context_fixture(
    true_context: bool,
    device_label: str,
    white_rabbit_model: str,
    white_rabbit_url: tuple[str, int],
    white_rabbit_v2community: str,
    data_directory: str,
    simulator_user: str,
) -> Iterator[SatTangoTestHarnessContext]:
    """
    Yield a Tango context containing the device/s under test.

    :param true_context: whether to test against an existing Tango deployment
    :param device_label: name of the device under test.
    :param white_rabbit_model: the white rabbit model
    :param white_rabbit_url: URL of the White Rabbit, if already present
    :param white_rabbit_v2community: SNMP V2 community name to use when authenticating
    :param data_directory: directory where simuations are stored
    :param simulator_user: user+goup for user.

    :yields: a Tango context containing the devices under test
    """
    harness = SatTangoTestHarness(device_label)

    if not true_context:
        if white_rabbit_url is None:
            harness.add_white_rabbit_simulator(
                data_directory,
                simulator_user,
            )
            harness.add_white_rabbit_device(
                white_rabbit_model,
                "127.0.0.1",
                1610,
                white_rabbit_v2community,
            )

    with harness as context:
        yield context


@pytest.fixture(name="white_rabbit_device", scope="module")
def white_rabbit_device_fixture(
    functional_test_context: SatTangoTestHarnessContext,
) -> Iterator[tango.DeviceProxy]:
    """
    Return a DeviceProxy to an instance of SatWhiteRabbit.

    :param functional_test_context: context in which the functional tests run.

    :yields: A proxy to an instance of SatWhiteRabbit.
    """
    yield functional_test_context.get_white_rabbit_device()
