# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.

Feature: Monitoring and control of WhiteRabbit
    Test that SAT subsystem can monitor a whiterabbit switch system.

    Scenario: Monitor WhiteRabbit Switch
        Given a SatWhiteRabbit Tango device
        And the SatWhiteRabbit is in the DISABLE state
        And the SatWhiteRabbit is in health state UNKNOWN
        And the SatWhiteRabbit is in adminMode OFFLINE
        When I set adminMode to ONLINE
        Then the SatWhiteRabbit is in the ON state
        And the SatWhiteRabbit is in health state OK

