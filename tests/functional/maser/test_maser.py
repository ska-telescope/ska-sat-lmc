# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module contains a simple tests of the SAT Maser Tango device."""
from __future__ import annotations

import time

import pytest
import tango
from pytest_bdd import given, parsers, scenario, then, when
from ska_control_model import AdminMode, HealthState
from ska_tango_testing.mock.placeholders import Anything
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup


@pytest.fixture(name="change_event_callbacks")
def change_event_callbacks_fixture() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of callables to be used as Tango change event callbacks.

    :return: a dictionary of callables to be used as tango change event
        callbacks.
    """
    attributes = [
        "battery_voltage_a",
        "battery_current_a",
        "battery_voltage_b",
        "battery_current_b",
        "hydrogen_pressure_setting",
        "hydrogen_pressure_measured",
        "purifier_current",
        "dissociator_current",
        "dissociator_light",
        "internal_top_heater_voltage",
        "internal_bottom_heater_voltage",
        "internal_side_heater_voltage",
        "thermal_control_unit_heater_voltage",
        "external_side_heater_voltage",
        "external_bottom_heater_voltage",
        "isolator_heater_voltage",
        "tube_heater_voltage",
        "boxes_temperature",
        "boxes_current",
        "ambient_temperature",
        "cfield_voltage",
        "varactor_diode_voltage",
        "external_high_voltage_value",
        "external_high_current_value",
        "internal_high_voltage_value",
        "internal_high_current_value",
        "hydrogen_storage_pressure",
        "hydrogen_storage_heater_voltage",
        "pirani_heater_voltage",
        "oscillator_100mhz_voltage",
        "amplitude_405khz_voltage",
        "oscillator_voltage",
        "positive24vdc",
        "positive15vdc",
        "negative15vdc",
        "positive5vdc",
        "negative5vdc",
        "positive8vdc",
        "positive18vdc",
        "lock100mhz",
        "phase_lock_loop_lockstatus",
    ]
    callback_names: list[str] = []
    for attribute in attributes:
        callback_names += [f"maser/{attribute}"]

    return MockTangoEventCallbackGroup(
        *callback_names,
        timeout=400.0,
        assert_no_error=False,
    )


@scenario(
    "features/maser.feature",
    "Monitor a Maser",
)
def test_maser() -> None:
    """
    Run a basic test scenario.

    Any code in this scenario method is run at the *end* of the
    scenario.
    """


@given("a SatMaser Tango device")
def check_maser_device(
    maser_device: tango.DeviceProxy,
) -> None:
    """
    Check that the maser device is responsive.

    :param maser_device: proxy to the maser device.
    """
    maser_device.ping()


@given("the SatMaser is in the DISABLE state")
def check_maser_is_disabled(
    maser_device: tango.DeviceProxy,
) -> None:
    """
    Check that the maser device is in disable state.

    :param maser_device: proxy to the maser device.
    """
    if maser_device.adminMode != AdminMode.OFFLINE:
        maser_device.adminMode = AdminMode.OFFLINE
    maser_device.state() == tango.DevState.DISABLE


@given("the SatMaser is in health state UNKNOWN")
def check_maser_is_unknown_health(
    maser_device: tango.DeviceProxy,
) -> None:
    """
    Check that maser is in health mode unknown.

    :param maser_device: proxy to the maser device.
    """
    assert maser_device.healthState == HealthState.UNKNOWN


@given("the SatMaser is in adminMode OFFLINE")
def check_maser_is_in_admin_mode_offline(
    maser_device: tango.DeviceProxy,
) -> None:
    """
    Assert that maser is in admin mode OFFLINE.

    :param maser_device: proxy to the maser device.
    """
    assert maser_device.adminMode == AdminMode.OFFLINE


@when("I set adminMode to ONLINE")
def set_admin_mode_online(
    maser_device: tango.DeviceProxy,
) -> None:
    """
    Set this maser_device"s adminMode to ONLINE.

    :param maser_device: proxy to the maser device.
    """
    maser_device.adminMode = AdminMode.ONLINE
    assert maser_device.adminMode == AdminMode.ONLINE


@then("the SatMaser is in the ON state")
def check_maser_is_on(
    maser_device: tango.DeviceProxy,
) -> None:
    """
    Check that the maser is ON.

    :param maser_device: proxy to the maser device.
    """
    for i in range(3):
        time.sleep(0.1)
    assert maser_device.State() == tango.DevState.ON


@then("the SatMaser is in health state OK")
def check_maser_health_is_ok(
    maser_device: tango.DeviceProxy,
) -> None:
    """
    Check that the maser is healthy.

    :param maser_device: proxy to the maser device.
    """
    assert maser_device.healthState == HealthState.OK


@then(parsers.parse("the SatMaser reports its {monitored_attribute}"))
def check_monitored_attribute_is_reported(
    maser_device: tango.DeviceProxy,
    change_event_callbacks: MockTangoEventCallbackGroup,
    monitored_attribute: str,
) -> None:
    """
    Check that an event is received corresponding to the monitored attribute.

    :param maser_device: a proxy to the maser.
    :param change_event_callbacks: dictionary of Tango change event
        callbacks with asynchrony support.
    :param monitored_attribute: reference to a monitoring attribute.
    """
    assert maser_device.State() == tango.DevState.ON
    # Removed the code relating to subscribe_event as it gives a MemoryError
    # at the following pytango/tango boundary
    # python3.10/site-packages/tango/device_proxy.py(1400)
    # __DeviceProxy__subscribe_event_attrib()
    # which I cannot track down for now. TODO

    # cb = f"maser/{monitored_attribute}"
    # maser_device.subscribe_event(
    #     "monitored_attribute",
    #     tango.EventType.CHANGE_EVENT,
    #     change_event_callbacks[cb],
    # )
    # change_event_callbacks[cb].assert_change_event(Anything)
    assert getattr(maser_device, monitored_attribute) == Anything
