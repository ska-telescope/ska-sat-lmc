# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.

Feature: Monitoring and control of a maser
    Test that SAT subsystem can monitor a maser.

    Scenario: Monitor a Maser
        Given a SatMaser Tango device
        And the SatMaser is in the DISABLE state
        And the SatMaser is in health state UNKNOWN
        And the SatMaser is in adminMode OFFLINE
        When I set adminMode to ONLINE
        Then the SatMaser is in health state OK
        And the SatMaser is in the ON state
        And the SatMaser reports its battery_voltage_a
        And the SatMaser reports its battery_current_a
        And the SatMaser reports its battery_voltage_b
        And the SatMaser reports its battery_current_b
        And the SatMaser reports its hydrogen_pressure_setting
        And the SatMaser reports its hydrogen_pressure_measured
        And the SatMaser reports its purifier_current
        And the SatMaser reports its dissociator_current
        And the SatMaser reports its dissociator_light
        And the SatMaser reports its internal_top_heater_voltage
        And the SatMaser reports its internal_bottom_heater_voltage
        And the SatMaser reports its internal_side_heater_voltage
        And the SatMaser reports its thermal_control_unit_heater_voltage
        And the SatMaser reports its external_side_heater_voltage
        And the SatMaser reports its external_bottom_heater_voltage
        And the SatMaser reports its isolator_heater_voltage
        And the SatMaser reports its tube_heater_voltage
        And the SatMaser reports its boxes_temperature
        And the SatMaser reports its boxes_current
        And the SatMaser reports its ambient_temperature
        And the SatMaser reports its cfield_voltage
        And the SatMaser reports its varactor_diode_voltage
        And the SatMaser reports its external_high_voltage_value
        And the SatMaser reports its external_high_current_value
        And the SatMaser reports its internal_high_voltage_value
        And the SatMaser reports its internal_high_current_value
        And the SatMaser reports its hydrogen_storage_pressure
        And the SatMaser reports its hydrogen_storage_heater_voltage
        And the SatMaser reports its pirani_heater_voltage
        And the SatMaser reports its oscillator_100mhz_voltage
        And the SatMaser reports its amplitude_405khz_voltage
        And the SatMaser reports its oscillator_voltage
        And the SatMaser reports its positive24vdc
        And the SatMaser reports its positive15vdc
        And the SatMaser reports its negative15vdc
        And the SatMaser reports its positive5vdc
        And the SatMaser reports its negative5vdc 
        And the SatMaser reports its positive8vdc
        And the SatMaser reports its positive18vdc
        And the SatMaser reports its lock100mhz
        And the SatMaser reports its phase_lock_loop_lockstatus
