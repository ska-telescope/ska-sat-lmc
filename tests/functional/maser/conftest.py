# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT LMC project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains pytest-specific test harness for SAT functional tests."""
import os
from typing import Iterator

import pytest
import tango

from tests.functional.maser.harness import (
    SatTangoTestHarness,
    SatTangoTestHarnessContext,
)


@pytest.fixture(name="true_context", scope="session")
def true_context_fixture(request: pytest.FixtureRequest) -> bool:
    """
    Return whether to test against an existing Tango deployment.

    If True, then Tango is already deployed, and the tests will be run
    against that deployment.

    If False, then Tango is not deployed, so the test harness will stand
    up a test context and run the tests against that.

    :param request: A pytest object giving access to the requesting test
        context.

    :return: whether to test against an existing Tango deployment
    """
    if request.config.getoption("--true-context"):
        return True
    if os.getenv("TRUE_TANGO_CONTEXT", None):
        return True
    return False


@pytest.fixture(name="maser_host", scope="module")
def maser_host_fixture() -> str | None:
    """
    Return the maser host from a url.

    :return: the host ip of the maser,
        or None if no maser (genuine or simulated) is available.
    """
    url_var = "MASER_URL"
    if url_var in os.environ:
        (host,) = os.environ[url_var].split(":")
        return host
    return None


@pytest.fixture(name="maser_port", scope="module")
def maser_port_fixture() -> int | None:
    """
    Return the maser port from a url.

    :return: the port of the maser,
        or None if no maser (genuine or simulated) is available.
    """
    url_var = "MASER_URL"
    if url_var in os.environ:
        host, port_str = os.environ[url_var].split(":")
        return int(port_str)
    return None


@pytest.fixture(name="device_label", scope="session")
def device_label_fixture() -> str | None:
    """
    Return the name of the device under test.

    :return: the name of the station under test.
    """
    return os.environ.get("DEVICE_LABEL")


@pytest.fixture(name="functional_test_context", scope="module")
def functional_test_context_fixture(
    true_context: bool,
    device_label: str,
    maser_host: str,
    maser_port: int,
) -> Iterator[SatTangoTestHarnessContext]:
    """
    Yield a Tango context containing the device/s under test.

    :param true_context: whether to test against an existing Tango deployment
    :param device_label: name of the device under test.
    :param maser_host: hostname of the maser, if already present
    :param maser_port: port of the maser, if already present

    :yields: a Tango context containing the devices under test
    """
    harness = SatTangoTestHarness(device_label)

    if not true_context:
        if maser_host is None:
            harness.add_maser_simulator(
                "127.0.0.1",
                14000,
            )
            harness.add_maser_device(
                "127.0.0.1",
                14000,
            )
        else:
            harness.add_maser_device(
                maser_host,
                maser_port,
            )

    with harness as context:
        yield context


@pytest.fixture(name="maser_device", scope="module")
def maser_device_fixture(
    functional_test_context: SatTangoTestHarnessContext,
) -> Iterator[tango.DeviceProxy]:
    """
    Return a DeviceProxy to an instance of SatMaser.

    :param functional_test_context: context in which the functional tests run.

    :yields: A proxy to an instance of SatMaser.
    """
    yield functional_test_context.get_maser_device()
