# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module contains the tests for SatWhiteRabbit."""

from __future__ import annotations

import time

import pytest
import tango
from ska_control_model import (
    AdminMode,
    ControlMode,
    HealthState,
    SimulationMode,
    TestMode,
)
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import DevState, EventType

from ska_sat_lmc.whiterabbit.wr_device import SatWhiteRabbit


@pytest.fixture(name="change_event_callbacks")
def change_event_callbacks_fixture() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of callables to be used as Tango change event callbacks.

    :return: a dictionary of callables to be used as tango change event
        callbacks.
    """
    return MockTangoEventCallbackGroup(
        "admin_mode",
        "health_state",
        "state",
        "system_status",
        timeout=3.0,
    )


@pytest.fixture(name="whiterabbit_name", scope="session")
def whiterabbit_name_fixture() -> str:
    """
    Return the name of the whiterabbit Tango device.

    :return: the name of the whiterabbit Tango device.
    """
    return "sat-lmc/wrswitch/1"


def test_init_device(
    whiterabbit_device: tango.DeviceProxy,
) -> None:
    """
    Test for Initial state.

    :param whiterabbit_device: fixture that provides a
        :py:class:`tango.DeviceProxy` to the device under test, in a
        :py:class:`tango.test_context.DeviceTestContext`.
    """
    assert whiterabbit_device.State() == DevState.DISABLE
    assert whiterabbit_device.status() == "The device is in DISABLE state."
    assert whiterabbit_device.adminMode == AdminMode.OFFLINE
    assert whiterabbit_device.healthState == HealthState.UNKNOWN
    assert whiterabbit_device.controlMode == ControlMode.REMOTE
    assert whiterabbit_device.simulationMode == SimulationMode.FALSE
    assert whiterabbit_device.testMode == TestMode.NONE


def test_attribute(
    whiterabbit_device: SatWhiteRabbit,
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Test for Attributes.

    :param whiterabbit_device: fixture that provides a
        :py:class:`tango.DeviceProxy` to the device under test, in a
        :py:class:`tango.test_context.DeviceTestContext`.
    :param change_event_callbacks: list of callbacks
    """
    whiterabbit_device.subscribe_event(
        "state",
        EventType.CHANGE_EVENT,
        change_event_callbacks["state"],
    )
    whiterabbit_device.subscribe_event(
        "healthState",
        EventType.CHANGE_EVENT,
        change_event_callbacks["health_state"],
    )
    whiterabbit_device.subscribe_event(
        "adminMode",
        EventType.CHANGE_EVENT,
        change_event_callbacks["admin_mode"],
    )

    change_event_callbacks["state"].assert_change_event(DevState.DISABLE)
    change_event_callbacks["health_state"].assert_change_event(HealthState.UNKNOWN)
    change_event_callbacks["admin_mode"].assert_change_event(AdminMode.OFFLINE)

    whiterabbit_device.adminMode = AdminMode.ONLINE  # type: ignore
    assert whiterabbit_device.adminMode == AdminMode.ONLINE
    change_event_callbacks["admin_mode"].assert_change_event(
        AdminMode.ONLINE, lookahead=4
    )
    is_on = False
    for i in range(20):
        if whiterabbit_device.State() == DevState.ON:
            is_on = True
            break
        time.sleep(1)
    assert is_on
    change_event_callbacks["state"].assert_change_event(DevState.ON, lookahead=4)
    assert whiterabbit_device.healthState == HealthState.OK
    change_event_callbacks["health_state"].assert_change_event(
        HealthState.OK, lookahead=4
    )

    # These should not change
    assert whiterabbit_device.controlMode == ControlMode.REMOTE
    assert whiterabbit_device.simulationMode == SimulationMode.FALSE
    assert whiterabbit_device.testMode == TestMode.NONE
    assert whiterabbit_device.wrsTempFPGA == 47
