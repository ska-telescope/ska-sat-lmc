# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.
"""This subpackage contains unit tests of the ska-sat-lmc.whiterabbit."""
