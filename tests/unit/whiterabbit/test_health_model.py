# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module contains the tests for SatWhiteRabbit."""

from __future__ import annotations

import logging
from typing import Any

import pytest
from ska_control_model import HealthState

from ska_sat_lmc.testing.mock import MockCallable
from ska_sat_lmc.whiterabbit.wr_health_model import WhiteRabbitHealthModel, WRState


class TestHealthModel:
    """A class for tests of the health model reporting."""

    @pytest.fixture
    def health_model(
        self: TestHealthModel, logger: logging.Logger
    ) -> WhiteRabbitHealthModel:
        """
        Fixture to return the health model.

        :param logger: the logger

        :return: Health model to be used.
        """
        health_model = WhiteRabbitHealthModel(MockCallable(), logger)
        health_model.update_state(communicating=False)
        return health_model

    @pytest.mark.parametrize(
        ("data", "expected_health", "expected_report"),
        [
            pytest.param(
                {
                    "general_status": WRState.OK,
                    "timing_status": WRState.OK,
                    "system_status": WRState.OK,
                },
                HealthState.OK,
                "Health is OK",
            ),
            pytest.param(
                {
                    "general_status": WRState.OK,
                    "timing_status": WRState.OK,
                    "system_status": WRState.WARNING,
                },
                HealthState.DEGRADED,
                "system_status reports warning",
            ),
            pytest.param(
                {
                    "general_status": WRState.OK,
                    "timing_status": WRState.WARNING,
                    "system_status": WRState.CRITICAL,
                },
                HealthState.FAILED,
                "timing_status reports warning, system_status reports critical",
            ),
            pytest.param(
                {
                    "general_status": WRState.CRITICAL,
                    "timing_status": WRState.OK,
                    "system_status": WRState.CRITICAL,
                },
                HealthState.FAILED,
                "general_status reports critical, system_status reports critical",
            ),
            pytest.param(
                {
                    "general_status": WRState.WARNING,
                    "timing_status": WRState.WARNING,
                    "system_status": WRState.WARNING,
                },
                HealthState.DEGRADED,
                (
                    "general_status reports warning, timing_status reports warning, "
                    "system_status reports warning"
                ),
            ),
        ],
    )
    def test_health_report(
        self: TestHealthModel,
        health_model: WhiteRabbitHealthModel,
        data: dict[str, Any],
        expected_health: HealthState,
        expected_report: str,
    ) -> None:
        """
        Tests for evaluating whiterabbit health.

        :param health_model: Health model fixture.
        :param data: Health data values for health model.
        :param expected_health: Expected health.
        :param expected_report: report of failed or degraded attributes
        """
        health, report = health_model.evaluate_health()
        assert health == HealthState.UNKNOWN
        assert report == "Device has not connected yet"
        health_model.update_state(communicating=True)
        health, report = health_model.evaluate_health()
        assert health == HealthState.OK
        assert report == "Health is OK"
        health_model.update_state(**data)
        health, report = health_model.evaluate_health()
        assert health == expected_health
        assert report == expected_report
        health_model.update_state(communicating=False)
        health, report = health_model.evaluate_health()
        assert health == HealthState.FAILED
        assert report == "Device has disconnected"
