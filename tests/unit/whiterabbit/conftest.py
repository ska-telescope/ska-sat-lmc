# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module defines a pytest harness for testing the SAT.LMC controller module."""

import os
import signal
import subprocess
from typing import Any, Generator

import pytest
from tango import DeviceProxy
from tango.test_context import DeviceTestContext

from ska_sat_lmc.whiterabbit.wr_device import SatWhiteRabbit


@pytest.fixture(scope="session")
def simulator() -> Generator:
    """
    Create a simulator for white rabbit unit testing.

    :yields: host & port else None
    :raises RuntimeError: Simulator failed"
    """
    if int(os.getenv("SKA_SNMP_DEVICE_SIMULATOR", "1").strip()):
        sim_user = os.getenv("SKA_SNMP_DEVICE_SIMULATOR_USER", "").strip()
        if sim_user:
            user, group = sim_user.split(":")
            user_args = [f"--process-user={user}", f"--process-group={group}"]
        else:
            user_args = []
        host, port = "127.0.0.1", 1610
        sim_process: Any = subprocess.Popen(  # Any because mypy seems to hate Popen
            [
                "snmpsim-command-responder",
                *user_args,
                "--data-dir=tests/unit/whiterabbit/snmpsim_data/switch",
                f"--agent-udpv4-endpoint={host}:{port}",
            ],
            encoding="utf-8",
            stderr=subprocess.PIPE,
        )
        try:
            while sim_process.poll() is None:
                line = sim_process.stderr.readline()
                if line.startswith(f"  Listening at UDP/IPv4 endpoint {host}:{port}"):
                    yield host, port
                    break
            else:
                cmd = " ".join(sim_process.args)
                return_code = sim_process.returncode
                raise RuntimeError(
                    f'Simulator command "{cmd}" exited with code {return_code}'
                    " without ever listening"
                )
        finally:
            sim_process.send_signal(signal.SIGTERM)
            sim_process.wait()
    else:
        yield None


@pytest.fixture(name="model")
def model_fixture() -> str:
    """
    Specify the model name.

    :return: the model name
    """
    return "SWITCH"


@pytest.fixture(name="endpoint")
def endpoint_fixture(simulator: tuple[str, int]) -> tuple[str, int]:
    """
    Define an endpoint.

    :param simulator: the simulator

    :return: the host & port
    """
    if simulator:
        return simulator
    else:
        host, port = os.getenv("SKA_SNMP_DEVICE_TEST_ENDPOINT").strip().split(":")
        return host, int(port)


@pytest.fixture(name="whiterabbit_device")
def whiterabbit_device_fixture(model: str, endpoint: tuple[str, int]) -> DeviceProxy:
    """
    Create a tango device context for the whiterabbit.

    :param model: white rabbit model
    :param endpoint: host & port

    :yields: the white rabbit device context
    """
    # simulator()
    host, port = endpoint
    ctx = DeviceTestContext(
        SatWhiteRabbit,
        properties=dict(
            Model=model,
            Host=host,
            Port=port,
            V2Community="switch",
            LoggingLevelDefault=5,
            UpdateRate=1.0,
        ),
    )
    with ctx as dev:
        yield dev
