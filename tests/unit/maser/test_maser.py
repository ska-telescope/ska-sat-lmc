# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module contains the tests for SatMaser."""

from __future__ import annotations

import gc
import time
from typing import Generator

import pytest
from _pytest.fixtures import SubRequest
from ska_control_model import (
    AdminMode,
    ControlMode,
    HealthState,
    SimulationMode,
    TestMode,
)
from ska_tango_testing.context import (
    TangoContextProtocol,
    ThreadedTestTangoContextManager,
)
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import DeviceProxy, DevState, EventType

from ska_sat_lmc.clocks.comms import UdpSocket
from ska_sat_lmc.clocks.maser import SatMaser

# TODO: [MCCS-1211] Weird hang-at-garbage-collection bug
gc.disable()


@pytest.fixture(name="change_event_callbacks")
def change_event_callbacks_fixture() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of callables to be used as Tango change event callbacks.

    :return: a dictionary of callables to be used as tango change event
        callbacks.
    """
    return MockTangoEventCallbackGroup(
        "admin_mode",
        "health_state",
        "state",
        "battery_voltage_a",
        "battery_current_a",
        "battery_voltage_b",
        "battery_current_b",
        timeout=3.0,
    )


@pytest.fixture(name="maser_name", scope="session")
def maser_name_fixture() -> str:
    """
    Return the name of the maser Tango device.

    :return: the name of the maser Tango device.
    """
    return "sat-lmc/maser/1"


@pytest.fixture(name="maser_host", scope="session")
def maser_host_fixture() -> str:
    """
    Return the host of the maser Tango device.

    :return: the host of the maser Tango device.
    """
    return "127.0.0.1"


@pytest.fixture(name="maser_port", scope="session")
def maser_port_fixture() -> str:
    """
    Return the host of the maser Tango device.

    :return: the host of the maser Tango device.
    """
    return "5000"


@pytest.fixture(name="tango_harness")
def tango_harness_fixture(
    maser_name: str,
    maser_host: str,
    maser_port: str,
) -> Generator[TangoContextProtocol, None, None]:
    """
    Return a Tango harness against which to run tests of the deployment.

    :param maser_name: the name of the maser Tango device
    :param maser_host: the host of the maser node
    :param maser_port: the port of the maser node

    :yields: tango harness
    """
    context_manager = ThreadedTestTangoContextManager()
    context_manager.add_device(
        maser_name,
        "ska_sat_lmc.SatMaser",
        MaserHost=maser_host,
        MaserPort=maser_port,
        LoggingLevelDefault=5,
    )
    with context_manager as context:
        yield context


@pytest.fixture(name="maser_device")
def maser_device_fixture(
    tango_harness: TangoContextProtocol,
    maser_name: str,
) -> DeviceProxy:
    """
    Fixture that returns the maser Tango device under test.

    :param tango_harness: a test harness for Tango devices.
    :param maser_name: name of the maser Tango device.

    :yields: maser device
    """
    yield tango_harness.get_device(maser_name)


@pytest.fixture(name="patched_udpserver")
def patched_udpserver_fixture(
    request: SubRequest, monkeypatch: pytest.MonkeyPatch
) -> Generator:
    """
    Fixture that patches the socket connect & readline methods.

    :param request: A pytest object giving access to the requesting test
        context
    :param monkeypatch: does what is says on the tin

    :yields: udpserver
    """

    def mock_connect(self: UdpSocket) -> None:
        """
        Provide mocked socket connection.

        :param self: not used for mock
        """
        self._connected = True

    def mock_write_readline(
        self: UdpSocket, buff: bytes, eol: bytes, timeout: float
    ) -> bytes:
        """
        Provide a mocked reply from mock socket connection.

        :param self: not used for mock
        :param buff: not used for mock
        :param eol: not used for mock
        :param timeout: not used for mock

        :return: reply with bytes
        """
        return request.param.encode()

    with monkeypatch.context() as patched_context:
        patched_context.setattr(UdpSocket, "connect", mock_connect)
        patched_context.setattr(UdpSocket, "write_readline", mock_write_readline)
        yield


def test_init_device(
    maser_device: SatMaser,
) -> None:
    """
    Test for Initial state.

    :param maser_device: fixture that provides a
        :py:class:`tango.DeviceProxy` to the device under test, in a
        :py:class:`tango.test_context.DeviceTestContext`.
    """
    assert maser_device.state() == DevState.DISABLE
    assert maser_device.status() == "The device is in DISABLE state."
    assert maser_device.adminMode == AdminMode.OFFLINE
    assert maser_device.healthState == HealthState.UNKNOWN
    assert maser_device.controlMode == ControlMode.REMOTE
    assert maser_device.simulationMode == SimulationMode.FALSE
    assert maser_device.testMode == TestMode.NONE


@pytest.mark.parametrize(
    "patched_udpserver",
    [
        "$46d04247F9E26384E71FB1A7EE012B0C111D2091100E70AD07"
        "46E00D673C0135CA70104270203A0E084A74510494C6DEffCAC88400CD007D1"
    ],
    indirect=True,
)
def test_attribute_values(
    maser_device: SatMaser,
    change_event_callbacks: MockTangoEventCallbackGroup,
    patched_udpserver: None,
) -> None:
    """
    Test of connection to maser with valid attributes.

    :param maser_device: fixture that provides a
        :py:class:`tango.DeviceProxy` to the device under test, in a
        :py:class:`tango.test_context.DeviceTestContext`.
    :param change_event_callbacks: dictionary of Tango change event
        callbacks with asynchrony support.
    :param patched_udpserver: patched context
    """
    maser_device.adminMode == AdminMode.OFFLINE
    maser_device.subscribe_event(
        "state",
        EventType.CHANGE_EVENT,
        change_event_callbacks["state"],
    )
    change_event_callbacks["state"].assert_change_event(DevState.DISABLE)
    maser_device.subscribe_event(
        "healthState",
        EventType.CHANGE_EVENT,
        change_event_callbacks["health_state"],
    )
    change_event_callbacks["health_state"].assert_change_event(HealthState.UNKNOWN)
    maser_device.subscribe_event(
        "adminMode",
        EventType.CHANGE_EVENT,
        change_event_callbacks["admin_mode"],
    )
    change_event_callbacks["admin_mode"].assert_change_event(AdminMode.OFFLINE)

    maser_device.adminMode = AdminMode.ONLINE  # type: ignore[assignment]
    assert maser_device.adminMode == AdminMode.ONLINE
    assert maser_device.state() == DevState.ON
    assert maser_device.status() == "The device is in ON state."
    assert maser_device.healthState == HealthState.OK
    change_event_callbacks["admin_mode"].assert_change_event(AdminMode.ONLINE)
    change_event_callbacks["health_state"].assert_change_event(HealthState.OK)
    change_event_callbacks["state"].assert_change_event(DevState.ON)
    # These should not change
    assert maser_device.controlMode == ControlMode.REMOTE
    assert maser_device.simulationMode == SimulationMode.FALSE
    assert maser_device.testMode == TestMode.NONE

    maser_device.subscribe_event(
        "battery_voltage_a",
        EventType.CHANGE_EVENT,
        change_event_callbacks["battery_voltage_a"],
    )
    maser_device.subscribe_event(
        "battery_current_a",
        EventType.CHANGE_EVENT,
        change_event_callbacks["battery_current_a"],
    )
    maser_device.subscribe_event(
        "battery_voltage_b",
        EventType.CHANGE_EVENT,
        change_event_callbacks["battery_voltage_b"],
    )
    maser_device.subscribe_event(
        "battery_current_b",
        EventType.CHANGE_EVENT,
        change_event_callbacks["battery_current_b"],
    )
    time.sleep(2)

    assert maser_device.battery_voltage_a == pytest.approx(27.66106)
    assert maser_device.battery_current_a == pytest.approx(0.08056619)
    assert maser_device.battery_voltage_b == pytest.approx(28.1005)
    assert maser_device.battery_current_b == pytest.approx(3.08837)
    assert maser_device.hydrogen_pressure_setting == pytest.approx(5.8300632)
    assert maser_device.hydrogen_pressure_measured == pytest.approx(1.5319785)
    assert maser_device.purifier_current == pytest.approx(0.61889489)
    assert maser_device.dissociator_current == pytest.approx(0.5163561)
    assert maser_device.dissociator_light == pytest.approx(4.64842559)
    assert maser_device.internal_top_heater_voltage == pytest.approx(1.45995719)
    assert maser_device.internal_bottom_heater_voltage == pytest.approx(0.94238039)
    assert maser_device.internal_side_heater_voltage == pytest.approx(1.391598)
    assert maser_device.thermal_control_unit_heater_voltage == pytest.approx(2.5439388)
    assert maser_device.external_side_heater_voltage == pytest.approx(1.32812159)
    assert maser_device.external_bottom_heater_voltage == pytest.approx(1.1279268)
    assert maser_device.isolator_heater_voltage == pytest.approx(0.84472439)
    assert maser_device.tube_heater_voltage == pytest.approx(0.56640479)
    assert maser_device.boxes_temperature == pytest.approx(42.96864)
    assert maser_device.boxes_current == pytest.approx(0.26122979)
    assert maser_device.ambient_temperature == pytest.approx(22.607364)
    assert maser_device.cfield_voltage == pytest.approx(0.0463866)
    assert maser_device.varactor_diode_voltage == pytest.approx(3.61815479)
    assert maser_device.external_high_voltage_value == pytest.approx(3.50194416)
    assert maser_device.external_high_current_value == pytest.approx(16.11324)
    assert maser_device.internal_high_voltage_value == pytest.approx(3.50389728)
    assert maser_device.internal_high_current_value == pytest.approx(14.16012)
    assert maser_device.hydrogen_storage_pressure == pytest.approx(1.0937472)
    assert maser_device.hydrogen_storage_heater_voltage == pytest.approx(12.951627)
    assert maser_device.pirani_heater_voltage == pytest.approx(11.3586135)
    assert maser_device.oscillator_100mhz_voltage == pytest.approx(0.634764)
    assert maser_device.amplitude_405khz_voltage == pytest.approx(8.715798)
    assert maser_device.oscillator_voltage == pytest.approx(4.29198119)
    assert maser_device.positive24vdc == pytest.approx(24.90228)
    assert maser_device.positive15vdc == pytest.approx(15.78125)
    assert maser_device.negative15vdc == pytest.approx(-15.625)
    assert maser_device.positive5vdc == pytest.approx(5.15592)
    assert maser_device.negative5vdc == pytest.approx(0.0)
    assert maser_device.positive8vdc == pytest.approx(8.0072999)
    assert maser_device.positive18vdc == pytest.approx(0.0)
    assert maser_device.lock100mhz == pytest.approx(4.882499)
    assert maser_device.phase_lock_loop_lockstatus


#     change_event_callbacks["battery_voltage_a"].assert_change_event(
#         27.661062, lookahead=50
#     )
#     change_event_callbacks["battery_current_a"].assert_change_event(
#         0.08056619999999999, lookahead=5
#     )
#     change_event_callbacks["battery_voltage_b"].assert_change_event(
#         28.100514, lookahead=50
#     )
#     change_event_callbacks["battery_current_b"].assert_change_event(
#         3.088371, lookahead=50
#     )


@pytest.mark.parametrize(
    "patched_udpserver",
    [
        "$46ZZ4247F9E26384E71FB1A7EE012B0C111D2091100E70AD07"
        "46E00D673C0135CA70104270203A0E084A74510494C6DEffCAC88400CD007D1"
    ],
    indirect=True,
)
def test_bad_chars(
    maser_device: SatMaser,
    change_event_callbacks: MockTangoEventCallbackGroup,
    patched_udpserver: None,
) -> None:
    """
    Test of connection to maser and catch invalid characters sent.

    :param maser_device: fixture that provides a
        :py:class:`tango.DeviceProxy` to the device under test, in a
        :py:class:`tango.test_context.DeviceTestContext`.
    :param change_event_callbacks: dictionary of Tango change event
        callbacks with asynchrony support.
    :param patched_udpserver: patched context
    """
    maser_device.subscribe_event(
        "adminMode",
        EventType.CHANGE_EVENT,
        change_event_callbacks["admin_mode"],
    )
    change_event_callbacks["admin_mode"].assert_change_event(AdminMode.OFFLINE)

    maser_device.adminMode = AdminMode.ONLINE  # type: ignore[assignment]
    change_event_callbacks["admin_mode"].assert_change_event(AdminMode.ONLINE)

    time.sleep(2)
    assert maser_device.adminMode == AdminMode.ONLINE
    assert maser_device.state() == DevState.FAULT
    assert maser_device.status() == "The device is in FAULT state."
    assert maser_device.healthState == HealthState.FAILED


@pytest.mark.parametrize(
    "patched_udpserver",
    [
        "$46d04247F9E26384E71FB1A7EE012B0C111D2091100E70AD07"
        "46E00D673C0135CA70104270203A0E084A74510494C6DE00CAC88400CD007D1"
    ],
    indirect=True,
)
def test_attribute_alarm(
    maser_device: SatMaser,
    change_event_callbacks: MockTangoEventCallbackGroup,
    patched_udpserver: None,
) -> None:
    """
    Test of connection to maser and monitor alarm condition.

    :param maser_device: fixture that provides a
        :py:class:`tango.DeviceProxy` to the device under test, in a
        :py:class:`tango.test_context.DeviceTestContext`.
    :param change_event_callbacks: dictionary of Tango change event
        callbacks with asynchrony support.
    :param patched_udpserver: patched context
    """
    maser_device.subscribe_event(
        "adminMode",
        EventType.CHANGE_EVENT,
        change_event_callbacks["admin_mode"],
    )
    maser_device.subscribe_event(
        "state",
        EventType.CHANGE_EVENT,
        change_event_callbacks["state"],
    )
    change_event_callbacks["admin_mode"].assert_change_event(AdminMode.OFFLINE)

    maser_device.adminMode = AdminMode.ONLINE  # type: ignore[assignment]

    #    change_event_callbacks["admin_mode"].assert_change_event(AdminMode.ONLINE,timeout=30.0)
    assert maser_device.adminMode == AdminMode.ONLINE
    time.sleep(2)
    assert maser_device.state() == DevState.ALARM
    assert maser_device.healthState == HealthState.FAILED
