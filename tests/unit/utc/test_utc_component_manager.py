# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module contains the tests of the utc component manager."""

from __future__ import annotations

import unittest
import unittest.mock
from typing import Callable, Iterator

import pytest
from ska_control_model import CommunicationStatus, HealthState
from ska_tango_testing.mock import MockCallableGroup

from ska_sat_lmc.utc import UtcComponentManager
from tests.harness import SatTangoTestHarness, SatTangoTestHarnessContext


@pytest.fixture(name="test_context")
def test_context_fixture(
    whiterabbit_trls: list[str],
    mock_whiterabbit_factory: Callable[[], unittest.mock.Mock],
) -> Iterator[SatTangoTestHarnessContext]:
    """
    Return a test context in which mock whiterabbit devices are running.

    :param whiterabbit_trls: TRL's of the whiterabbits
    :param mock_whiterabbit_factory: a factory that returns a mock whiterabbit
        device each time it is called

    :yields: a test context.
    """
    harness = SatTangoTestHarness()

    for trl in whiterabbit_trls:
        harness.add_mock_whiterabbit_device(trl, mock_whiterabbit_factory())

    with harness as context:
        yield context


def test_communication(
    test_context: SatTangoTestHarnessContext,
    utc_component_manager: UtcComponentManager,
    callbacks: MockCallableGroup,
) -> None:
    """
    Test the utc component manager's management of communication.

    :param test_context: the test context in which this test will be run.
    :param utc_component_manager: the utc component manager under test.
    :param callbacks: A dictionary of callbacks with async support.
    """
    assert utc_component_manager.communication_state == CommunicationStatus.DISABLED
    utc_component_manager.start_communicating()
    callbacks["communication_state"].assert_call(CommunicationStatus.NOT_ESTABLISHED)
    callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)
    assert utc_component_manager.communication_state == CommunicationStatus.ESTABLISHED

    utc_component_manager.stop_communicating()
    callbacks["communication_state"].assert_call(CommunicationStatus.DISABLED)
    assert utc_component_manager.communication_state == CommunicationStatus.DISABLED


@pytest.mark.parametrize(
    "initial_health_state",
    [
        HealthState.UNKNOWN,
        HealthState.OK,
        HealthState.DEGRADED,
        HealthState.FAILED,
    ],
)
def test_healths(
    test_context: SatTangoTestHarnessContext,
    utc_component_manager: UtcComponentManager,
    callbacks: MockCallableGroup,
    initial_health_state: HealthState,
) -> None:
    """
    Test the utc component manager's management of health of subdevices.

    :param test_context: the test context in which this test will be run.
    :param utc_component_manager: the utc component manager under test.
    :param callbacks: A dictionary of callbacks with async support.
    :param initial_health_state: the initial healthState
    """
    utc_component_manager.start_communicating()
    callbacks["communication_state"].assert_call(CommunicationStatus.NOT_ESTABLISHED)
    callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)
    # set the health as mocked white rabbits don't send device_health_state_changed.
    for trl in utc_component_manager._whiterabbits.keys():
        utc_component_manager._whiterabbits[trl].health = initial_health_state

    for trl in utc_component_manager._whiterabbits.keys():
        assert utc_component_manager.get_health(trl) == initial_health_state
