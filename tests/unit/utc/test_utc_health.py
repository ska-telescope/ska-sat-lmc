# -*- coding: utf-8 -*-
#
# This file is part of the SKA Sat LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests for SatUtcDevice."""
from __future__ import annotations

import gc
import unittest.mock
from typing import Callable, Iterator

import pytest
from ska_control_model import AdminMode, CommunicationStatus, HealthState
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import DevState, EventType

from ska_sat_lmc import SatUtc
from ska_sat_lmc.device_proxy import SatDeviceProxy
from ska_sat_lmc.utc.utc_component_manager import UtcComponentManager
from tests.harness import SatTangoTestHarness, SatTangoTestHarnessContext

# To stop tests hanging during gc.
gc.disable()


@pytest.fixture(name="device_under_test")
def device_under_test_fixture(
    test_context: SatTangoTestHarnessContext,
) -> SatDeviceProxy:
    """
    Fixture that returns the device under test.

    :param test_context: the context in which the tests run

    :return: the device under test
    """
    return test_context.get_utc_device()


class TestPatchedUtc:
    """Test class for SatUtc tests that patches the component manager."""

    @pytest.fixture(name="test_context")
    def test_context_fixture(
        self: TestPatchedUtc,
        whiterabbit_trls: list[str],
        mock_whiterabbit_factory: Callable[[], unittest.mock.Mock],
        patched_utc_class: type[SatUtc],
    ) -> Iterator[SatTangoTestHarnessContext]:
        """
        Create a test harness providing a Tango context for devices under test.

        :param whiterabbit_trls: A list of white rabbit TRLs.
        :param mock_whiterabbit_factory: a factory for whiterabbits
        :param patched_utc_class: A UTC device
            class patched with additional methods for testing.

        :yield: A tango context with devices to test.
        """
        harness = SatTangoTestHarness()

        for trl in whiterabbit_trls:
            harness.add_mock_whiterabbit_device(trl, mock_whiterabbit_factory())

        harness.add_utc_device(
            whiterabbit_trls,
            device_class=patched_utc_class,
        )
        with harness as context:
            yield context

    def test_health_rollup(
        self: TestPatchedUtc,
        device_under_test: SatDeviceProxy,
        utc_component_manager: UtcComponentManager,
        whiterabbit_trls: list[str],
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Tests for evaluating health rollup.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param whiterabbit_trls: TRLs for whiterabbit sub-devices
        :param utc_component_manager: mock CM
        :param change_event_callbacks: dictionary of callables with asynchronous
            assertion support, for use as callbacks in testing
        """

        def _set_health(
            trls: list[str],
            health_to_set: HealthState,
            health_expected: HealthState,
        ) -> None:
            """
            Mock a health change on sub-devices and check healthstate.

            Helper function that mocks a change in health for a list of sub-devices to
            a particular healthstate before checking the changes have propogated to the
            devices health.

            :param trls: A list of device TRLs.
            :param health_to_set: The health devices should be mocked to have.
            :param health_expected: Expected health after devices have transitioned.
            """
            assert utc_component_manager._component_state_callback is not None
            utc_health = device_under_test.healthState

            for trl in trls:
                utc_component_manager._component_state_changed_callback(
                    health=health_to_set, trl=trl
                )

            if utc_health != health_expected:
                change_event_callbacks["healthState"].assert_change_event(
                    health_expected, lookahead=5
                )
            assert device_under_test.healthState == health_expected

        device_under_test.subscribe_event(
            "adminMode",
            EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )
        device_under_test.subscribe_event(
            "state",
            EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )
        device_under_test.subscribe_event(
            "healthState",
            EventType.CHANGE_EVENT,
            change_event_callbacks["healthState"],
        )

        change_event_callbacks["adminMode"].assert_change_event(AdminMode.OFFLINE)
        assert device_under_test.adminMode == AdminMode.OFFLINE
        change_event_callbacks["state"].assert_change_event(DevState.DISABLE)
        assert device_under_test.state() == DevState.DISABLE
        change_event_callbacks["healthState"].assert_change_event(HealthState.UNKNOWN)
        assert device_under_test.healthState == HealthState.UNKNOWN

        device_under_test.adminMode = AdminMode.ONLINE
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.ONLINE)
        assert device_under_test.adminMode == AdminMode.ONLINE
        utc_component_manager._communication_state_changed_callback(
            CommunicationStatus.ESTABLISHED
        )

        _set_health(
            ["low-sat/wr/1", "low-sat/wr/2", "low-sat/wr/3"],
            HealthState.OK,
            HealthState.OK,
        )
        # 1 Whiterabbit degraded (1/3), expect UTC to be degraded.
        _set_health(
            ["low-sat/wr/1"],
            HealthState.DEGRADED,
            HealthState.DEGRADED,
        )
        # 2 Whiterabbits degraded (2/3), expect UTC to be degraded.
        _set_health(
            ["low-sat/wr/1", "low-sat/wr/2"],
            HealthState.DEGRADED,
            HealthState.DEGRADED,
        )
        # 3 Whiterabbits degraded (3/3), expect UTC to be degraded.
        _set_health(
            ["low-sat/wr/1", "low-sat/wr/2", "low-sat/wr/3"],
            HealthState.DEGRADED,
            HealthState.DEGRADED,
        )
        # 1 Whiterabbits failed(1/3), expect UTC to be  failed.
        _set_health(
            ["low-sat/wr/2"],
            HealthState.FAILED,
            HealthState.FAILED,
        )
        _set_health(
            ["low-sat/wr/1", "low-sat/wr/2", "low-sat/wr/3"],
            HealthState.OK,
            HealthState.OK,
        )
