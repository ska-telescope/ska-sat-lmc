# -*- coding: utf-8 -*-
#
# This file is part of the SKA Sat LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests for SatUtcDevice."""
from __future__ import annotations

import gc
import os

# import time
import unittest.mock
from typing import Callable, Iterator

import pytest
from ska_control_model import (
    AdminMode,
    ControlMode,
    HealthState,
    SimulationMode,
    TestMode,
)
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import DevState, EventType

# from ska_sat_lmc import SatUtc
from ska_sat_lmc.device_proxy import SatDeviceProxy

# from ska_sat_lmc.utc.utc_component_manager import UtcComponentManager
from tests.harness import SatTangoTestHarness, SatTangoTestHarnessContext

# To stop tests hanging during gc.
gc.disable()


@pytest.fixture(name="device_under_test")
def device_under_test_fixture(
    test_context: SatTangoTestHarnessContext,
) -> SatDeviceProxy:
    """
    Fixture that returns the device under test.

    :param test_context: the context in which the tests run

    :return: the device under test
    """
    return test_context.get_utc_device()


class TestSatUtc:
    """Test class for SatUtc tests."""

    @pytest.fixture(name="test_context")
    def test_context_fixture(  # pylint: disable=too-many-arguments
        self: TestSatUtc,
        whiterabbit_trls: list[str],
        mock_whiterabbit_factory: Callable[[], unittest.mock.Mock],
    ) -> Iterator[SatTangoTestHarnessContext]:
        """
        Create a test harness providing a Tango context for devices under test.

        :param whiterabbit_trls: A list of white rabbit TRLs.
        :param mock_whiterabbit_factory: a factory for whiterabbits

        :yield: A tango context with devices to test.
        """
        harness = SatTangoTestHarness()
        for trl in whiterabbit_trls:
            harness.add_mock_whiterabbit_device(trl, mock_whiterabbit_factory())

        harness.add_utc_device(
            whiterabbit_trls,
        )
        with harness as context:
            yield context

    def test_init_device(
        self: TestSatUtc,
        device_under_test: SatDeviceProxy,
    ) -> None:
        """
        Test for Initial state.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.state() == DevState.DISABLE
        assert device_under_test.status() == "The device is in DISABLE state."
        assert device_under_test.adminMode == AdminMode.OFFLINE
        assert device_under_test.healthState == HealthState.UNKNOWN
        assert device_under_test.controlMode == ControlMode.REMOTE
        assert device_under_test.simulationMode == SimulationMode.FALSE
        assert device_under_test.testMode == TestMode.NONE

    def test_build_state(
        self: TestSatUtc,
        device_under_test: SatDeviceProxy,
    ) -> None:
        """
        Test for buildState.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        release_filename = os.path.join(os.getcwd(), ".release")
        with open(release_filename) as fd:
            line = fd.readline()
            release = line.strip().split("=")[1]
        assert device_under_test.buildState == f"SatUtc Software Version: {release}"

    def test_version_id(
        self: TestSatUtc,
        device_under_test: SatDeviceProxy,
    ) -> None:
        """
        Test for versionId.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        release_filename = os.path.join(os.getcwd(), ".release")
        with open(release_filename) as fd:
            line = fd.readline()
            release = line.strip().split("=")[1]
        assert device_under_test.versionId == release

    def test_health_state(
        self: TestSatUtc,
        device_under_test: SatDeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test for healthState.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.

        """
        device_under_test.subscribe_event(
            "healthState",
            EventType.CHANGE_EVENT,
            change_event_callbacks["healthState"],
        )
        change_event_callbacks["healthState"].assert_change_event(HealthState.UNKNOWN)
        assert device_under_test.healthState == HealthState.UNKNOWN

    def test_control_mode(
        self: TestSatUtc,
        device_under_test: SatDeviceProxy,
    ) -> None:
        """
        Test for controlMode.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        assert device_under_test.controlMode == ControlMode.REMOTE

    def test_simulation_mode(
        self: TestSatUtc,
        device_under_test: SatDeviceProxy,
    ) -> None:
        """
        Test for simulationMode.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        assert device_under_test.simulationMode == SimulationMode.FALSE

    def test_test_mode(
        self: TestSatUtc,
        device_under_test: SatDeviceProxy,
    ) -> None:
        """
        Test for testMode.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        assert device_under_test.testMode == TestMode.NONE
