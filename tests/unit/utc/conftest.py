# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module defines a pytest harness for testing the SAT.LMC utc module."""

from __future__ import annotations

import logging
import unittest
from typing import Callable, Iterable

import pytest
import pytest_mock
import tango
from ska_control_model import AdminMode, CommunicationStatus, HealthState
from ska_tango_testing.mock import MockCallable, MockCallableGroup
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from ska_sat_lmc.device_proxy import SatDeviceProxy
from ska_sat_lmc.testing.mock import MockDeviceBuilder
from ska_sat_lmc.utc import SatUtc, UtcComponentManager


@pytest.fixture()
def callbacks() -> MockCallableGroup:
    """
    Return a dictionary of callables to be used as callbacks.

    :return: a dictionary of callables to be used as callbacks.
    """
    return MockCallableGroup(
        "communication_state",
        "component_state",
    )


@pytest.fixture(name="utc_trl")
def utc_trl_fixture() -> str:
    """
    Return the utc_trl.

    :return: the utc_trl.
    """
    return "low-sat/utc/1"


@pytest.fixture(name="whiterabbit_trls")
def whiterabbit_trls_fixture() -> Iterable[str]:
    """
    Return the whiterabbit trl's.

    :return: the whiterabbit_trls.
    """
    return ["low-sat/wr/1", "low-sat/wr/2", "low-sat/wr/3"]


@pytest.fixture()
def component_state_changed_callback(
    mock_callback_factory: Callable[[], unittest.mock.Mock],
) -> unittest.mock.Mock:
    """
    Return a mock callback for a change of state.

    :param mock_callback_factory: fixture that provides a mock callback
        factory (i.e. an object that returns mock callbacks when
        called).

    :return: a mock callback to be called when the component manager
        detects that state has changed.
    """
    return mock_callback_factory()


@pytest.fixture(name="change_event_callbacks")
def change_event_callbacks_fixture() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of change event callbacks with asynchrony support.

    :return: a callback group.
    """
    return MockTangoEventCallbackGroup(
        "healthState",
        "adminMode",
        "state",
        timeout=15.0,
    )


@pytest.fixture(name="whiterabbits")
def whiterabbits_fixture(
    whiterabbit_trls: Iterable[str], logger: logging.Logger
) -> list[SatDeviceProxy]:
    """
    Return a list of proxies to whiterabbit devices.

    :param whiterabbit_trls: names of whiterabbit_trls
    :param logger: a logger for the proxies to use.
    :return: a list of proxies to whiterabbit devices
    """
    return [SatDeviceProxy(trl, logger) for trl in whiterabbit_trls]


@pytest.fixture(name="mock_whiterabbit_factory")
def mock_whiterabbit_factory_fixture() -> MockDeviceBuilder:
    """
    Fixture that provides a mock whiterabbit factory.

    :return: a factory for mock whiterabbit devices.
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    builder.add_attribute("healthState", HealthState.OK)
    builder.add_attribute("adminMode", AdminMode.ONLINE)
    return builder


@pytest.fixture(name="mock_component_manager")
def mock_component_manager_fixture(
    mocker: pytest_mock.plugin.MockerFixture,
) -> unittest.mock.Mock:
    """
    Return a mock component manager.

    The mock component manager is a simple mock except for one bit of
    extra functionality: when we call start_communicating() on it, it
    makes calls to callbacks signaling that communication is established.

    :param mocker: pytest wrapper for unittest.mock

    :return: a mock component manager
    """
    mock = mocker.Mock()
    mock.is_communicating = False

    def _start_communicating(mock: unittest.mock.Mock) -> None:
        mock.is_communicating = True
        mock._communication_state_changed_callback(CommunicationStatus.NOT_ESTABLISHED)
        mock._communication_state_changed_callback(CommunicationStatus.ESTABLISHED)
        mock.is_communicating = True

    mock.start_communicating.side_effect = lambda: _start_communicating(mock)

    mock.max_queued_tasks = 0
    mock.max_executing_tasks = 1
    mock.abort_commands = None
    return mock


@pytest.fixture(name="utc_component_manager")
def utc_component_manager_fixture(
    whiterabbit_trls: Iterable[str],
    logger: logging.Logger,
    callbacks: MockCallableGroup,
) -> UtcComponentManager:
    """
    Return a utc component manager in simulation mode.

    :param whiterabbit_trls: whiterabbit tango resource locators
    :param logger: the logger to be used by this object.
    :param callbacks: dictionary of callables with asynchronous
        assertion support, for use as callbacks in testing

    :return: a component manager for the Sat utc device
    """
    cmpnt_manager = UtcComponentManager(
        whiterabbit_trls,
        logger,
        callbacks["communication_state"],
        callbacks["component_state"],
    )
    wrapped_device_communication_changed_callback = MockCallable(
        wraps=cmpnt_manager._device_communication_state_changed
    )
    cmpnt_manager._device_communication_state_changed = (  # type: ignore[assignment]
        wrapped_device_communication_changed_callback
    )
    return cmpnt_manager


@pytest.fixture(name="patched_utc_class")
def patched_utc_class_fixture(
    utc_component_manager: UtcComponentManager,
) -> type[SatUtc]:
    """
    Return a utc device with a patched component manager.

    :param utc_component_manager: the mock component manager with which
        to patch the device
    :return: a utc device that is patched with a mock component
        manager.
    """

    class PatchedUtc(SatUtc):
        """A utc device patched with a component manager."""

        def create_component_manager(
            self: PatchedUtc,
        ) -> UtcComponentManager:
            """
            Return a patched component manager instead of the usual one.

            :return: a mock component manager
            """
            wrapped_state_changed_callback = MockCallable(
                wraps=self._component_state_changed_callback
            )
            utc_component_manager._component_state_changed_callback = (
                wrapped_state_changed_callback
            )

            wrapped_comms_changed_callback = MockCallable(
                wraps=self._communication_state_changed_callback
            )
            utc_component_manager._communication_state_changed_callback = (
                wrapped_comms_changed_callback
            )
            return utc_component_manager

    return PatchedUtc
