# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module contains the tests for SatPhaseMicroStepper."""

from __future__ import annotations

import gc
import time
from typing import Any, Generator

import pytest
from _pytest.fixtures import SubRequest
from ska_control_model import (
    AdminMode,
    ControlMode,
    HealthState,
    ResultCode,
    SimulationMode,
    TestMode,
)
from ska_tango_testing.context import (
    TangoContextProtocol,
    ThreadedTestTangoContextManager,
)
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import DeviceProxy, DevState, EventType

from ska_sat_lmc.clocks.comms import TcpSocket, UdpSocket
from ska_sat_lmc.clocks.phasemicrostepper import SatPhaseMicroStepper

# TODO: [MCCS-1211] Weird hang-at-garbage-collection bug
gc.disable()


@pytest.fixture(name="change_event_callbacks")
def change_event_callbacks_fixture() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of callables to be used as Tango change event callbacks.

    :return: a dictionary of callables to be used as tango change event
        callbacks.
    """
    return MockTangoEventCallbackGroup(
        "admin_mode",
        "health_state",
        "state",
        "serial_number",
        "first_internal_oscillator",
        "second_internal_oscillator",
        "internal_temperature",
        timeout=2.0,
    )


@pytest.fixture(name="phasemicrostepper_name", scope="session")
def phasemicrostepper_name_fixture() -> str:
    """
    Return the name of the phasemicrostepper Tango device.

    :return: the name of the phasemicrostepper Tango device.
    """
    return "sat-lmc/phase/1"


@pytest.fixture(name="phasemicrostepper_host", scope="session")
def phasemicrostepper_host_fixture() -> str:
    """
    Return the url of the phasemicrostepper Tango device.

    :return: the host name of the phasemicrostepper Tango device.
    """
    return "127.0.0.1"


@pytest.fixture(name="phasemicrostepper_tcp_port", scope="session")
def phasemicrostepper_tcp_port_fixture() -> str:
    """
    Return the tcp port number of the phasemicrostepper Tango device.

    :return: the tcp port number of the phasemicrostepper Tango device.
    """
    return "5000"


@pytest.fixture(name="phasemicrostepper_udp_port", scope="session")
def phasemicrostepper_udp_port_fixture() -> str:
    """
    Return the udp port number of the phasemicrostepper Tango device.

    :return: the udp port number of the phasemicrostepper Tango device.
    """
    return "3100"


@pytest.fixture(name="tango_harness")
def tango_harness_fixture(
    phasemicrostepper_name: str,
    phasemicrostepper_host: str,
    phasemicrostepper_tcp_port: int,
    phasemicrostepper_udp_port: int,
) -> Generator[TangoContextProtocol, None, None]:
    """
    Return a Tango harness against which to run tests of the deployment.

    :param phasemicrostepper_name: the name of the phasemicrostepper
        Tango device
    :param phasemicrostepper_host: the host name
    :param phasemicrostepper_tcp_port: the tcp port number
    :param phasemicrostepper_udp_port: the udp port number
    :yields: tango harness
    """
    context_manager = ThreadedTestTangoContextManager()
    context_manager.add_device(
        phasemicrostepper_name,
        "ska_sat_lmc.SatPhaseMicroStepper",
        PhaseHost=phasemicrostepper_host,
        TcpPort=phasemicrostepper_tcp_port,
        UdpPort=phasemicrostepper_udp_port,
        LoggingLevelDefault=5,
    )
    with context_manager as context:
        yield context


@pytest.fixture(name="phasemicrostepper_device")
def phasemicrostepper_device_fixture(
    tango_harness: TangoContextProtocol,
    phasemicrostepper_name: str,
) -> DeviceProxy:
    """
    Fixture that returns the phasemicrostepper Tango device under test.

    :param tango_harness: a test harness for Tango devices.
    :param phasemicrostepper_name: name of the phasemicrostepper Tango
        device. :yield: the phasemicrostepper Tango device under test.
    :yields: phasemicrostepper device
    """
    yield tango_harness.get_device(phasemicrostepper_name)


@pytest.fixture(name="patched_udpserver")
def patched_udpserver_fixture(
    request: SubRequest, monkeypatch: pytest.MonkeyPatch
) -> Generator:
    """
    Fixture that patches the socket connect & readline methods.

    :param request: A pytest object giving access to the requesting test
        context
    :param monkeypatch: does what is says on the tin

    :yields: udpserver
    """

    def mock_connect(self: UdpSocket) -> None:
        """
        Provide mocked socket connection.

        :param self: not used for mock
        """
        self._connected = True

    def mock_readline(self: UdpSocket, eol: bytes, timeout: float) -> bytes:
        """
        Provide a mocked reply from mock socket connection.

        :param self: not used for mock
        :param eol: not used for mock
        :param timeout: not used for mock

        :return: reply with bytes
        """
        return request.param.encode()

    with monkeypatch.context() as patched_context:
        patched_context.setattr(UdpSocket, "connect", mock_connect)
        patched_context.setattr(UdpSocket, "readline", mock_readline)
        yield


# @pytest.mark.skip(reason="future jira ticket work")
def test_init_device(
    phasemicrostepper_device: SatPhaseMicroStepper,
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Test for Initial state.

    :param phasemicrostepper_device: fixture that provides a
        :py:class:`` to the device under test, in a
        :py:class:`tango.test_context.DeviceTestContext`.
    :param change_event_callbacks: dictionary of Tango change event
        callbacks with asynchrony support.
    """
    assert phasemicrostepper_device.state() == DevState.DISABLE
    assert phasemicrostepper_device.status() == "The device is in DISABLE state."
    assert phasemicrostepper_device.adminMode == AdminMode.OFFLINE
    assert phasemicrostepper_device.healthState == HealthState.UNKNOWN
    assert phasemicrostepper_device.controlMode == ControlMode.REMOTE
    assert phasemicrostepper_device.simulationMode == SimulationMode.FALSE
    assert phasemicrostepper_device.testMode == TestMode.NONE


# @pytest.mark.skip(reason="future jira ticket work")
@pytest.mark.parametrize(
    "patched_udpserver",
    ["EOG008 0.98 0.97 36.5"],
    indirect=True,
)
def test_attribute_values(
    phasemicrostepper_device: SatPhaseMicroStepper,
    change_event_callbacks: MockTangoEventCallbackGroup,
    patched_udpserver: None,
) -> None:
    """
    Test of connection to phasemicrostepper with valid attributes.

    :param phasemicrostepper_device: fixture that provides a
        :py:class:`` to the device under test, in a
        :py:class:`tango.test_context.DeviceTestContext`.
    :param change_event_callbacks: dictionary of Tango change event
        callbacks with asynchrony support.
    :param patched_udpserver: patched context
    """
    phasemicrostepper_device.adminMode == AdminMode.OFFLINE
    phasemicrostepper_device.subscribe_event(
        "state",
        EventType.CHANGE_EVENT,
        change_event_callbacks["state"],
    )
    phasemicrostepper_device.subscribe_event(
        "healthState",
        EventType.CHANGE_EVENT,
        change_event_callbacks["health_state"],
    )
    phasemicrostepper_device.subscribe_event(
        "adminMode",
        EventType.CHANGE_EVENT,
        change_event_callbacks["admin_mode"],
    )
    change_event_callbacks["state"].assert_change_event(DevState.DISABLE)
    change_event_callbacks["health_state"].assert_change_event(HealthState.UNKNOWN)
    change_event_callbacks["admin_mode"].assert_change_event(AdminMode.OFFLINE)

    phasemicrostepper_device.adminMode = AdminMode.ONLINE  # type: ignore[assignment]
    assert phasemicrostepper_device.adminMode == AdminMode.ONLINE
    assert phasemicrostepper_device.state() == DevState.ON
    assert phasemicrostepper_device.status() == "The device is in ON state."
    assert phasemicrostepper_device.healthState == HealthState.OK
    assert phasemicrostepper_device.controlMode == ControlMode.REMOTE
    change_event_callbacks["state"].assert_change_event(DevState.ON)
    change_event_callbacks["health_state"].assert_change_event(HealthState.OK)
    change_event_callbacks["admin_mode"].assert_change_event(AdminMode.ONLINE)
    time.sleep(1)
    assert phasemicrostepper_device.simulationMode == SimulationMode.FALSE
    assert phasemicrostepper_device.testMode == TestMode.NONE
    phasemicrostepper_device.subscribe_event(
        "first_internal_oscillator",
        EventType.CHANGE_EVENT,
        change_event_callbacks["first_internal_oscillator"],
    )
    phasemicrostepper_device.subscribe_event(
        "second_internal_oscillator",
        EventType.CHANGE_EVENT,
        change_event_callbacks["second_internal_oscillator"],
    )
    phasemicrostepper_device.subscribe_event(
        "internal_temperature",
        EventType.CHANGE_EVENT,
        change_event_callbacks["internal_temperature"],
    )
    phasemicrostepper_device.subscribe_event(
        "serial_number",
        EventType.CHANGE_EVENT,
        change_event_callbacks["serial_number"],
    )
    change_event_callbacks["first_internal_oscillator"].assert_change_event(0.98)
    change_event_callbacks["second_internal_oscillator"].assert_change_event(0.97)
    change_event_callbacks["internal_temperature"].assert_change_event(36.5)
    change_event_callbacks["serial_number"].assert_change_event(8)
    assert phasemicrostepper_device.serial_number == 8
    assert phasemicrostepper_device.first_internal_oscillator == 0.98
    assert phasemicrostepper_device.second_internal_oscillator == 0.97
    assert phasemicrostepper_device.internal_temperature == 36.5


# @pytest.mark.skip(reason="future jira ticket work")
def test_tcp_command_syncpps(
    phasemicrostepper_device: SatPhaseMicroStepper,
    change_event_callbacks: MockTangoEventCallbackGroup,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """
    Test of connection to phasemicrostepper with valid attributes.

    :param phasemicrostepper_device: fixture that provides a
        :py:class:`` to the device under test, in a
        :py:class:`tango.test_context.DeviceTestContext`.
    :param change_event_callbacks: dictionary of Tango change event
        callbacks with asynchrony support.
    :param monkeypatch: what it says on the tin
    """
    replyreply = "syncPPS\r\nsyncPPS done, Error = 0\r\n"

    def mock_connect(self: SatPhaseMicroStepper) -> None:
        """
        Replace TcpSocket.connect() with mock method.

        :param self: self explanatory
        """
        self._connected = True  # type: ignore[attr-defined]

    def mock_write_readlines(
        self: SatPhaseMicroStepper,
        buff: str,
        nlines: int,
        eol: str,
        timeout: float,
    ) -> bytes:
        """
        Replace TcpSocket.write_readlines() with mock method.

        :param self: self explanatory
        :param buff: command to write to socket
        :param nlines: number of lines to read from socket
        :param eol: end of line terminator
        :param timeout: time before connection is broken
        :return: a byte array read from the socket
        """
        return replyreply.encode()

    monkeypatch.setattr(TcpSocket, "connect", mock_connect)
    monkeypatch.setattr(TcpSocket, "write_readlines", mock_write_readlines)

    phasemicrostepper_device.adminMode == AdminMode.OFFLINE
    phasemicrostepper_device.subscribe_event(
        "state",
        EventType.CHANGE_EVENT,
        change_event_callbacks["state"],
    )
    change_event_callbacks["state"].assert_change_event(DevState.DISABLE)
    change_event_callbacks["state"].assert_not_called()

    phasemicrostepper_device.adminMode = AdminMode.ONLINE  # type: ignore[assignment]
    change_event_callbacks["state"].assert_change_event(DevState.ON)
    change_event_callbacks["state"].assert_not_called()

    reply = phasemicrostepper_device.command_inout("SyncPPS")
    assert reply[0][0] == ResultCode.OK

    replyreply = "syncPPS\r\nsyncPPS done, Error = 1\r\n"
    monkeypatch.setattr(TcpSocket, "write_readlines", mock_write_readlines)

    reply = phasemicrostepper_device.command_inout("SyncPPS")
    assert reply[0][0] == ResultCode.FAILED


# @pytest.mark.skip(reason="future jira ticket work")
@pytest.mark.parametrize(
    (
        "tango_command",
        "tango_command_argin",
        "expected_tango_command_return",
    ),
    [
        (
            "SetOffsetFrequency",
            300000,
            [[ResultCode.OK], ["setF 300000"]],
        ),
        (
            "GetOffsetFrequency",
            None,
            "getF 300000",
        ),
        (
            "AdvanceAllPhase",
            15000,
            [[ResultCode.OK], ["advPh 15000"]],
        ),
        (
            "Advance10M",
            6,
            [[ResultCode.OK], ["adv10M 6"]],
        ),
        (
            "AdvancePPS",
            10000,
            [[ResultCode.OK], ["advPPS 10000"]],
        ),
        (
            "SetIPaddress",
            "10.10.5.6",
            [[ResultCode.OK], ["setIPa 10.10.5.6"]],
        ),
        (
            "SetUDPaddress",
            "10.10.5.6",
            [[ResultCode.OK], ["setUDPa 10.10.5.6"]],
        ),
    ],
)
def test_command(
    phasemicrostepper_device: SatPhaseMicroStepper,
    change_event_callbacks: MockTangoEventCallbackGroup,
    monkeypatch: pytest.MonkeyPatch,
    tango_command: str,
    tango_command_argin: Any,
    expected_tango_command_return: Any,
) -> None:
    """
    Test tango device command.

    :param phasemicrostepper_device: fixture that provides a
        :py:class:`` to the device under test, in a
        :py:class:`tango.test_context.DeviceTestContext`.
    :param change_event_callbacks: dictionary of Tango change event
        callbacks with asynchrony support.
    :param monkeypatch: what it says on the tin
    :param tango_command: name of the tango command under test.
    :param tango_command_argin: argument to the tango command
    :param expected_tango_command_return: the expected return value
        of the tango command
    """

    def mock_connect(self: SatPhaseMicroStepper) -> None:
        """
        Replace TcpSocket.connect() with mock method.

        :param self: self explanatory
        """
        self._connected = True  # type: ignore[attr-defined]

    def mock_write_readline(
        self: SatPhaseMicroStepper,
        buff: bytes,
        eol: str,
        timeout: float,
    ) -> bytes:
        """
        Replace TcpSocket.write_readline() with mock method.

        :param self: self explanatory
        :param buff: command to write to socket
        :param eol: end of line terminator
        :param timeout: time before connection is broken
        :return: a byte array read from the socket
        """
        sbuff = buff.decode()
        if "getF" in sbuff:
            sbuff = sbuff.rstrip() + " 300000\r\n"
        return sbuff.encode()

    monkeypatch.setattr(TcpSocket, "connect", mock_connect)
    monkeypatch.setattr(TcpSocket, "write_readline", mock_write_readline)

    phasemicrostepper_device.adminMode == AdminMode.OFFLINE
    phasemicrostepper_device.subscribe_event(
        "state",
        EventType.CHANGE_EVENT,
        change_event_callbacks["state"],
    )
    change_event_callbacks["state"].assert_change_event(DevState.DISABLE)
    change_event_callbacks["state"].assert_not_called()

    phasemicrostepper_device.adminMode = AdminMode.ONLINE  # type: ignore[assignment]
    change_event_callbacks["state"].assert_change_event(DevState.ON)
    change_event_callbacks["state"].assert_not_called()

    reply = phasemicrostepper_device.command_inout(tango_command, tango_command_argin)
    assert reply == expected_tango_command_return
