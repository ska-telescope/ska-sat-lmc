# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module defines a pytest harness for testing the SAT.LMC controller module."""

from __future__ import annotations

import logging
import unittest
from typing import Callable

import pytest
import pytest_mock
from ska_control_model import CommunicationStatus
from ska_tango_testing.context import TangoContextProtocol
from ska_tango_testing.mock import MockCallableGroup
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from ska_sat_lmc.controller import ControllerComponentManager, SatController


@pytest.fixture()
def callbacks() -> MockCallableGroup:
    """
    Return a dictionary of callables to be used as callbacks.

    :return: a dictionary of callables to be used as callbacks.
    """
    return MockCallableGroup(
        "communication_state",
        "component_state",
    )


@pytest.fixture()
def controller_trl() -> str:
    """
    Return the controller_trl.

    :return: the controller_trl.
    """
    return "low-sat/controller/1"


@pytest.fixture()
def clock_trl() -> str:
    """
    Return the clock_trl managed by the controller.

    :return: the clock_trl managed by the controller.
    """
    return ""


@pytest.fixture()
def frq_trl() -> str:
    """
    Return the frq_trl managed by the controller.

    :return: the frq_trl managed by the controller.
    """
    return ""


@pytest.fixture()
def utc_trl() -> str:
    """
    Return the utc_trl managed by the controller.

    :return: the utc_trl managed by the controller.
    """
    return "low-sat/utc/1"


@pytest.fixture()
def component_state_changed_callback(
    mock_callback_factory: Callable[[], unittest.mock.Mock],
) -> unittest.mock.Mock:
    """
    Return a mock callback for a change of state.

    :param mock_callback_factory: fixture that provides a mock callback
        factory (i.e. an object that returns mock callbacks when
        called).

    :return: a mock callback to be called when the component manager
        detects that state has changed.
    """
    return mock_callback_factory()


@pytest.fixture(name="change_event_callbacks")
def change_event_callbacks_fixture() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of change event callbacks with asynchrony support.

    :return: a callback group.
    """
    return MockTangoEventCallbackGroup(
        "healthState",
        "adminMode",
        "state",
        timeout=15.0,
    )


@pytest.fixture(name="controller_component_manager")
def controller_component_manager_fixture(
    tango_harness: TangoContextProtocol,
    clock_trl: str,
    frq_trl: str,
    utc_trl: str,
    logger: logging.Logger,
    callbacks: MockCallableGroup,
) -> ControllerComponentManager:
    """
    Return a controller component manager in simulation mode.

    :param tango_harness: a test harness for SAT tango devices
    :param clock_trl: clock tango resource locator
    :param frq_trl: frq tango resource locator
    :param utc_trl: utc tango resource locator
    :param logger: the logger to be used by this object.
    :param callbacks: dictionary of callables with asynchronous
        assertion support, for use as callbacks in testing

    :return: a component manager for the Sat controller device
    """
    return ControllerComponentManager(
        clock_trl,
        frq_trl,
        utc_trl,
        logger,
        callbacks["communication_state"],
        callbacks["component_state"],
    )


@pytest.fixture()
def mock_component_manager(
    mocker: pytest_mock.plugin.MockerFixture,
) -> unittest.mock.Mock:
    """
    Return a mock component manager.

    The mock component manager is a simple mock except for one bit of
    extra functionality: when we call start_communicating() on it, it
    makes calls to callbacks signaling that communication is established
    and the component is off.

    :param mocker: pytest wrapper for unittest.mock

    :return: a mock component manager
    """
    mock = mocker.Mock()
    mock.is_communicating = False

    def _start_communicating(mock: unittest.mock.Mock) -> None:
        mock.is_communicating = True
        mock._communication_state_changed_callback(CommunicationStatus.NOT_ESTABLISHED)
        mock._communication_state_changed_callback(CommunicationStatus.ESTABLISHED)

    mock.start_communicating.side_effect = lambda: _start_communicating(mock)

    mock.max_queued_tasks = 0
    mock.max_executing_tasks = 1
    mock.abort_commands = None
    return mock


@pytest.fixture()
def patched_controller_device_class(
    mock_component_manager: unittest.mock.Mock,
) -> type[SatController]:
    """
    Return a controller device that is patched with a mock component manager.

    :param mock_component_manager: the mock component manager with which
        to patch the device
    :return: a controller device that is patched with a mock component
        manager.
    """

    class PatchedSatController(SatController):
        """A controller device patched with a mock component manager."""

        def create_component_manager(
            self: PatchedSatController,
        ) -> unittest.mock.Mock:
            """
            Return a mock component manager instead of the usual one.

            :return: a mock component manager
            """
            mock_component_manager._communication_state_changed_callback = (
                self._communication_state_callback
            )
            mock_component_manager._component_state_changed_callback = (
                self._component_state_callback
            )
            return mock_component_manager

    return PatchedSatController
