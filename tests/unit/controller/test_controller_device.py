# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""Contains the tests for the SatController Tango device_under_test prototype."""

from __future__ import annotations

import os
import unittest
from typing import Iterator

import pytest
from ska_control_model import (
    AdminMode,
    ControlMode,
    HealthState,
    SimulationMode,
    TestMode,
)
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import DevState, EventType

from ska_sat_lmc import SatController
from ska_sat_lmc.device_proxy import SatDeviceProxy
from tests.harness import SatTangoTestHarness, SatTangoTestHarnessContext


@pytest.fixture(name="device_under_test")
def device_under_test_fixture(
    test_context: SatTangoTestHarnessContext,
) -> SatDeviceProxy:
    """
    Fixture that returns the device under test.

    :param test_context: the context in which the tests run

    :return: the device under test
    """
    return test_context.get_controller_device()


class TestSatController:
    """Tests of the SatController device."""

    @pytest.fixture(name="test_context")
    def test_context_fixture(  # pylint: disable=too-many-arguments
        self: TestSatController,
        clock_trl: str,
        frq_trl: str,
        utc_trl: str,
        patched_controller_device_class: type[SatController],
    ) -> Iterator[SatTangoTestHarnessContext]:
        """
        Create a test harness providing a Tango context for devices under test.

        :param clock_trl: the clock tango resource locator.
        :param frq_trl: the frq tango resource locator.
        :param utc_trl: the utc tango resource locator.
        :param patched_controller_device_class: A Controller device
            class patched with additional methods for testing.

        :yield: A tango context with devices to test.
        """
        harness = SatTangoTestHarness()
        harness.add_controller_device(
            clock_trl,
            frq_trl,
            utc_trl,
            device_class=patched_controller_device_class,
        )
        with harness as context:
            yield context

    def test_InitDevice(
        self: TestSatController,
        device_under_test: SatDeviceProxy,
    ) -> None:
        """
        Test for Initial state.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        assert device_under_test.state() == DevState.DISABLE
        assert device_under_test.status() == "The device is in DISABLE state."
        assert device_under_test.adminMode == AdminMode.OFFLINE
        assert device_under_test.healthState == HealthState.UNKNOWN
        assert device_under_test.controlMode == ControlMode.REMOTE
        assert device_under_test.simulationMode == SimulationMode.FALSE
        assert device_under_test.testMode == TestMode.NONE

    def test_adminMode(
        self: TestSatController,
        device_under_test: SatDeviceProxy,
        mock_component_manager: unittest.mock.Mock,
    ) -> None:
        """
        Test adminMode.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param mock_component_manager: a mock component manager that has
            been patched into the device under test
        """
        assert device_under_test.adminMode == AdminMode.OFFLINE
        assert device_under_test.state() == DevState.DISABLE

        device_under_test.adminMode = AdminMode.ONLINE
        mock_component_manager.start_communicating.assert_called_once_with()

        assert device_under_test.adminMode == AdminMode.ONLINE
        assert device_under_test.state() == DevState.ON

    def test_buildState(
        self: TestSatController,
        device_under_test: SatDeviceProxy,
    ) -> None:
        """
        Test for buildState.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        release_filename = os.path.join(os.getcwd(), ".release")
        with open(release_filename) as fd:
            line = fd.readline()
            release = line.strip().split("=")[1]
        assert (
            device_under_test.buildState
            == f"PatchedSatController Software Version: {release}"
        )

    def test_versionId(
        self: TestSatController,
        device_under_test: SatDeviceProxy,
    ) -> None:
        """
        Test for versionId.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        release_filename = os.path.join(os.getcwd(), ".release")
        with open(release_filename) as fd:
            line = fd.readline()
            release = line.strip().split("=")[1]
        assert device_under_test.versionId == release

    def test_healthState(
        self: TestSatController,
        device_under_test: SatDeviceProxy,
        mock_component_manager: unittest.mock.Mock,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test for healthState.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param mock_component_manager: a mock component manager that has
            been patched into the device under test
        :param change_event_callbacks: a callback that we
            can use to subscribe to health state changes on the device
        """
        device_under_test.subscribe_event(
            "healthState",
            EventType.CHANGE_EVENT,
            change_event_callbacks["healthState"],
        )
        change_event_callbacks["healthState"].assert_change_event(HealthState.UNKNOWN)
        assert device_under_test.healthState == HealthState.UNKNOWN

    def test_controlMode(
        self: TestSatController,
        device_under_test: SatDeviceProxy,
    ) -> None:
        """
        Test for controlMode.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        assert device_under_test.controlMode == ControlMode.REMOTE

    def test_simulationMode(
        self: TestSatController,
        device_under_test: SatDeviceProxy,
    ) -> None:
        """
        Test for simulationMode.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        assert device_under_test.simulationMode == SimulationMode.FALSE

    def test_testMode(
        self: TestSatController,
        device_under_test: SatDeviceProxy,
    ) -> None:
        """
        Test for testMode.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        assert device_under_test.testMode == TestMode.NONE
