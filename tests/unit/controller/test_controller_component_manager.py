# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module contains the tests of the controller component manager."""

from __future__ import annotations

from ska_control_model import CommunicationStatus
from ska_tango_testing.mock import MockCallableGroup

from ska_sat_lmc.controller import ControllerComponentManager


class TestControllerComponentManager:
    """Tests of the controller component manager."""

    def test_communication(
        self: TestControllerComponentManager,
        controller_component_manager: ControllerComponentManager,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the controller component manager's management of communication.

        :param controller_component_manager: the controller component
            manager under test.
        :param callbacks: A dictionary of callbacks with async support.
        """
        assert (
            controller_component_manager.communication_state
            == CommunicationStatus.DISABLED
        )
        controller_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)
        assert (
            controller_component_manager.communication_state
            == CommunicationStatus.ESTABLISHED
        )

        controller_component_manager.stop_communicating()
        callbacks["communication_state"].assert_call(CommunicationStatus.DISABLED)
        assert (
            controller_component_manager.communication_state
            == CommunicationStatus.DISABLED
        )
