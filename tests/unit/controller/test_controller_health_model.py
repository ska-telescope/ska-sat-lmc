# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module contains the tests for SatWhiteRabbit."""

from __future__ import annotations

import logging
from typing import Any

import pytest
from ska_control_model import HealthState

from ska_sat_lmc.controller.controller_health_model import ControllerHealthModel
from ska_sat_lmc.testing.mock import MockCallable


class TestHealthModel:
    """A class for tests of the health model reporting."""

    @pytest.fixture
    def health_model(
        self: TestHealthModel, logger: logging.Logger
    ) -> ControllerHealthModel:
        """
        Fixture to return the health model.

        :param logger: the logger

        :return: Health model to be used.
        """
        health_model = ControllerHealthModel(
            MockCallable(),
            ["low-sat/clock/1", "low-sat/frq/1", "low-sat/utc/1"],
            logger,
        )
        health_model.update_state(communicating=False)
        return health_model

    @pytest.mark.parametrize(
        ("data", "expected_health", "expected_report"),
        [
            pytest.param(
                {
                    "low-sat/clock/1": HealthState.OK,
                    "low-sat/frq/1": HealthState.UNKNOWN,
                    "low-sat/utc/1": HealthState.OK,
                },
                HealthState.UNKNOWN,
                f"low-sat/frq/1 reports health {HealthState.UNKNOWN.name}",
            ),
            pytest.param(
                {
                    "low-sat/clock/1": HealthState.OK,
                    "low-sat/frq/1": HealthState.OK,
                    "low-sat/utc/1": HealthState.DEGRADED,
                },
                HealthState.DEGRADED,
                f"low-sat/utc/1 reports health {HealthState.DEGRADED.name}",
            ),
            pytest.param(
                {
                    "low-sat/clock/1": HealthState.OK,
                    "low-sat/frq/1": HealthState.DEGRADED,
                    "low-sat/utc/1": HealthState.FAILED,
                },
                HealthState.FAILED,
                f"low-sat/frq/1 reports health {HealthState.DEGRADED.name}, "
                f"low-sat/utc/1 reports health {HealthState.FAILED.name}",
            ),
            pytest.param(
                {
                    "low-sat/clock/1": HealthState.FAILED,
                    "low-sat/frq/1": HealthState.OK,
                    "low-sat/utc/1": HealthState.FAILED,
                },
                HealthState.FAILED,
                f"low-sat/clock/1 reports health {HealthState.FAILED.name}, "
                f"low-sat/utc/1 reports health {HealthState.FAILED.name}",
            ),
            pytest.param(
                {
                    "low-sat/clock/1": HealthState.DEGRADED,
                    "low-sat/frq/1": HealthState.DEGRADED,
                    "low-sat/utc/1": HealthState.DEGRADED,
                },
                HealthState.DEGRADED,
                (
                    f"low-sat/clock/1 reports health {HealthState.DEGRADED.name}, "
                    f"low-sat/frq/1 reports health {HealthState.DEGRADED.name}, "
                    f"low-sat/utc/1 reports health {HealthState.DEGRADED.name}"
                ),
            ),
            pytest.param(
                {
                    "low-sat/clock/1": HealthState.OK,
                    "low-sat/frq/1": HealthState.OK,
                    "low-sat/utc/1": HealthState.OK,
                },
                HealthState.OK,
                "Health is OK",
            ),
        ],
    )
    def test_health_report(
        self: TestHealthModel,
        health_model: ControllerHealthModel,
        data: dict[str, Any],
        expected_health: HealthState,
        expected_report: str,
    ) -> None:
        """
        Tests for evaluating utc health.

        :param health_model: Health model fixture.
        :param data: Health data values for health model.
        :param expected_health: Expected health.
        :param expected_report: report of failed or degraded attributes
        """
        health, report = health_model.evaluate_health()
        assert health == HealthState.UNKNOWN
        health_model.update_state(communicating=True)
        health, report = health_model.evaluate_health()
        assert health == HealthState.UNKNOWN
        health_model.update_state(**data)
        health, report = health_model.evaluate_health()
        assert health == expected_health
        assert report == expected_report
        health_model.update_state(fault=True)
        health, report = health_model.evaluate_health()
        assert health == HealthState.FAILED
        health_model.update_state(communicating=False)
        assert health == HealthState.FAILED
