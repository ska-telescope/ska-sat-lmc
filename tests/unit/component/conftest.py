# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module contains the test harness for the SAT.LMC component module."""

from __future__ import annotations

import logging
import unittest.mock
from typing import Callable

import pytest
import pytest_mock
from ska_tango_testing.mock import MockCallableGroup

from ska_sat_lmc.device_proxy import SatDeviceProxy
from ska_sat_lmc.testing.mock import MockDeviceBuilder


@pytest.fixture()
def mock_factory() -> MockDeviceBuilder:
    """
    Fixture that provides a mock factory for device proxy mocks.

    :return: a factory for device proxy mocks
    """
    builder = MockDeviceBuilder()
    return builder


@pytest.fixture()
def trl() -> str:
    """
    Return an TRLFQDN for a mock device.

    :return: an TRL for a mock device.
    """
    return "mock/mock/1"


@pytest.fixture()
def mock_proxy(trl: str, logger: logging.Logger) -> SatDeviceProxy:
    """
    Return a mock proxy to the provided FQDN.

    :param trl: the TRL of the device.
    :param logger: a logger for the device proxy.

    :return: a mock proxy to the device with the TRL provided.
    """
    return SatDeviceProxy(trl, logger)


@pytest.fixture()
def mock_component_manager_factory(
    mocker: pytest_mock.MockerFixture,
) -> Callable[[], unittest.mock.Mock]:
    """
    Return a factory that can be called to provide a mock component manager.

    :param mocker: fixture that wraps unittest.mock
    :return: a mock component manager factory
    """
    return mocker.Mock


@pytest.fixture()
def callbacks() -> MockCallableGroup:
    """
    Return a dictionary of callables to be used as callbacks.

    :return: a dictionary of callables to be used as callbacks.
    """
    return MockCallableGroup(
        "communication_state",
        "component_state",
        "mock_command",
        "task",
    )
