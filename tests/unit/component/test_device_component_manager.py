# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module contains tests of the device_component_manager module."""

from __future__ import annotations

import logging

import pytest
import tango
from ska_control_model import AdminMode, CommunicationStatus, HealthState
from ska_tango_testing.context import TangoContextProtocol
from ska_tango_testing.mock import MockCallableGroup

from ska_sat_lmc.component import DeviceComponentManager


@pytest.fixture(name="component_manager")
def component_manager_fixture(
    tango_harness: TangoContextProtocol,
    trl: str,
    logger: logging.Logger,
    callbacks: MockCallableGroup,
) -> DeviceComponentManager:
    """
    Return a device component manager for testing.

    :param tango_harness: a test harness for SAT tango devices
    :param trl: the TRL of the device to be managed by this component
        manager.
    :param logger: a logger for the component manager to use.
    :param callbacks: dictionary of callables with asynchronous
        assertion support, for use as callbacks in testing

    :return: a device component manager for testing.
    """
    return DeviceComponentManager(
        trl,
        logger,
        callbacks["communication_state"],
        callbacks["component_state"],
    )


class TestDeviceComponentManager:
    """Tests of the DeviceComponentManager class."""

    def test_communication(
        self: TestDeviceComponentManager,
        component_manager: DeviceComponentManager,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the component manager's communication with the device.

        :param component_manager: the component manager under test
        :param callbacks: dictionary of callables with asynchronous
            assertion support, for use as callbacks in testing
        """
        assert component_manager.communication_status == CommunicationStatus.DISABLED

        component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.ESTABLISHED, lookahead=3
        )
        assert component_manager.communication_status == CommunicationStatus.ESTABLISHED

        component_manager.stop_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.DISABLED, lookahead=3
        )
        assert component_manager.communication_status == CommunicationStatus.DISABLED

    def test_health(
        self: TestDeviceComponentManager,
        component_manager: DeviceComponentManager,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the component managers handling of health.

        :param component_manager: the component manager under test
        :param callbacks: dictionary of callables with asynchronous
            assertion support, for use as callbacks in testing
        """
        assert component_manager.health is None

        component_manager._device_admin_mode_changed(
            "adminMode", AdminMode.ONLINE, tango.AttrQuality.ATTR_VALID
        )
        assert component_manager.health == HealthState.UNKNOWN
        callbacks["component_state"].assert_call(health=HealthState.UNKNOWN)

        component_manager._device_health_state_changed(
            "healthState", HealthState.DEGRADED, tango.AttrQuality.ATTR_VALID
        )
        assert component_manager.health == HealthState.DEGRADED
        callbacks["component_state"].assert_call(health=HealthState.DEGRADED)

        component_manager._device_admin_mode_changed(
            "adminMode", AdminMode.RESERVED, tango.AttrQuality.ATTR_VALID
        )
        assert component_manager.health is None
        callbacks["component_state"].assert_call(health=None)

        component_manager._device_admin_mode_changed(
            "adminMode", AdminMode.ONLINE, tango.AttrQuality.ATTR_VALID
        )
        assert component_manager.health == HealthState.DEGRADED
        callbacks["component_state"].assert_call(health=HealthState.DEGRADED)
