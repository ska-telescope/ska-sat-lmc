# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""
This module contains pytest fixtures other test setups.

These are common to all ska-sat-lmc tests: unit, integration and
functional (BDD).
"""
from __future__ import annotations

import _pytest
import pytest
import tango


def pytest_sessionstart(session: pytest.Session) -> None:
    """
    Pytest hook; prints info about tango version.

    :param session: a pytest Session object
    """
    print(tango.utils.info())


def pytest_addoption(
    parser: _pytest.config.argparsing.Parser,  # type: ignore[name-defined]
) -> None:
    """
    Add a command line option to pytest.

    This is a pytest hook, here implemented to add the `--true-context`
    option, used to indicate that a true Tango subsystem is available,
    so there is no need for the test harness to spin up a Tango test
    context.

    :param parser: the command line options parser
    """
    parser.addoption(
        "--true-context",
        action="store_true",
        default=False,
        help=(
            "Tell pytest that you have a true Tango context and don't "
            "need to spin up a Tango test context"
        ),
    )
    parser.addoption(
        "--sim-user",
        action="store",
        default="",
        help=("Tell pytest the user & group to run the simulator"),
    )
    parser.addoption(
        "--data-dir",
        action="store",
        default="",
        help=("Tell pytest the data directory for the simulator"),
    )
