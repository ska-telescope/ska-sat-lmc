# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module contains the ska_sat_lmc utc simulator device prototype."""

from __future__ import annotations  # allow forward references in type hints

from typing import Any

from ska_control_model import AdminMode, HealthState
from tango import DevState
from tango.server import Device, attribute

__all__ = ["UtcSimulator", "main"]


class UtcSimulator(Device):
    """An implementation of a UtcSimulator Tango device for SatLMC."""

    # ---------------
    # Initialisation
    # ---------------
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        """
        Initialise this device object.

        :param args: positional args to the init
        :param kwargs: keyword args to the init
        """
        # We aren't supposed to define initialisation methods for Tango
        # devices; we are only supposed to define an `init_device` method. But
        # we insist on doing so here, just so that we can define some
        # attributes, thereby stopping the linters from complaining about
        # "attribute-defined-outside-init" etc. We still need to make sure that
        # `init_device` re-initialises any values defined in here.
        super().__init__(*args, **kwargs)
        self._health_state: HealthState
        self._health_report: str
        self._admin_mode: AdminMode

    def init_device(self: UtcSimulator) -> None:
        """Initialise the device."""
        super().init_device()
        self._health_state = HealthState.UNKNOWN
        self._health_report = "Utc health is UNKNOWN"
        self.set_change_event("healthState", True)
        self.set_archive_event("healthState", True)
        self.set_change_event("adminMode", True)
        self.set_archive_event("adminMode", True)
        self._admin_mode = AdminMode.OFFLINE
        self.set_change_event("State", True)
        self.set_archive_event("State", True)
        self.set_state(DevState.DISABLE)

    # ----------
    # Attributes
    # ----------
    @attribute(dtype=AdminMode)
    def adminMode(self: UtcSimulator) -> AdminMode:
        """
        Read the adminMode of utc simulator.

        :return: adminMode of the utc simulator
        """
        return self._admin_mode

    @adminMode.write  # type: ignore[no-redef]
    def adminMode(self: UtcSimulator, admin_mode: AdminMode) -> None:
        """
        Set the adminMode of utc simulator.

        :param admin_mode: the new adminMode
        """
        if self._admin_mode != admin_mode:
            self._admin_mode = AdminMode(admin_mode)
            if admin_mode == AdminMode.ONLINE:
                self.set_state(DevState.ON)
                self.setHealthState(HealthState.OK)
            elif admin_mode == AdminMode.OFFLINE:
                self.set_state(DevState.DISABLE)
                self.setHealthState(HealthState.FAILED)
            elif admin_mode == AdminMode.ENGINEERING:
                self.set_state(DevState.ON)
            else:  # NOT_FITTED or RESERVED
                self.set_state(DevState.DISABLE)
                self.setHealthState(HealthState.UNKNOWN)

            self.push_change_event("adminMode", admin_mode)
            self.push_archive_event("adminMode", admin_mode)

    @attribute(dtype=HealthState)
    def healthState(self: UtcSimulator) -> HealthState:
        """
        Read the health of utc simulator.

        :return: health of the utc simulator
        """
        return self._health_state

    @healthState.write  # type: ignore[no-redef]
    def healthState(self: UtcSimulator, health: HealthState) -> None:
        """
        Set the health of utc simulator.

        :param health: the new health state
        """
        self.setHealthState(health)

    def setHealthState(self: UtcSimulator, health: HealthState) -> None:
        """
        Set the health of utc simulator.

        :param health: the new health state
        """
        if self._health_state != health:
            self._health_state = HealthState(health)
            if health == HealthState.OK:
                self._health_report = "Utc health is OK"
            elif health == HealthState.DEGRADED:
                self._health_report = "Utc health is degraded"
            elif health == HealthState.FAILED:
                self._health_report = "Utc health is failed"
            else:
                self._health_report = "Utc health is unknown"

            self.push_change_event("healthState", health)
            self.push_archive_event("healthState", health)

    @attribute(dtype="DevString")
    def healthReport(self: UtcSimulator) -> str:
        """
        Get the health report.

        :return: the health report.
        """
        return self._health_report


# ----------
# Run server
# ----------


def main(*args: str, **kwargs: str) -> int:
    """
    Entry point for module.

    :param args: positional arguments
    :param kwargs: named arguments

    :return: exit code
    """
    return UtcSimulator.run_server(args=args or None, **kwargs)


if __name__ == "__main__":
    main()
