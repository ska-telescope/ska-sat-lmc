# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module contains the ska_sat_lmc Utc device prototype."""

from __future__ import annotations  # allow forward references in type hints

import json
import sys
from typing import Any

from ska_control_model import AdminMode, CommunicationStatus, HealthState, PowerState
from ska_control_model.health_rollup import HealthRollup, HealthSummary
from ska_tango_base.base import SKABaseDevice
from tango.server import attribute, device_property

from ska_sat_lmc.component import SatComponentManager
from ska_sat_lmc.utc.utc_component_manager import UtcComponentManager

__all__ = ["SatUtc", "main"]


class SatUtc(SKABaseDevice[SatComponentManager]):
    """An implementation of a UTC Tango device for SatLMC."""

    # Device Properties
    # -----------------
    SatWhiteRabbitTrls = device_property(dtype="DevVarStringArray", default_value=[])

    # ---------------
    # Initialisation
    # ---------------
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        """
        Initialise this device object.

        :param args: positional args to the init
        :param kwargs: keyword args to the init
        """
        # We aren't supposed to define initialisation methods for Tango
        # devices; we are only supposed to define an `init_device` method. But
        # we insist on doing so here, just so that we can define some
        # attributes, thereby stopping the linters from complaining about
        # "attribute-defined-outside-init" etc. We still need to make sure that
        # `init_device` re-initialises any values defined in here.
        super().__init__(*args, **kwargs)
        self._health_state: HealthState
        self._health_report: str
        self._health_rollup: HealthRollup
        self._version: str
        self._communication_state: CommunicationStatus | None
        self.component_manager: UtcComponentManager

    def init_device(self: SatUtc) -> None:
        """
         Initialise the device.

        :raises Exception: When device initialisation fails
        """
        try:
            super().init_device()
            self._health_state = HealthState.UNKNOWN
            self._health_report = ""
            self._health_rollup = self._setup_health_rollup()
            self._communication_state = None
            self._version_id = sys.modules["ska_sat_lmc"].__version__
            device_name = f'{str(self.__class__).rsplit(".", maxsplit=1)[-1][0:-2]}'
            self._version = f"{device_name} Software Version: {self._version_id}"
            properties = (
                f"Initialised {device_name} device with properties:\n"
                f"\tSatWhiteRabbitTrls: {self.SatWhiteRabbitTrls}\n"
            )
            version_info = f"{self.__class__.__name__}, {self._build_state}"
            self.logger.info("\n%s\n%s\n%s", version_info, self._version, properties)
            self.logger.info("Init device complete")
        except Exception as ex:
            self.logger.error(f"Initialise failed: Incomplete server: {repr(ex)}")
            raise

    def _init_state_model(self: SatUtc) -> None:
        """Initialise the state model."""
        super()._init_state_model()
        self._health_state = HealthState.UNKNOWN  # InitCommand.do() does this too late.
        self.set_change_event("healthState", True, False)
        self.set_archive_event("healthState", True, False)

    def create_component_manager(
        self: SatUtc,
    ) -> UtcComponentManager:
        """
        Create and return a component manager for this device.

        :return: a component manager for this device.
        """
        return UtcComponentManager(
            self.SatWhiteRabbitTrls,
            self.logger,
            self._communication_state_changed_callback,
            self._component_state_changed_callback,
        )

    def _setup_health_rollup(
        self: SatUtc,
    ) -> HealthRollup:
        #   Rollup is based on three configurable thresholds:
        # * the number of FAILED (or UNKNOWN) sources that cause health
        #   to roll up to overall FAILED;
        # * the number of FAILED (or UNKNOWN) sources that cause health
        #   to roll up to overall DEGRADED;
        # * the number of DEGRADED sources that cause health to roll up to
        #   overall DEGRADED.

        # Here the "self" entry represents UtcController specific health changes
        # of which there are currently none.
        rollup_members = ["self"]
        thresholds = {}
        if len(self.SatWhiteRabbitTrls) > 0:
            rollup_members.append("whiterabbits")
            thresholds["whiterabbits"] = (1, 1, 1)

        health_rollup = HealthRollup(
            rollup_members,
            (1, 1, 1),  # thresholds for self? all?
            self._health_changed,
            self._health_summary_changed,
        )

        if "whiterabbits" in thresholds:
            health_rollup.define(
                "whiterabbits", self.SatWhiteRabbitTrls, thresholds["whiterabbits"]
            )

        return health_rollup

    def _update_admin_mode(self: SatUtc, admin_mode: AdminMode) -> None:
        super()._update_admin_mode(admin_mode)
        self._health_rollup.online = admin_mode in [
            AdminMode.ENGINEERING,
            AdminMode.ONLINE,
        ]

    # ----------
    # Callbacks
    # ----------
    def _communication_state_changed_callback(
        self: SatUtc,
        communication_state: CommunicationStatus,
    ) -> None:
        """
        Handle change in communications status between component manager and component.

        This is a callback hook, called by the component manager when
        the communications status changes. It is implemented here to
        drive the op_state.

        :param communication_state: the status of communications
            between the component manager and its component.
        """
        action_map = {
            CommunicationStatus.DISABLED: "component_disconnected",
            CommunicationStatus.NOT_ESTABLISHED: "component_disconnected",
            CommunicationStatus.ESTABLISHED: "component_on",
        }

        action = action_map[communication_state]
        if action is not None:
            self.op_state_model.perform_action(action)

        if communication_state == CommunicationStatus.DISABLED:
            self._component_state_changed_callback(
                trl="self", health=HealthState.UNKNOWN
            )
        elif communication_state == CommunicationStatus.ESTABLISHED:
            self._component_state_changed_callback(trl="self", health=HealthState.OK)

    def _component_state_changed_callback(
        self: SatUtc,
        fault: bool | None = None,
        power: PowerState | None = None,
        health: HealthState | None = None,
        trl: str | None = None,
    ) -> None:
        """
        Call this method whenever the state changes.

        Responsible for updating the tango side of things i.e. making
        sure the attribute is up to date, and events are pushed.

        :param fault: whether the component has faulted or not
        :param power: unused in this implementation
        :param health: An optional parameter with the new health state of the device.
        :param trl: The TRL of the device.
        """
        self.logger.debug(
            f"component state changed for {trl if trl is not None else 'self'}"
            f" with health {health} and fault {fault}"
        )
        if health is not None and trl is not None:
            self._health_rollup.health_changed(source=trl, health=health)
        if fault is not None and fault:
            self.op_state_model.perform_action("component_fault")

    def _health_changed(self: SatUtc, health: HealthState) -> None:
        """
        Handle change in this device's health state.

        This is a callback hook, called whenever the HealthModel's
        evaluated health state changes. It is responsible for updating
        the tango side of things i.e. making sure the attribute is up to
        date, and events are pushed.

        :param health: the new health value
        """
        if health is not None:
            if self._health_state != health:
                self._health_state = health
                self.push_change_event("healthState", health)

    def _health_summary_changed(self: SatUtc, health_summary: HealthSummary) -> None:
        """
        Handle change in this device's health summary.

        This is a callback hook, called whenever this device's
        evaluated health summary changes. It is responsible for updating
        the tango side of things i.e. making sure the attribute is up to
        date, and events are pushed.

        :param health_summary: the new health summary
        """
        self._health_report = json.dumps(health_summary)

    # ----------
    # Attributes
    # ----------
    @attribute(dtype="str")
    def buildState(self: SatUtc) -> str:
        """
        Read the Build State of the device.

        :return: the build state of the device
        """
        return self._version

    @attribute(dtype="str")
    def versionId(self: SatUtc) -> str:
        """
        Read the Version Id of the device.

        :return: the version id of the device
        """
        return self._version_id

    @attribute(dtype=str)
    def subdevice_healths(self: SatUtc) -> str:
        """
        Read the healths of utc sub-devices.

        :return: health of the sat utc sub-devices
        """
        return json.dumps(self.component_manager.get_healths())

    @attribute(dtype="DevString")
    def healthReport(self: SatUtc) -> str:
        """
        Get the health report.

        :return: the health report.
        """
        return self._health_report


# ----------
# Run server
# ----------


def main(*args: str, **kwargs: str) -> int:
    """
    Entry point for module.

    :param args: positional arguments
    :param kwargs: named arguments

    :return: exit code
    """
    return SatUtc.run_server(args=args or None, **kwargs)


if __name__ == "__main__":
    main()
