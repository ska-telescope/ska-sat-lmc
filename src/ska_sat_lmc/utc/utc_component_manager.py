# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module implements component management for the SAT.LMC controller."""

from __future__ import annotations

import functools
import logging
import threading
from typing import Callable, Iterable

from ska_control_model import CommunicationStatus, HealthState

from ska_sat_lmc.component import DeviceComponentManager, SatComponentManager

__all__ = ["UtcComponentManager"]


class _WhiteRabbitProxy(DeviceComponentManager):
    """A proxy to a whiterabbit device."""

    def __init__(
        self: _WhiteRabbitProxy,
        trl: str,
        logger: logging.Logger,
        communication_state_callback: Callable[[CommunicationStatus], None],
        component_state_callback: Callable[..., None],
    ) -> None:
        super().__init__(
            trl,
            logger,
            communication_state_callback,
            component_state_callback,
        )


class UtcComponentManager(SatComponentManager):
    """A component manager for an SAT LMC controller."""

    def __init__(
        self: UtcComponentManager,
        whiterabbit_trls: Iterable[str],
        logger: logging.Logger,
        communication_state_changed_callback: Callable[[CommunicationStatus], None],
        component_state_changed_callback: Callable[..., None],
    ) -> None:
        """
        Initialise a new instance.

        :param whiterabbit_trls: TRL's  of all whiterabbit devices
        :param logger: the logger to be used by this object.
        :param communication_state_changed_callback: callback to be
            called when the state of the communications channel between
            the component manager and its component changes
        :param component_state_changed_callback: callback to be called
            when state changes
        """
        self._communication_state_changed_callback = (
            communication_state_changed_callback
        )
        self._component_state_changed_callback = component_state_changed_callback
        self.__communication_lock = threading.Lock()
        self._logger = logger

        self._device_communication_states: dict[str, CommunicationStatus] = {}
        for trl in whiterabbit_trls:
            self._device_communication_states[trl] = CommunicationStatus.DISABLED

        self._whiterabbits: dict[str, _WhiteRabbitProxy] = {
            trl: _WhiteRabbitProxy(
                trl,
                logger,
                functools.partial(self._device_communication_state_changed, trl),
                functools.partial(component_state_changed_callback, trl=trl),
            )
            for trl in whiterabbit_trls
        }
        super().__init__(
            logger,
            communication_state_changed_callback,
            component_state_changed_callback,
            fault=None,
        )

    def start_communicating(self: UtcComponentManager) -> None:
        """Establish communication with its components."""
        if self.communication_state == CommunicationStatus.ESTABLISHED:
            return
        if self.communication_state == CommunicationStatus.DISABLED:
            self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)

        if not self._device_communication_states:
            self._update_communication_state(CommunicationStatus.ESTABLISHED)
        else:
            for proxy in self._whiterabbits.values():
                proxy.start_communicating()

    def stop_communicating(self: UtcComponentManager) -> None:
        """Break off communication with its components."""
        if self.communication_state == CommunicationStatus.DISABLED:
            return
        self._update_communication_state(CommunicationStatus.DISABLED)
        self._update_component_state(fault=None)

        for proxy in self._whiterabbits.values():
            proxy.stop_communicating()

    def _device_communication_state_changed(
        self: UtcComponentManager,
        trl: str,
        communication_state: CommunicationStatus,
    ) -> None:
        """
        Handle communication changes.

        :param trl: TRL of changed device
        :param communication_state: new status
        """
        if trl not in self._device_communication_states:
            self.logger.warning(
                f"Received a communication status changed event for device {trl} "
                "which is not managed by this controller. "
                "Probably it was released just a moment ago. "
                "The event will be discarded."
            )
            return

        self._device_communication_states[trl] = communication_state
        if self.communication_state == CommunicationStatus.DISABLED:
            return
        self._evaluate_communication_state()

    def _evaluate_communication_state(
        self: UtcComponentManager,
    ) -> None:
        # Many callback threads could be hitting this method at the same time, so it's
        # possible (likely) that the GIL will suspend a thread between checking if it
        # need to update, and actually updating. This leads to callbacks appearing out
        # of order, which breaks tests. Therefore we need to serialise access.
        with self.__communication_lock:
            if (
                CommunicationStatus.DISABLED
                in self._device_communication_states.values()
            ):
                self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)
            elif (
                CommunicationStatus.NOT_ESTABLISHED
                in self._device_communication_states.values()
            ):
                self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)
            else:
                self._update_communication_state(CommunicationStatus.ESTABLISHED)
                self._update_component_state(fault=False)

    def _health_changed(
        self: UtcComponentManager,
        trl: str,
        health: HealthState | None,
    ) -> None:
        """
        Handle a change in the health of a sat subsystem.

        :param trl: the TRL of the subsystem.
        :param health: the new health state of the subsystem, or None if
            the health could not be determined.
        """
        if self._component_state_changed_callback is not None:
            self._component_state_changed_callback(trl, health)

    def get_health(self: UtcComponentManager, trl: str) -> HealthState | None:
        """
        Return the health of a subsystem with given TRL.

        :param trl: TRL of subsystem to return health of
        :return: health of subsystem given by TRL
        """
        if trl in self._whiterabbits.keys():
            if (
                self._device_communication_states[trl]
                == CommunicationStatus.ESTABLISHED
            ):
                return self._whiterabbits[trl].health
        return None

    def get_healths(self: UtcComponentManager) -> dict[str, HealthState | None]:
        """
        Return the health of a subsystem with given TRL.

        :return: a list of healths of subsystems
        """
        device_healths: dict[str, HealthState | None] = {}
        for trl in self._whiterabbits:
            health = self.get_health(trl)
            device_healths.update({trl: health})
        return device_healths
