# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module implements an abstract component manager for simple object components."""
from __future__ import annotations  # allow forward references in type hints

import logging
from typing import Callable

from ska_control_model import AdminMode, CommunicationStatus, HealthState
from tango import AttrQuality, DevFailed, DevState

from ska_sat_lmc.device_proxy import SatDeviceProxy

from .component_manager import SatComponentManager

__all__ = ["DeviceComponentManager"]


# pylint: disable=too-many-instance-attributes
class DeviceComponentManager(SatComponentManager):
    """An abstract component manager for a Tango device component."""

    def __init__(
        self: DeviceComponentManager,
        trl: str,
        logger: logging.Logger,
        communication_state_callback: Callable[[CommunicationStatus], None],
        component_state_callback: Callable[..., None],
    ) -> None:
        """
        Initialise a new instance.

        :param trl: the TRL of the device
        :param logger: the logger to be used by this object.
        :param communication_state_callback: callback to be
            called when the status of the communications channel between
            the component manager and its component changes
        :param component_state_callback: callback to be
            called when the component state changes
        """
        self._trl: str = trl
        self._device_proxy: SatDeviceProxy | None = None
        self._logger = logger
        self._component_state_callback = component_state_callback
        self._faulty: bool | None = None
        self._health: HealthState | None = None
        self._device_health_state = HealthState.UNKNOWN
        self._device_admin_mode = AdminMode.OFFLINE
        self._event_callbacks: dict[str, Callable] = {
            "healthState": self._device_health_state_changed,
            "adminMode": self._device_admin_mode_changed,
            "state": self._device_state_changed,
        }
        super().__init__(
            logger,
            communication_state_callback,
            component_state_callback,
            fault=None,
        )

    def start_communicating(self: DeviceComponentManager) -> None:
        """Establish communication with the component, then start monitoring."""
        self._logger.info("Started communicating")
        super().start_communicating()
        self._connect_to_device()

    def _connect_to_device(self: DeviceComponentManager) -> None:
        """Connect to the device and then start monitoring."""
        self._logger.info(f"Connecting to device {self._trl}")

        try:
            self._device_proxy = SatDeviceProxy(self._trl, self._logger, connect=True)
            self._device_proxy.ping()
            self.update_communication_status(CommunicationStatus.ESTABLISHED)

            for event, callback in self._event_callbacks.items():
                self._device_proxy.add_change_event_callback(event, callback)
        except DevFailed:
            self._logger.error(f"Could not connect to device '{self._trl}'")

    def stop_communicating(self: DeviceComponentManager) -> None:
        """Cease monitoring the component, and break off all communication with it."""
        # TODO: Presumably, unsubscriptions occur before the underlying
        # DeviceProxy is deleted. But we should really do this
        # explicitly.
        super().stop_communicating()
        self._device_proxy = None

    @property
    def health(self: DeviceComponentManager) -> HealthState | None:
        """
        Return the evaluated health state of the device.

        This will be either the health state that the device reports, or
        None if the device is in an admin mode that indicates that its
        health should not be rolled up.

        :return: the evaluated health state of the device.
        """
        return self._health

    @health.setter
    def health(self: DeviceComponentManager, value: HealthState) -> None:
        """
        Set the health state of the device.

        :param value: the new healthState value
        """
        self._health = value

    def _device_state_changed(
        self: DeviceComponentManager,
        event_name: str,
        event_value: DevState,
        event_quality: AttrQuality,
    ) -> None:
        """
        Handle an change event on device state.

        :param event_name: name of the event; will always be "state" for this callback
        :param event_value: the new state
        :param event_quality: the quality of the change event
        """
        if event_value == DevState.FAULT:
            self.update_component_state(fault=True)

    def _device_health_state_changed(
        self: DeviceComponentManager,
        event_name: str,
        event_value: HealthState,
        event_quality: AttrQuality,
    ) -> None:
        """
        Handle an change event on device health state.

        :param event_name: name of the event; will always be
            "healthState" for this callback
        :param event_value: the new health state
        :param event_quality: the quality of the change event
        """
        self._device_health_state = event_value
        self._update_health()

    def _device_admin_mode_changed(
        self: DeviceComponentManager,
        event_name: str,
        event_value: AdminMode,
        event_quality: AttrQuality,
    ) -> None:
        """
        Handle an change event on device admin mode.

        :param event_name: name of the event; will always be
            "adminMode" for this callback
        :param event_value: the new admin mode
        :param event_quality: the quality of the change event
        """
        self._device_admin_mode = event_value
        self._update_health()

    def _update_health(
        self: DeviceComponentManager,
    ) -> None:
        health = (
            self._device_health_state
            if self._device_admin_mode in [AdminMode.ENGINEERING, AdminMode.ONLINE]
            else None
        )
        if self._health != health:
            self._health = health
            if self._component_state_callback is not None:
                self._component_state_callback(health=self._health)
