# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module implements utils for component managers in SAT.LMC."""

from __future__ import annotations  # allow forward references in type hints

import functools
from typing import Any, Callable, TypeVar, cast

from ska_control_model import CommunicationStatus

from .component_manager import SatComponentManager

__all__ = ["check_communicating"]


Wrapped = TypeVar("Wrapped", bound=Callable[..., Any])


def check_communicating(func: Wrapped) -> Wrapped:
    """
    Return a function that checks component communication before calling a function.

    The component manager needs to have established communications with
    the component, in order for the function to be called.

    This function is intended to be used as a decorator:

    .. code-block:: python

        @check_communicating
        def scan(self):
            ...

    :param func: the wrapped function

    :return: the wrapped function
    """

    @functools.wraps(func)
    def _wrapper(
        component_manager: SatComponentManager,
        *args: Any,
        **kwargs: Any,
    ) -> Any:
        """
        Check for component communication before calling the function.

        This is a wrapper function that implements the functionality of
        the decorator.

        :param component_manager: the component manager to check
        :param args: positional arguments to the wrapped function
        :param kwargs: keyword arguments to the wrapped function
        :raises ConnectionError: if communication with the component has
            not been established.
        :return: whatever the wrapped function returns
        """
        if component_manager.communication_status != CommunicationStatus.ESTABLISHED:
            raise ConnectionError("Not connected")
        return func(component_manager, *args, **kwargs)

    return cast(Wrapped, _wrapper)
