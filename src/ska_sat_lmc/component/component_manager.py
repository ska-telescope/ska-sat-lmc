# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module implements a functionality for component managers in SAT.LMC."""

from __future__ import annotations  # allow forward references in type hints

import logging
import threading
from typing import Any, Callable

from ska_control_model import CommunicationStatus, TaskStatus
from ska_tango_base.base import BaseComponentManager

from ska_sat_lmc.utils import threadsafe

__all__ = ["SatComponentManager"]


class SatComponentManager(BaseComponentManager):
    """
    A component manager for Sat LMC.

    Therefore this class accepts two callback arguments: one for when
    communication with the component changes and one for when the
    component state changes. In the last case, callback hooks are
    provided so that the component can indicate the change to this
    component manager.
    """

    def __init__(
        self: SatComponentManager,
        logger: logging.Logger,
        communication_status_callback: Callable,
        component_state_callback: Callable,
        **kwargs: Any,
    ):
        """
        Initialise a new instance.

        :param logger: a logger for this object to use
        :param communication_status_callback: callback to be called when
            the state of communications between the component manager
            and its component changes.
        :param component_state_callback: callback to be called when the
            state of the component changes.
        :param kwargs: other keyword args
        """
        self._logger = logger
        self.__communication_lock = threading.Lock()
        self._communication_status = CommunicationStatus.DISABLED
        self._communication_status_callback = communication_status_callback
        self._fault: bool | None = None
        self._component_state_callback = component_state_callback
        super().__init__(
            logger,
            communication_status_callback,
            component_state_callback,
            **kwargs,
        )

    def start_communicating(self: SatComponentManager) -> None:
        """Start communicating with the component."""
        if self._communication_status == CommunicationStatus.ESTABLISHED:
            return
        if self._communication_status == CommunicationStatus.DISABLED:
            self.update_communication_status(CommunicationStatus.DISABLED)
        # It's up to subclasses to set communication status to ESTABLISHED via a call
        # to update_communication_status()

    def stop_communicating(self: SatComponentManager) -> None:
        """Break off communicating with the component."""
        if self._communication_status == CommunicationStatus.DISABLED:
            return

        self.update_communication_status(CommunicationStatus.DISABLED)
        self.update_component_state(False)

    @threadsafe
    def update_communication_status(
        self: SatComponentManager,
        communication_status: CommunicationStatus,
    ) -> None:
        """
        Handle a change in communication state.

        This is a helper method for use by subclasses.

        :param communication_status: the new communication status of the
            component manager.
        """
        with self.__communication_lock:
            if self._communication_status != communication_status:
                self._logger.info(
                    "Updating  communication status from "
                    f"{self._communication_status.name} "
                    f"to {communication_status.name}"
                )
                self._communication_status = communication_status
                if self._communication_status_callback is not None:
                    self._communication_status_callback(communication_status)

    @property
    def is_communicating(self: SatComponentManager) -> bool:
        """
        Return communication with the component is established.

        SatLmc uses the more expressive :py:attr:`communication_status`
        for this, but this is still needed as a base classes hook.

        :return: whether communication with the component is
            established.
        """
        return self._communication_status == CommunicationStatus.ESTABLISHED

    @property
    def communication_status(self: SatComponentManager) -> CommunicationStatus:
        """
        Return the communication status of this component manager.

        This is implemented as a replacement for the
        ``is_communicating`` property, which should be deprecated.

        :return: status of the communication channel with the component.
        """
        return self._communication_status

    @threadsafe
    def update_component_state(self: SatComponentManager, fault: bool | None) -> None:
        """
        Update the component fault status, calling callbacks as required.

        This is a helper method for use by subclasses.

        :param fault: whether the component has faulted. If ``False``,
            then this is a notification that the component has
            *recovered* from a fault.
        """
        if self._fault != fault:
            self._fault = fault
            if self._component_state_callback is not None and fault is not None:
                self._component_state_callback(fault=fault)

    def component_state_changed(self: SatComponentManager, fault: bool) -> None:
        """
        Handle notification that the component's fault status has changed.

        This is a callback hook, to be passed to the managed component.

        :param fault: whether the component has faulted. If ``False``,
            then this is a notification that the component has
            *recovered* from a fault.
        """
        self.update_component_state(fault)

    @property
    def fault(self: SatComponentManager) -> bool | None:
        """
        Return whether this component manager is currently experiencing a fault.

        :return: whether this component manager is currently
            experiencing a fault.
        """
        return self._fault

    def off(
        self: SatComponentManager, task_callback: Callable | None = None
    ) -> tuple[TaskStatus, str]:
        """
        Turn the component off.

        :param task_callback: callback to be called when the status of
            the command changes

        :return: the completed status and not implemented message
        """
        return (TaskStatus.COMPLETED, "Off is not implemented")

    def standby(
        self: SatComponentManager, task_callback: Callable | None = None
    ) -> tuple[TaskStatus, str]:
        """
        Put the component into low-power standby mode.

        :param task_callback: callback to be called when the status of
            the command changes

        :return: the completed status and not implemented message
        """
        return (TaskStatus.COMPLETED, "Standby is not implemented")

    def on(
        self: SatComponentManager, task_callback: Callable | None = None
    ) -> tuple[TaskStatus, str]:
        """
        Turn the component on.

        :param task_callback: callback to be called when the status of
            the command changes

        :return: the completed status and not implemented message
        """
        return (TaskStatus.COMPLETED, "On is not implemented")

    def reset(
        self: SatComponentManager, task_callback: Callable | None = None
    ) -> tuple[TaskStatus, str]:
        """
        Reset the component (from fault state).

        :param task_callback: callback to be called when the status of
            the command changes

        :return: the completed status and not implemented message
        """
        return (TaskStatus.COMPLETED, "Reset is not implemented")

    def abort_tasks(
        self: SatComponentManager, task_callback: Callable | None = None
    ) -> tuple[TaskStatus, str]:
        """
        Abort all tasks queued & running.

        :param task_callback: callback to be called whenever the status
            of the task changes.

        :return: the completed status and no implemented message
        """
        return (TaskStatus.COMPLETED, "abort_commands is not implemented")
