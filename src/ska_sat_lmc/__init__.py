# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This package implements SKA SAT LMC subsystem."""

__version__ = "0.4.0"

__all__ = [
    # devices
    "SatController",
    "SatUtc",
    "SatMaser",
    "SatPhaseMicroStepper",
    "SatWhiteRabbit",
    "WhiteRabbitSimulator",
    # device subpackages
    "controller",
    "utc",
    "whiterabbit",
]

from .clocks.maser import SatMaser
from .clocks.phasemicrostepper import SatPhaseMicroStepper
from .controller.controller_device import SatController
from .utc.utc_device import SatUtc
from .whiterabbit import SatWhiteRabbit, WhiteRabbitSimulator
