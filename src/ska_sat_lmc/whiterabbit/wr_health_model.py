# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""An implementation of a health model for a SatWhiteRabbit."""

from __future__ import annotations

import logging
from enum import Enum
from typing import Any, Callable

from ska_control_model import HealthState

from ska_sat_lmc.health import HealthModel

__all__ = ["WhiteRabbitHealthModel", "WRState"]


class WRState(Enum):
    """Define error conditions from SNMP Enum type."""

    OK = 1
    WARNING = 2
    CRITICAL = 3


class WhiteRabbitHealthModel(HealthModel):
    """A health model for a white rabbit."""

    def __init__(
        self: WhiteRabbitHealthModel,
        component_changed_callback: Callable[[Any], None],
        logger: logging.Logger,
    ) -> None:
        """
        Initialise a new instance.

        :param component_changed_callback: callback to be called whenever
            there is a change to this health model's evaluated health
            state.
        :param logger: the logger
        """
        self._running = False
        super().__init__(component_changed_callback, logger)
        self._logger = logger

    def evaluate_health(self: WhiteRabbitHealthModel) -> tuple[HealthState, str]:
        """
        Compute overall health of the whiterabbit.

        As the attributes general_status, timing_status & system_status are enum's
        the value of the of each will also determine the healthstate as well as any
        attribute outside the min/max alarm levels. The only time HealthState is UNKNOWN
        is before the server has communicated with the SNMP device. Once communication
        has been established and it fails then HealthState will be FAILED.

        :return: the overall health & a health report
        """
        if not self._state["communicating"]:
            if not self._running:
                return HealthState.UNKNOWN, "Device has not connected yet"
            return HealthState.FAILED, "Device has disconnected"
        self._running = True

        healthmap: dict[Enum, tuple[HealthState, str]] = {
            WRState.WARNING: (HealthState.DEGRADED, "warning"),
            WRState.CRITICAL: (HealthState.FAILED, "critical"),
        }
        report = ""
        overall_health = HealthState.OK
        attrs = ["general_status", "timing_status", "system_status"]
        for health, health_state in healthmap.items():
            for attr in attrs:
                if attr in self._state and self._state[attr] == health:
                    report += f"{attr} reports {health_state[1]}, "
                    overall_health = health_state[0]
        if report:
            return overall_health, report[:-2]
        return HealthState.OK, "Health is OK"
