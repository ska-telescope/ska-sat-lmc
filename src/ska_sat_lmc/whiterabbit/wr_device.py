# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module implements the White Rabbit device."""

from __future__ import annotations  # allow forward references in type hints

import importlib.resources
import sys
from typing import Any, cast

from ska_attribute_polling.attribute_polling_device import AttributePollingDevice
from ska_control_model import CommunicationStatus, HealthState, PowerState
from ska_snmp_device.definitions import load_device_definition, parse_device_definition
from ska_snmp_device.snmp_component_manager import SNMPComponentManager
from tango import Attribute
from tango.server import attribute, device_property

from ska_sat_lmc.whiterabbit.wr_health_model import WhiteRabbitHealthModel

__all__ = ["SatWhiteRabbit", "main"]


class SatWhiteRabbit(AttributePollingDevice):
    """An implementation of a White Rabbit Tango device for SatLmc."""

    Model = device_property(dtype=str, mandatory=True)
    Host = device_property(dtype=str, mandatory=True)
    Port = device_property(dtype=int, default_value=161)
    V2Community = device_property(dtype=str)
    V3UserName = device_property(dtype=str)
    V3AuthKey = device_property(dtype=str)
    V3PrivKey = device_property(dtype=str)
    MaxObjectsPerSNMPCmd = device_property(dtype=int, default_value=24)
    UpdateRate = device_property(dtype=float, default_value=3.0)

    DeviceModels: dict[str, str] = {
        "SWITCH": "switch.yaml",
        "ZEN-TP-FL": "node.yaml",
        "Z16-SWITCH-V5_2": "z16-switch.yaml",
        "Z16-SWITCH-V5_0": "z16-switch-v5_0.yaml",
        "Z16-ZEN-TP-FL": "z16-node.yaml",
    }

    # ---------------
    # Initialisation
    # ---------------
    def __init__(
        self: SatWhiteRabbit,
        *args: Any,
        **kwargs: Any,
    ) -> None:
        """
        Initialise a new instance.

        :param args: positional arguments.
        :param kwargs: keyword arguments.
        """
        # This __init__ method is created for type-hinting purposes only.
        # Tango devices are not supposed to have __init__ methods,
        # And they have a strange __new__ method,
        # that calls __init__ when you least expect it.
        # So don't put anything executable in here
        # (other than the super() call).
        super().__init__(*args, **kwargs)
        self._dynamic_attrs: dict[str, Attribute]
        self._health_state: HealthState
        self._health_model: WhiteRabbitHealthModel

    def init_device(self: SatWhiteRabbit) -> None:
        """Initialise the device."""
        try:
            super().init_device()
            self._version_id = sys.modules["ska_sat_lmc"].__version__
            device_name = f'{str(self.__class__).rsplit(".", maxsplit=1)[-1][0:-2]}'
            version = f"{device_name} Software Version: {self._version_id}"
            properties = f"Initialised {device_name} on: {self.Host}:{self.Port}"
            version_info = f"{self.__class__.__name__}, {self._build_state}"
            self.logger.info("\n%s\n%s\n%s", version_info, version, properties)
            self.logger.info("Init device complete")
        # pylint: disable=broad-exception-caught
        except Exception as ex:
            self.logger.error(f"Initialise failed: Incomplete server: {repr(ex)}")

    def delete_device(self: SatWhiteRabbit) -> None:
        """Delete the device."""
        try:
            self.logger.info("Deleting device")
            self.component_manager.stop_communicating()
        # pylint: disable=broad-exception-caught
        except Exception as ex:
            self.logger.error(f"Failed to delete device {repr(ex)}")

    def _init_state_model(self: SatWhiteRabbit) -> None:
        """Initialise the state model."""
        super()._init_state_model()
        self._health_state = HealthState.UNKNOWN
        self._health_model = WhiteRabbitHealthModel(
            self._component_state_changed, self.logger
        )
        self.set_change_event("healthState", True, False)
        self.set_archive_event("healthState", True, False)

    def create_component_manager(self) -> SNMPComponentManager:
        """
        Create and return a component manager.

        :return: SNMPComponent manager
        """
        filename = self.DeviceModels[self.Model]
        device_definition = importlib.resources.files(
            "ska_sat_lmc.whiterabbit.device_definitions"
        ).joinpath(filename)
        # This goes here because you don't have access to properties
        # until tango.server.BaseDevice.init_device() has been called
        dynamic_attrs = parse_device_definition(
            load_device_definition(str(device_definition), None)
        )
        self._dynamic_attrs = {attr.name: attr for attr in dynamic_attrs}
        assert (self.V2Community and not self.V3UserName) or (
            not self.V2Community and self.V3UserName
        ), "Can't be V2 & V3 simultaneously"

        if self.V2Community:
            authority = self.V2Community
        else:
            authority = {
                "auth": self.V3UserName,
                "authKey": self.V3AuthKey,
                "privKey": self.V3PrivKey,
            }

        return SNMPComponentManager(
            host=self.Host,
            port=self.Port,
            authority=authority,
            max_objects_per_pdu=self.MaxObjectsPerSNMPCmd,
            logger=self.logger,
            communication_state_callback=self._communication_state_changed,
            component_state_callback=self._component_state_changed,
            attributes=dynamic_attrs,
            poll_rate=self.UpdateRate,
        )

    def _communication_state_changed(
        self: SatWhiteRabbit,
        communication_state: CommunicationStatus,
    ) -> None:
        """
        Handle change in communications status between component manager and component.

        This is a callback hook, called by the component manager when
        the communications status changes. It is implemented here to
        drive the op_state.

        :param communication_state: the status of communications between
            the component manager and its component.
        """
        action_map = {
            CommunicationStatus.DISABLED: "component_disconnected",
            CommunicationStatus.NOT_ESTABLISHED: "component_disconnected",
            CommunicationStatus.ESTABLISHED: "component_on",
        }
        action = action_map[communication_state]
        if action is not None:
            self.op_state_model.perform_action(action)

        info_msg = f"Communication status is {communication_state}"
        self.logger.info(info_msg)
        self._health_model.update_state(
            communicating=communication_state == CommunicationStatus.ESTABLISHED
        )

    def _component_state_changed(
        self: SatWhiteRabbit,
        fault: bool | None = None,
        power: PowerState | None = None,
        **kwargs: Any,
    ) -> None:
        """
        Handle change in the state of the component.

        This is a callback hook, called by the component manager when
        the state of the component changes.

        :param fault: whether the component has faulted or not
        :param power: unused in this implementation
        :param kwargs: state change parameters.
        """
        if "health" in kwargs:
            health = kwargs.pop("health")
            if self._health_state != health:
                # update health_state which is an attribute in the base class
                # change and archive events are pushed there
                self._update_health_state(health)
        # update base class for power and/or fault
        super()._component_state_changed(**kwargs)

        for attribute_name, value in kwargs.items():
            info_msg = f"Updating {attribute_name}, with {value}"
            self.logger.info(info_msg)
        if kwargs:
            # Update health when any attribute value changes
            # and/or communication or fault state changes.
            self._health_model.update_state(**kwargs)

    @attribute(dtype="DevString")
    def healthReport(self: SatWhiteRabbit) -> str:
        """
        Get the health report.

        :return: the health report.
        """
        return self._health_model.health_report


# ----------
# Run server
# ----------


def main(*args: str, **kwargs: str) -> int:  # pragma: no cover
    """
    Entry point for module.

    :param args: positional arguments
    :param kwargs: named arguments
    :return: exit code
    """
    return cast(int, SatWhiteRabbit.run_server(args=args or None, **kwargs))


if __name__ == "__main__":
    main()
