# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This subpackage contains modules for helper classes in the SKA SAT LMC tests."""


__all__ = ["TangoHarness", "tango_harness"]

from .tango_harness import TangoHarness
