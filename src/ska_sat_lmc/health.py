# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module implements infrastructure for health management."""

from __future__ import annotations  # allow forward references in type hints

import logging
from typing import Any, Callable, FrozenSet, Optional

from ska_control_model import HealthState

__all__ = ["HealthModel"]


class HealthModel:
    """
    A simple health model the supports.

    * HealthState.UNKNOWN -- when communication with the component is
      not established.

    * HealthState.FAILED -- when the component has faulted

    * HealthState.OK -- when neither of the above conditions holds.

    This health model does not support HealthState.DEGRADED. It is up to
    subclasses to implement support for DEGRADED if required.
    """

    def __init__(
        self: HealthModel,
        health_changed_callback: Callable[..., None],
        logger: logging.Logger,
        **kwargs: Any,
    ) -> None:
        """
        Initialise a new instance.

        :param health_changed_callback: callback to be called
            whenever there is a change to this this health model's
            evaluated health state.
        :param logger: logging
        :param kwargs: additional keyword arguments specifying initial
            values for state.
        """
        self._logger = logger
        self._logger.info(kwargs)
        self._state: dict[str, Any] = {
            "communicating": False,
            "fault": False,
            **kwargs,
        }
        self._fault = False
        self._health_state, self._health_report = self.evaluate_health()
        self._health_changed_callback = health_changed_callback
        self._health_changed_callback(health=self._health_state)
        if not hasattr(self, "_health_rules"):
            self._health_rules = HealthRules()

    @property
    def health_state(self: HealthModel) -> HealthState:
        """
        Return the health state.

        :return: the health state.
        """
        return self._health_state

    @property
    def health_report(self: HealthModel) -> str:
        """
        Return the health report.

        This is a short string to explain why the device is in its current health state.

        :return: a health report
        """
        return self._health_report

    def update_health(self: HealthModel) -> None:
        """
        Update health state.

        This method calls the :py:meth:``evaluate_health`` method to
        figure out what the new health state should be, and then updates
        the ``health_state`` attribute, calling the callback if
        required.
        """
        health_state, health_report = self.evaluate_health()
        self._logger.debug(
            f"Evaluated new health as {health_state}"
            f" old health {self._health_state}"
        )
        if self._health_state != health_state:
            self._health_state = health_state
            self._health_report = health_report
            self._health_changed_callback(health=self._health_state)

    def evaluate_health(self: HealthModel) -> tuple[HealthState, str]:
        """
        Re-evaluate the health state.

        This method contains the logic for evaluating the health. It is
        this method that should be extended by subclasses in order to
        define how health is evaluated by their particular device.

        :return: the new health state.
        """
        if not self._state["communicating"]:
            return HealthState.UNKNOWN, "Device has not connected."
        if self._state["fault"]:
            return HealthState.FAILED, "Device is in fault state"
        return HealthState.OK, "Health is OK."

    def update_state(self: HealthModel, **kwargs: Any) -> None:
        """
        Update this health model with state relevant to evaluating health.

        :param kwargs: updated state
        """
        self._state.update(**kwargs)
        self.update_health()

    def component_fault(self: HealthModel, fault: bool) -> None:
        """
        Handle a component experiencing or recovering from a fault.

        This is a callback hook that is called when the component goes
        into or out of FAULT state.

        :param fault: whether the component has faulted or not
        """
        self._state["fault"] = fault
        self.update_health()

    def is_communicating(self: HealthModel, communicating: bool) -> None:
        """
        Handle change in communication with the component.

        :param communicating: whether communications with the component
            is established.
        """
        self._state["communicating"] = communicating
        self.update_health()

    @property
    def health_params(self: HealthModel) -> dict[str, float]:
        """
        Get the thresholds for health rules.

        :return: the thresholds for health rules
        """
        return self._health_rules._thresholds

    @health_params.setter
    def health_params(self: HealthModel, params: dict[str, float]) -> None:
        """
        Set the thresholds for health rules.

        :param params: A dictionary of parameters with the param name as
            key and threshold as value
        """
        self._health_rules._thresholds = self._health_rules.default_thresholds | params


class HealthRules:
    """
    A class to store health rules.

    This should be implemented by each device with health roll-up logic.
    """

    DEGRADED_THRESHOLD = 0.05
    FAILED_THRESHOLD = 0.2

    def __init__(
        self: HealthRules, thresholds: Optional[dict[str, float]] = None
    ) -> None:
        """
        Create a new instance.

        :param thresholds: the conditions for particular health states
        """
        if thresholds is None:
            self._thresholds = self.default_thresholds
        else:
            self._thresholds = self.default_thresholds | thresholds

    def get_fraction_in_states(
        self: HealthRules,
        device_dict: dict[str, HealthState | None],
        states: FrozenSet[HealthState | None],
    ) -> float:
        """
        Get the fraction of devices in a given list of states.

        :param device_dict: dictionary of devices, key fqdn and value
            health
        :param states: the states to check
        :return: the fraction of the devices in the given states
        """
        return self.get_count_in_states(device_dict, states) / float(len(device_dict))

    def get_count_in_states(
        self: HealthRules,
        device_dict: dict[str, HealthState | None],
        states: FrozenSet[HealthState | None],
    ) -> int:
        """
        Get the number of devices in a given list of state.

        :param device_dict: dictionary of devices, key fqdn and value
            health
        :param states: the states to check
        :return: the number of the devices in the given states
        """
        return sum(map(lambda s: s in states, device_dict.values()))

    @property
    def default_thresholds(self: HealthRules) -> dict[str, float]:
        """
        Get the default thresholds for this device.

        This should be overriden in a derived class to provide default
        thresholds.

        :return: an empty dictionary
        """
        return {}

    def failed_rule(self: HealthRules) -> bool:
        """
        Rule for the FAILED state.

        :raises NotImplementedError: must be implemented in derived
            class
        """
        raise NotImplementedError("HealthRules is abstract")

    def degraded_rule(self: HealthRules) -> bool:
        """
        Rule for the DEGRADED state.

        :raises NotImplementedError: must be implemented in derived
            class
        """
        raise NotImplementedError("HealthRules is abstract")

    def unknown_rule(self: HealthRules) -> bool:
        """
        Rule for the UNKNOWN state.

        :raises NotImplementedError: must be implemented in derived
            class
        """
        raise NotImplementedError("HealthRules is abstract")

    def healthy_rule(self: HealthRules) -> bool:
        """
        Rule for the OK state.

        :raises NotImplementedError: must be implemented in derived
            class
        """
        raise NotImplementedError("HealthRules is abstract")

    @property
    def rules(self: HealthRules) -> dict[HealthState, Callable[..., bool]]:
        """
        Get the transition rules for the station.

        The rules must be implemented on a device-by-device basis.

        :return: the transition rules for the station
        """
        return {
            HealthState.FAILED: self.failed_rule,
            HealthState.DEGRADED: self.degraded_rule,
            HealthState.OK: self.healthy_rule,
            HealthState.UNKNOWN: self.unknown_rule,
        }
