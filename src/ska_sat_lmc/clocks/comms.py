# This file is part of the SKA SAT LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module implements the Socket communication."""

from __future__ import annotations  # allow forward references in type hints

__all__ = ["parse_url", "TcpSocket", "UdpSocket"]

import functools
import logging
import re
import socket
import threading
import time
import weakref
from typing import Any, Callable, TypeVar, cast

Wrapped = TypeVar("Wrapped", bound=Callable[..., Any])


class SocketCommunicationError(RuntimeError):
    """Socket communication error."""


class SocketTimeout(RuntimeError):
    """Socket timeout error."""


def try_connect_socket(func: Wrapped) -> Wrapped:
    """
    Use as a decorator function for read/write functions.

    :param func: function to be wrapped
    :return: wrapped function
    """

    @functools.wraps(func)
    def rfunc(self: BaseSocket, *args: Any, **kwargs: Any) -> Any:
        """
        Perform reading of data via "_raw_read_task" in self.connect().

        This is a wrapper function that implements the functionality of
        the decorator.

        :raises SocketCommunicationError: connection problem
        :param self: the BaseSocket
        :param args: positional arguments to the wrapped function
        :param kwargs: keyword arguments to the wrapped function
        :return: the wrapped function
        """
        write_func = func.__name__.startswith("write")
        prev_timeout = kwargs.get("timeout", None)

        if (not self._connected) and ((not self._data) or write_func):
            self.connect(timeout=prev_timeout)

        if not self._connected:
            kwargs.update({"timeout": 0.0})
            try:
                return func(self, *args, **kwargs)
            except SocketTimeout:
                self.connect(timeout=prev_timeout)
                kwargs.update({"timeout": prev_timeout})

        try:
            return func(self, *args, **kwargs)
        except Exception as exc:
            try:
                self.close()
            # pylint: disable=broad-exception-caught
            except Exception:
                pass
            raise SocketCommunicationError from exc

    return cast(Wrapped, rfunc)


# pylint: disable=too-many-instance-attributes
class BaseSocket:
    """Base socket class to provide tcp or udp raw socket access."""

    # pylint: disable=too-many-arguments
    def __init__(
        self: BaseSocket,
        host: str | None = None,
        port: int | None = None,
        eol: bytes = b"\n",  # end of line for each rx message
        timeout: float = 3.0,  # default timeout for read/write
        logger: logging.Logger | None = None,
    ) -> None:
        """
        Initialise the baseclass.

        :param host: the host to connect to
        :param port: the port number to conect to
        :param eol: end of line terminator
        :param timeout: give up after certain time.
        :param logger: logging
        """
        self._host: str | None = host
        self._port: int | None = port
        self._socket: socket.socket | None = None
        self._timeout = timeout
        self._logger = logger
        self._threading: threading.Thread | None = None
        self._connected: bool = False
        self._eol = eol
        self._data = b""
        self._event = threading.Event()
        self._raw_read_task = None
        self._lock = threading.RLock()
        self._thread: threading.Thread

    def __del__(self: BaseSocket) -> None:
        self.close()

    def __str__(self: BaseSocket) -> str:
        return f"{self.__class__.__name__}[{self._host}:{self._port}]"

    @property
    def lock(self: BaseSocket) -> threading.RLock:
        """
        Return the thread lock.

        :return: the thread lock
        """
        return self._lock

    @property
    def is_connected(self: BaseSocket) -> bool:
        """
        Return the state of the connection.

        :return: True if connected else False
        """
        return self._connected

    def connect(
        self: BaseSocket,
        host: str | None = None,
        port: int | None = None,
        timeout: float | None = None,
    ) -> None:
        """
        Create a socket on the host at a given port.

        :param host: the host to connect to
        :param port: the port number to conect to
        :param timeout: give up after certain time.
        :return: the socket descriptor
        """
        self._host = host or self._host
        self._port = port or self._port
        # local_timeout = timeout if timeout is not None else self._timeout
        assert self._host is not None  # for the type checker
        assert self._port is not None  # for the type checker
        assert self._logger is not None  # for the type checker

        self.close()
        if not self._socket:
            with self._lock:
                try:
                    self._socket = self._connect(self._host, self._port)
                    self._connected = True
                    self._logger.info(f"Connection on socket({self._socket})")
                # pylint: disable=broad-exception-caught
                except Exception as ex:
                    print(ex)
                    self._logger.error(
                        f"Connection timeout on socket({self._host}, {self._port})"
                    )
                    self._connected = False
                    return

        self._thread = threading.Thread(
            target=self._raw_read,
            args=(weakref.proxy(self), self._socket),
            daemon=True,
        )
        self._thread.start()

    def _connect(self: BaseSocket, host: str, port: int) -> socket.socket:
        """
        Create a socket on the host at a given port.

        :param host: the host to connect to
        :param port: the port number to conect to
        :raises NotImplementedError: an abstract class
        """
        raise NotImplementedError

    def close(self: BaseSocket) -> None:
        """Close the socket."""
        if self._connected:
            try:
                self._shutdown()
            # TODO: Fix except-pass, it's a dangerous pattern
            # pylint: disable=broad-exception-caught
            except Exception:  # probably closed one the server side
                pass
            try:
                if self._socket is not None:
                    self._socket.close()
            finally:
                if self._thread:
                    self._thread = cast(threading.Thread, None)
                self._data = b""
                self._connected = False
                self._socket = None

    def _shutdown(self: BaseSocket) -> None:
        """Shut down one or both halves of the connection."""
        assert self._socket  # for the type checker
        self._socket.shutdown(socket.SHUT_RDWR)

    @try_connect_socket
    def readline(
        self: BaseSocket,
        eol: str | bytes | None = None,
        timeout: float | None = None,
    ) -> bytes:
        """
        Read a line of data from a socket.

        :param eol: end of line terminator
        :param timeout: time before connection is broken
        :return: a byte array read from the socket
        """
        return self._readline(eol, timeout)

    def _readline(
        self: BaseSocket,
        eol: str | bytes | None = None,
        timeout: float | None = None,
    ) -> bytes:
        """
        Read a line of data from a socket (internal only).

        :param eol: end of line terminator
        :param timeout: time before connection is broken
        :raises SocketCommunicationError: connection problem
        :return: a byte array read from the socket
        """
        assert self._logger is not None  # for the type checker
        local_eol = eol or self._eol
        if not isinstance(local_eol, bytes):
            local_eol = local_eol.encode()
        eol_pos = self._data.find(local_eol)
        while eol_pos == -1:
            if not self._event.wait(timeout or self._timeout):
                self._logger.error(f"timeout on socket({self._host}:{self._port})")
                self._event.clear()
                return b""

            self._event.clear()
            eol_pos = self._data.find(local_eol)
            if not self._connected:
                raise SocketCommunicationError

        msg = self._data[:eol_pos]
        self._data = self._data[eol_pos + len(local_eol) :]
        return msg

    @try_connect_socket
    def write(self: BaseSocket, msg: bytes, timeout: float | None = 3.0) -> None:
        """
        Write data to a socket.

        :param msg: data to write to socket
        :param timeout: time before connection is broken
        """
        with self._lock:
            self._sendall(msg)

    def _write(self: BaseSocket, msg: bytes, timeout: float | None = 3.0) -> None:
        """
        Write data to a socket (internal only).

        :param msg: data to write to socket
        :param timeout: time before connection is broken
        """
        self._sendall(msg)

    @try_connect_socket
    def write_readline(
        self: BaseSocket,
        msg: bytes,
        eol: str | bytes | None = None,
        timeout: float | None = None,
    ) -> bytes:
        """
        Write and then read data to/from a socket.

        :param msg: data to write to socket
        :param eol: end of line terminator
        :param timeout: time before connection is broken
        :return: a byte array read from the socket
        """
        with self._lock:
            if self._connected:
                self._sendall(msg)
                return self.readline(eol=eol, timeout=timeout)
            return b""

    @try_connect_socket
    def write_readlines(
        self: BaseSocket,
        msg: bytes,
        nb_lines: int,
        eol: str | bytes | None = None,
        timeout: float | None = None,
    ) -> bytes:
        """
        Write and then read data to/from a socket.

        :param msg: data to write to socket
        :param nb_lines: number of lines to read
        :param eol: end of line terminator
        :param timeout: time before connection is broken
        :return: a byte array read from the socket
        """
        with self._lock:
            self._sendall(msg)
            reply = b""
            for i in range(nb_lines):
                reply = reply + self.readline(eol=eol, timeout=timeout)

            return reply

    def flush(self: BaseSocket) -> None:
        """Flush the data buffer."""
        self._data = b""

    def _sendall(self: BaseSocket, data: bytes) -> None:
        """
        Send data buffer to server.

        :param data: array of bytes to send
        :raises NotImplementedError: abstract class
        """
        raise NotImplementedError

    @staticmethod
    def _raw_read(sock: BaseSocket, fd: socket.socket) -> None:
        """
        Read data from the socket and store in buffer.

        :param sock: base class
        :param fd: socket to read from
        :raises NotImplementedError: abstract class
        """
        raise NotImplementedError


class TcpSocket(BaseSocket):
    """
    Socket communication class to access raw socket layer using TCP protocol.

    :Example:
        from comms import TcpSocket
        sock = TcpSocket("tcp://127.0.0.1:45678")
    """

    def _connect(self: TcpSocket, host: str, port: int) -> socket.socket:
        """
        Create a tcp socket to send/recv data from.

        :param host: the host to connect to
        :param port: the port number to conect to
        :return: the socket descriptor
        """
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        sock.setsockopt(socket.SOL_IP, socket.IP_TOS, 0x10)
        sock.connect((host, port))
        return sock

    @staticmethod
    def _raw_read(sock: BaseSocket, fd: socket.socket) -> None:
        """
        Read data from the socket and store in buffer.

        :param sock: base class
        :param fd: socket to read from
        """
        try:
            while True:
                raw_data = fd.recv(16 * 1024)
                if raw_data:
                    sock._data += raw_data
                    sock._event.set()
                    time.sleep(sock._timeout)
                else:
                    break
        # pylint: disable=broad-exception-caught
        except Exception:
            pass
        finally:
            try:
                sock.close()
                sock._thread = cast(threading.Thread, None)
                sock._connected = False
                sock._socket = None
            except socket.error:
                pass
            except ReferenceError:
                pass

    def _sendall(self: BaseSocket, data: bytes) -> None:
        """
        Send data buffer to server.

        :param data: array of bytes to send
        """
        assert self._socket is not None  # for the type checker
        self._socket.sendall(data)


class UdpSocket(BaseSocket):
    """
    Socket communication class to access raw socket layer using UDP protocol.

    :example:
        from comms import UdpSocket
        sock = UdpSocket("udp://127.0.0.1:45678")
    """

    def _connect(self: UdpSocket, host: str, port: int) -> socket.socket:
        """
        Create a UDP socket to send/recv data from.

        :param host: the host to connect to
        :param port: the port number to conect to
        :return: the socket descriptor
        """
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_IP, socket.IP_TOS, 0x10)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # This line is not needed for maser simulator
        # sock.bind(("0.0.0.0", port))
        return sock

    @staticmethod
    def _raw_read(sock: BaseSocket, fd: socket.socket) -> None:
        """
        Read data from the socket and store in buffer.

        :param sock: base class
        :param fd: socket to read from
        """
        try:
            while True:
                raw_data, addr = fd.recvfrom(16 * 1024)
                if raw_data and addr[0] == sock._host:
                    sock._data += raw_data
                    sock._event.set()
                    time.sleep(1.0)
                else:
                    break
        # pylint: disable=broad-exception-caught
        except Exception as ex:
            print(repr(ex))
        finally:
            try:
                sock.close()
                sock._connected = False
                sock._socket = None
                sock._event.set()
            except ReferenceError:
                pass

    def _sendall(self: BaseSocket, data: bytes) -> None:
        """
        Send data buffer to server.

        :param data: array of bytes to send
        """
        assert self._socket is not None  # for the type checker
        self._socket.sendto(data, (self._host, self._port))


def parse_url(url: str) -> tuple[str, int]:
    """
    Parse the url string into component host & port.

    :param url: an encoded  string containing host, port and socket type
    :raises ValueError: invalid URL specified
    :return: the decoded host & port
    """
    if url.lower().startswith("tcp://"):
        parse = re.compile(r"^(tcp://)?([^:/]+?):([0-9]+)$")
        match = parse.match(url)
        if match is None:
            raise ValueError(f"Invalid TCP url ({url}) specified")
        host, port = match.group(2), int(match.group(3))
        return (host, port)
    if url.lower().startswith("udp://"):
        parse = re.compile(r"^(udp://)?([^:/]+?):([0-9]+)$")
        match = parse.match(url)
        if match is None:
            raise ValueError(f"Invalid UDP url ({url}) specified")
        host, port = match.group(2), int(match.group(3))
        return (host, port)
    raise ValueError(f"Invalid url ({url}) does not specify either tcp or udp")
