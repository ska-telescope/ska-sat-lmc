# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module implements the SatMaser device."""

from __future__ import annotations  # allow forward references in type hints

import sys
from typing import Any, cast

import tango
from ska_control_model import CommunicationStatus, HealthState, ResultCode
from ska_tango_base.base import SKABaseDevice
from ska_tango_base.commands import DeviceInitCommand
from tango.server import attribute, device_property

from ska_sat_lmc.clocks.maser.maser_driver import MaserDriver
from ska_sat_lmc.clocks.maser.maser_health_model import MaserHealthModel

DevVarLongStringArrayType = tuple[list[ResultCode], list[str | None]]

__all__ = ["SatMaser", "main"]


# pylint: disable=too-many-public-methods
class SatMaser(SKABaseDevice[MaserDriver]):
    """An implementation of a Maser Tango device for SatLmc."""

    # -----------------
    # Device Properties
    # -----------------
    MaserHost = device_property(dtype=str, mandatory=True, doc="The maser host")
    MaserPort = device_property(dtype=int, mandatory=True, doc="The maser port")
    UpdateRate = device_property(dtype=float, default_value=1.0)

    # ---------------
    # Initialisation
    # ---------------
    def __init__(
        self: SatMaser,
        *args: Any,
        **kwargs: Any,
    ) -> None:
        """
        Initialise a new instance.

        :param args: positional arguments.
        :param kwargs: keyword arguments.
        """
        # This __init__ method is created for type-hinting purposes only.
        # Tango devices are not supposed to have __init__ methods,
        # And they have a strange __new__ method,
        # that calls __init__ when you least expect it.
        # So don't put anything executable in here
        # (other than the super() call).
        self._health_state: HealthState
        self._health_model: MaserHealthModel
        self._component_manager: MaserDriver
        self._version = sys.modules["ska_sat_lmc"].__version__
        self.attribute_info: dict[str, Any]

        super().__init__(*args, **kwargs)

    def init_device(self: SatMaser) -> None:
        """Initialise the device."""
        # Can't have min/max alarm with these values set to None
        self.attribute_info = {
            "battery_voltage_a": 0.5,
            "battery_current_a": 0.5,
            "battery_voltage_b": None,
            "battery_current_b": None,
            "hydrogen_pressure_setting": None,
            "hydrogen_pressure_measured": None,
            "purifier_current": None,
            "dissociator_current": None,
            "dissociator_light": None,
            "internal_top_heater_voltage": None,
            "internal_bottom_heater_voltage": None,
            "internal_side_heater_voltage": None,
            "thermal_control_unit_heater_voltage": None,
            "external_side_heater_voltage": None,
            "external_bottom_heater_voltage": None,
            "isolator_heater_voltage": None,
            "tube_heater_voltage": None,
            "boxes_temperature": None,
            "boxes_current": None,
            "ambient_temperature": None,
            "cfield_voltage": None,
            "varactor_diode_voltage": None,
            "external_high_voltage_value": None,
            "external_high_current_value": None,
            "internal_high_voltage_value": None,
            "internal_high_current_value": None,
            "hydrogen_storage_pressure": None,
            "hydrogen_storage_heater_voltage": None,
            "pirani_heater_voltage": None,
            "oscillator_100mhz_voltage": None,
            "amplitude_405khz_voltage": None,
            "oscillator_voltage": None,
            "positive24vdc": 0.1,
            "positive15vdc": None,
            "negative15vdc": None,
            "positive5vdc": 0.5,
            "negative5vdc": None,
            "positive8vdc": None,
            "positive18vdc": None,
            "lock100mhz": None,
            "phase_lock_loop_lockstatus": None,
        }
        try:
            super().init_device()

            device_name = f'{str(self.__class__).rsplit(".", maxsplit=1)[-1][0:-2]}'
            version = f"{device_name} Software Version: {self._version}"
            props = (
                f"Initialised {device_name}"
                f"\nUDP on: {self.MaserHost}:{self.MaserPort}"
                f"\nand update rate: {self.UpdateRate}"
            )
            version_info = f"{self.__class__.__name__}, {self._build_state}"
            self.logger.info("\n%s\n%s\n%s", version_info, version, props)
            self.logger.info("Device initialisation complete")

        # pylint: disable=broad-exception-caught
        except Exception as ex:
            self.logger.error(repr(ex))

    def _init_state_model(self: SatMaser) -> None:
        """Initialise the state model."""
        super()._init_state_model()
        self._health_state = HealthState.UNKNOWN
        self._health_model = MaserHealthModel(
            self._component_state_changed, self.logger
        )
        for attr_name, value in self.attribute_info.items():
            self.set_change_event(attr_name, True, False)
            self.set_archive_event(attr_name, True, False)

        self.set_change_event("healthState", True, False)
        self.set_archive_event("healthState", True, False)

    def create_component_manager(
        self: SatMaser,
    ) -> MaserDriver:
        """
        Create and return a component manager for this device.

        :return: a component manager for this device.
        """
        return MaserDriver(
            self.logger,
            self.MaserHost,
            self.MaserPort,
            self._communication_state_changed,
            self._component_state_changed,
            self.UpdateRate,
        )

    def init_command_objects(self: SatMaser) -> None:
        """Initialise the command handlers for commands supported by this device."""
        super().init_command_objects()

    class InitCommand(DeviceInitCommand):
        """Implement the device initialisation for the Maser device."""

        def do(
            self: SatMaser.InitCommand, *args: Any, **kwargs: Any
        ) -> tuple[ResultCode, str]:
            """
            Initialise the attributes and properties.

            :param args: unspecified positional arguments. This should
                be empty and is provided for type hinting only
            :param kwargs: unspecified keyword arguments. This should be
                empty and is provided for type hinting only

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            """
            assert (
                not args and not kwargs
            ), f"do method has unexpected arguments {args}, {kwargs}"
            # pylint: disable=protected-access
            self._device._health_state = HealthState.UNKNOWN
            return (ResultCode.OK, "Init command completed OK")

    # ----------
    # Callbacks
    # ----------
    def _communication_state_changed(
        self: SatMaser,
        communication_state: CommunicationStatus,
    ) -> None:
        """
        Handle change in communications status between component manager and component.

        This is a callback hook, called by the component manager when
        the communications status changes. It is implemented here to
        drive the op_state.

        :param communication_state: the status of communications between
            the component manager and its component.
        """
        action_map = {
            CommunicationStatus.DISABLED: "component_disconnected",
            CommunicationStatus.NOT_ESTABLISHED: "component_disconnected",
            CommunicationStatus.ESTABLISHED: "component_on",
        }

        action = action_map[communication_state]
        if action is not None:
            self.op_state_model.perform_action(action)
        self._health_model.update_state(
            communicating=(communication_state == CommunicationStatus.ESTABLISHED)
        )

    # pylint: disable=arguments-differ
    def _component_state_changed(  # type: ignore[override]
        self: SatMaser,
        **kwargs: Any,
    ) -> None:
        """
        Handle change in the state of the component.

        This is a callback hook, called by the component manager when
        the state of the component changes.

        :param kwargs: state change parameters.
        """
        if "fault" in kwargs:
            fault = kwargs.pop("fault")
            super()._component_state_changed(fault=fault)
            self._health_model.update_state(fault=fault)

        if "health" in kwargs:
            health = kwargs.pop("health")
            if self._health_state != health:
                self._health_state = cast(HealthState, health)
                self.push_change_event("healthState", health)
                self.push_archive_event("healthState", health)

        for attr_name, value in kwargs.items():
            self.attribute_info[attr_name] = value
            self.push_change_event(attr_name, value)

        if self.dev_state() == tango.DevState.ALARM:
            self._health_model.update_state(fault=True)

    # ----------
    # Attributes
    # ----------
    # pylint: disable=unused-argument
    def is_attribute_allowed(self: SatMaser, attr_req_type: tango.AttReqType) -> bool:
        """
        Protect attribute access before being updated otherwise it reports alarm.

        :param attr_req_type: tango attribute type READ/WRITE

        :return: True if the attribute can be read else False
        """
        # Need to add DevState.ALARM to allow correction to attribute values
        # e.g. Can't fix a voltage problem with reset.
        return self.get_state() in [
            tango.DevState.ON,
            tango.DevState.ALARM,
        ]

    @attribute(
        dtype="float",
        label="Ubatt.A [V]",
        unit="Volts",
        max_value=100.0,
        min_value=0.0,
        max_alarm=99.5,
        min_alarm=0.4,
        doc="Battery voltage A",
        fisallowed=is_attribute_allowed,
    )
    def battery_voltage_a(self: SatMaser) -> float:
        """
        Report the voltage of battery A.

        :return: battery voltage A
        """
        return self.attribute_info["battery_voltage_a"]

    @attribute(
        dtype="float",
        label="Ibatt.A [A]",
        unit="Amps",
        max_value=5,
        min_value=0,
        max_alarm=4.0,
        min_alarm=0.01,
        doc="Battery current A",
        fisallowed=is_attribute_allowed,
    )
    def battery_current_a(self: SatMaser) -> float:
        """
        Report the current of battery A.

        :return: battery current A
        """
        return self.attribute_info["battery_current_a"]

    @attribute(
        dtype="float",
        label="Ubatt.B [V]",
        unit="Volts",
        max_value=100,
        min_value=0,
        doc="Battery voltage B",
        fisallowed=is_attribute_allowed,
    )
    def battery_voltage_b(self: SatMaser) -> float:
        """
        Report the voltage of battery B.

        :return: battery voltage B
        """
        return self.attribute_info["battery_voltage_b"]

    @attribute(
        dtype="float",
        label="Ibatt.B [A]",
        unit="Amps",
        max_value=5,
        min_value=0,
        doc="Battery current B",
        fisallowed=is_attribute_allowed,
    )
    def battery_current_b(self: SatMaser) -> float:
        """
        Report the current of battery B.

        :return: battery current B
        """
        return self.attribute_info["battery_current_b"]

    @attribute(
        dtype="float",
        label="Set.H [V]",
        unit="Volts",
        max_value=15,
        min_value=0,
        doc="Hydrogen pressure setting",
        fisallowed=is_attribute_allowed,
    )
    def hydrogen_pressure_setting(self: SatMaser) -> float:
        """
        Report the hydrogen pressure setting.

        :return: hydrogen pressure setting
        """
        return self.attribute_info["hydrogen_pressure_setting"]

    @attribute(
        dtype="float",
        label="Meas.H [V]",
        unit="Volts",
        max_value=5,
        min_value=0,
        doc="Hydrogen pressure measurement",
        fisallowed=is_attribute_allowed,
    )
    def hydrogen_pressure_measured(self: SatMaser) -> float:
        """
        Report the measured hydrogen pressure.

        :return: hydrogen pressure measurement
        """
        return self.attribute_info["hydrogen_pressure_measured"]

    @attribute(
        dtype="float",
        label="Ipurifiler [A]",
        unit="Amps",
        max_value=5,
        min_value=0,
        doc="Purifier current",
        fisallowed=is_attribute_allowed,
    )
    def purifier_current(self: SatMaser) -> float:
        """
        Report the purifier current.

        :return: purifier current
        """
        return self.attribute_info["purifier_current"]

    @attribute(
        dtype="float",
        label="dissociator_current [A]",
        unit="Amps",
        max_value=5,
        min_value=0,
        doc="Dissociator current",
        fisallowed=is_attribute_allowed,
    )
    def dissociator_current(self: SatMaser) -> float:
        """
        Report the dissociator current.

        :return: dissociator current
        """
        return self.attribute_info["dissociator_current"]

    @attribute(
        dtype="float",
        label="dissociator_light [V]",
        unit="Volts",
        max_value=5,
        min_value=0,
        doc="Dissociator light",
        fisallowed=is_attribute_allowed,
    )
    def dissociator_light(self: SatMaser) -> float:
        """
        Report the dissociator light.

        :return: dissociator light
        """
        return self.attribute_info["dissociator_light"]

    @attribute(
        dtype="float",
        label="ITheater [V]",
        unit="Volts",
        max_value=20,
        min_value=0,
        doc="Internal top heater",
        fisallowed=is_attribute_allowed,
    )
    def internal_top_heater_voltage(self: SatMaser) -> float:
        """
        Report the internal top heater voltage.

        :return: internal top heater voltage
        """
        return self.attribute_info["internal_top_heater_voltage"]

    @attribute(
        dtype="float",
        label="IBheater [V]",
        unit="Volts",
        max_value=20,
        min_value=0,
        doc="Internal bottom heater",
        fisallowed=is_attribute_allowed,
    )
    def internal_bottom_heater_voltage(self: SatMaser) -> float:
        """
        Report the internal bottom heater voltage.

        :return: internal bottom heater voltage
        """
        return self.attribute_info["internal_bottom_heater_voltage"]

    @attribute(
        dtype="float",
        label="ISheater [V]",
        unit="Volts",
        max_value=20,
        min_value=0,
        doc="Internal side heater",
        fisallowed=is_attribute_allowed,
    )
    def internal_side_heater_voltage(self: SatMaser) -> float:
        """
        Report the internal side heater voltage.

        :return: internal side heater voltage
        """
        return self.attribute_info["internal_side_heater_voltage"]

    @attribute(
        dtype="float",
        label="UTCheater [V]",
        unit="Volts",
        max_value=20,
        min_value=0,
        doc="Thermal control unit heater",
        fisallowed=is_attribute_allowed,
    )
    def thermal_control_unit_heater_voltage(self: SatMaser) -> float:
        """
        Report the thermal control unit heater voltage.

        :return: thermal control unit heater voltage
        """
        return self.attribute_info["thermal_control_unit_heater_voltage"]

    @attribute(
        dtype="float",
        label="ESheater [V]",
        unit="Volts",
        max_value=20,
        min_value=0,
        doc="External side heater",
        fisallowed=is_attribute_allowed,
    )
    def external_side_heater_voltage(self: SatMaser) -> float:
        """
        Report the external side heater voltage.

        :return: external side heater voltage
        """
        return self.attribute_info["external_side_heater_voltage"]

    @attribute(
        dtype="float",
        label="EBheater [V]",
        unit="Volts",
        max_value=20,
        min_value=0,
        doc="External bottom heater",
        fisallowed=is_attribute_allowed,
    )
    def external_bottom_heater_voltage(self: SatMaser) -> float:
        """
        Report the external bottom heater voltage.

        :return: external bottom heater voltage
        """
        return self.attribute_info["external_bottom_heater_voltage"]

    @attribute(
        dtype="float",
        label="Iheater [V]",
        unit="Volts",
        max_value=20,
        min_value=0,
        doc="Isolator heater",
        fisallowed=is_attribute_allowed,
    )
    def isolator_heater_voltage(self: SatMaser) -> float:
        """
        Report the isolator heater voltage.

        :return: isolator heater voltage
        """
        return self.attribute_info["isolator_heater_voltage"]

    @attribute(
        dtype="float",
        label="Theater [V]",
        unit="Volts",
        max_value=20,
        min_value=0,
        doc="Tube heater",
        fisallowed=is_attribute_allowed,
    )
    def tube_heater_voltage(self: SatMaser) -> float:
        """
        Report the tube heater voltage.

        :return: tube heater voltage
        """
        return self.attribute_info["tube_heater_voltage"]

    @attribute(
        dtype="float",
        label="Boxestemp [DegC]",
        unit="DegC",
        max_value=100,
        min_value=0,
        doc="Boxes temperature",
        fisallowed=is_attribute_allowed,
    )
    def boxes_temperature(self: SatMaser) -> float:
        """
        Report the boxes temperature.

        :return: boxes temperature
        """
        return self.attribute_info["boxes_temperature"]

    @attribute(
        dtype="float",
        label="boxes_current [A]",
        unit="Amps",
        max_value=5,
        min_value=0,
        doc="Boxes current",
        fisallowed=is_attribute_allowed,
    )
    def boxes_current(self: SatMaser) -> float:
        """
        Report the boxes current.

        :return: boxes current
        """
        return self.attribute_info["boxes_current"]

    @attribute(
        dtype="float",
        label="AmbTemp [DegC]",
        unit="DegC",
        max_value=50,
        min_value=0,
        doc="Ambient temperature",
        fisallowed=is_attribute_allowed,
    )
    def ambient_temperature(self: SatMaser) -> float:
        """
        Report the ambient temperature.

        :return: ambient temperature
        """
        return self.attribute_info["ambient_temperature"]

    @attribute(
        dtype="float",
        label="cfield_voltage [V]",
        unit="Volts",
        max_value=100,
        min_value=0,
        doc="C-field voltage",
        fisallowed=is_attribute_allowed,
    )
    def cfield_voltage(self: SatMaser) -> float:
        """
        Report the C-field voltage.

        :return: C-field voltage
        """
        return self.attribute_info["cfield_voltage"]

    @attribute(
        dtype="float",
        label="varactor_diode_voltage [V]",
        unit="Volts",
        max_value=100,
        min_value=0,
        doc="Varactor voltage",
        fisallowed=is_attribute_allowed,
    )
    def varactor_diode_voltage(self: SatMaser) -> float:
        """
        Report the varactor voltage.

        :return: varactor voltage
        """
        return self.attribute_info["varactor_diode_voltage"]

    @attribute(
        dtype="float",
        label="UHText [Kv]",
        unit="KV",
        max_value=5,
        min_value=0,
        doc="External high voltage value",
        fisallowed=is_attribute_allowed,
    )
    def external_high_voltage_value(self: SatMaser) -> float:
        """
        Report the external high voltage value.

        :return: external high voltage value
        """
        return self.attribute_info["external_high_voltage_value"]

    @attribute(
        dtype="float",
        label="IHText [uA]",
        unit="uAmps",
        max_value=500,
        min_value=0,
        doc="External high current value",
        fisallowed=is_attribute_allowed,
    )
    def external_high_current_value(self: SatMaser) -> float:
        """
        Report the external high current value.

        :return: external high current value
        """
        return self.attribute_info["external_high_current_value"]

    @attribute(
        dtype="float",
        label="UHTint [kV]",
        unit="KV",
        max_value=5,
        min_value=0,
        doc="Internal high voltage value",
        fisallowed=is_attribute_allowed,
    )
    def internal_high_voltage_value(self: SatMaser) -> float:
        """
        Report the internal high voltage value.

        :return: internal high voltage value
        """
        return self.attribute_info["internal_high_voltage_value"]

    @attribute(
        dtype="float",
        label="IHTint [uA]",
        unit="uAmps",
        max_value=500,
        min_value=0,
        doc="Internal high voltage current",
        fisallowed=is_attribute_allowed,
    )
    def internal_high_current_value(self: SatMaser) -> float:
        """
        Report the internal high current value.

        :return: internal high current value
        """
        return self.attribute_info["internal_high_current_value"]

    @attribute(
        dtype="float",
        label="hydrogen_storage_pressure [bar]",
        unit="Bar",
        max_value=20,
        min_value=0,
        doc="Hydrogen storage pressure",
        fisallowed=is_attribute_allowed,
    )
    def hydrogen_storage_pressure(self: SatMaser) -> float:
        """
        Report the hydrogen storage pressure.

        :return: hydrogen storage pressure
        """
        return self.attribute_info["hydrogen_storage_pressure"]

    @attribute(
        dtype="float",
        label="hydrogen_storage_heater_voltage [V]",
        unit="Volts",
        max_value=25,
        min_value=0,
        doc="Hydrogen storage heater",
        fisallowed=is_attribute_allowed,
    )
    def hydrogen_storage_heater_voltage(self: SatMaser) -> float:
        """
        Report the hydrogen storage heater voltage.

        :return: hydrogen storage heater voltage
        """
        return self.attribute_info["hydrogen_storage_heater_voltage"]

    @attribute(
        dtype="float",
        label="pirani_heater_voltage [V]",
        unit="Volts",
        max_value=25,
        min_value=0,
        doc="Pirani heater",
        fisallowed=is_attribute_allowed,
    )
    def pirani_heater_voltage(self: SatMaser) -> float:
        """
        Report the pirani heater voltage.

        :return: pirani heater voltage
        """
        return self.attribute_info["pirani_heater_voltage"]

    @attribute(
        dtype="float",
        label="oscillator_voltage 100MHz",
        unit="Volts",
        max_value=10,
        min_value=0,
        doc=" oscillator_voltage 100MHz",
        fisallowed=is_attribute_allowed,
    )
    def oscillator_100mhz_voltage(self: SatMaser) -> float:
        """
        Report the oscillator_voltage 100MHz.

        :return: oscillator_voltage 100MHz
        """
        return self.attribute_info["oscillator_100mhz_voltage"]

    @attribute(
        dtype="float",
        label="amplitude_405khz_voltage [V]",
        unit="Volts",
        max_value=15,
        min_value=0,
        doc="405 kHz Amplitude",
        fisallowed=is_attribute_allowed,
    )
    def amplitude_405khz_voltage(self: SatMaser) -> float:
        """
        Report the 405 kHz Amplitude.

        :return: the 405 kHz Amplitude
        """
        return self.attribute_info["amplitude_405khz_voltage"]

    @attribute(
        dtype="float",
        label="oscillator_voltage [V]",
        unit="Volts",
        max_value=10,
        min_value=0,
        doc="OCXO varicap voltage",
        fisallowed=is_attribute_allowed,
    )
    def oscillator_voltage(self: SatMaser) -> float:
        """
        Report the OCXO varicap voltage.

        :return: the OCXO varicap voltage
        """
        return self.attribute_info["oscillator_voltage"]

    @attribute(
        dtype="float",
        label="+24Vdc [V]",
        unit="Volts",
        max_value=25,
        min_value=0,
        max_alarm=24.95,
        min_alarm=0.05,
        doc="+24 V supply voltage",
        fisallowed=is_attribute_allowed,
    )
    def positive24vdc(self: SatMaser) -> float:
        """
        Report the +24 V supply voltage.

        :return: +24 V supply voltage
        """
        return self.attribute_info["positive24vdc"]

    @attribute(
        dtype="float",
        label="+15Vdc [V]",
        unit="Volts",
        max_value=20,
        min_value=0,
        doc="+15 V supply voltage",
        fisallowed=is_attribute_allowed,
    )
    def positive15vdc(self: SatMaser) -> float:
        """
        Report the +15 V supply voltage.

        :return: +15 V supply voltage
        """
        return self.attribute_info["positive15vdc"]

    @attribute(
        dtype="float",
        label="-15Vdc [V]",
        unit="Volts",
        max_value=0,
        min_value=-20,
        doc="-15 V supply voltage",
        fisallowed=is_attribute_allowed,
    )
    def negative15vdc(self: SatMaser) -> float:
        """
        Report the -15 V supply voltage.

        :return: -15 V supply voltage
        """
        return self.attribute_info["negative15vdc"]

    @attribute(
        dtype="float",
        label="+5Vdc [V]",
        unit="Volts",
        max_value=5.5,
        min_value=0,
        max_alarm=5.5,
        min_alarm=0.4,
        doc="+5V supply voltage",
        fisallowed=is_attribute_allowed,
    )
    def positive5vdc(self: SatMaser) -> float:
        """
        Report the +5 V supply voltage.

        :return: +5 V supply voltage
        """
        return self.attribute_info["positive5vdc"]

    @attribute(
        dtype="float",
        label="-5Vdc [V]",
        unit="Volts",
        max_value=0,
        min_value=-5,
        doc="-5V supply voltage",
        fisallowed=is_attribute_allowed,
    )
    def negative5vdc(self: SatMaser) -> float:
        """
        Report the -5 V supply voltage.

        :return: -5 V supply voltage
        """
        return self.attribute_info["negative5vdc"]

    @attribute(
        dtype="float",
        label="+8Vdc [V]",
        unit="Volts",
        max_value=8,
        min_value=0,
        doc="+8V supply voltage",
        fisallowed=is_attribute_allowed,
    )
    def positive8vdc(self: SatMaser) -> float:
        """
        Report the +8 V supply voltage.

        :return: +8 V supply voltage
        """
        return self.attribute_info["positive8vdc"]

    @attribute(
        dtype="float",
        label="+18Vdc [V]",
        unit="Volts",
        max_value=20,
        min_value=0,
        doc="+18V supply voltage",
        fisallowed=is_attribute_allowed,
    )
    def positive18vdc(self: SatMaser) -> float:
        """
        Report the +18 V supply voltage.

        :return: +18 V supply voltage
        """
        return self.attribute_info["positive18vdc"]

    @attribute(
        dtype="float",
        label="lock100MHz",
        unit="",
        max_value=10,
        min_value=0,
        doc="lock 100MHz status",
        fisallowed=is_attribute_allowed,
    )
    def lock100mhz(self: SatMaser) -> float:
        """
        Report the lock 100MHz status.

        :return: the lock 100MHz status
        """
        return self.attribute_info["lock100mhz"]

    @attribute(
        dtype="bool",
        label="pllLockStatus",
        unit="",
        doc="Main PLL lock status",
        fisallowed=is_attribute_allowed,
    )
    def phase_lock_loop_lockstatus(self: SatMaser) -> bool:
        """
        Report the main PLL lock status.

        :return: main PLL lock status
        """
        return self.attribute_info["phase_lock_loop_lockstatus"]


# ----------
# Run server
# ----------


def main(*args: str, **kwargs: str) -> int:
    """
    Entry point for module.

    :param args: positional arguments
    :param kwargs: named arguments

    :return: exit code
    """
    return SatMaser.run_server(args=args or None, **kwargs)


if __name__ == "__main__":
    main()
