# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module implements the Maser driver."""

from __future__ import annotations  # allow forward references in type hints

__all__ = ["MaserDriver"]

import logging
from typing import Any, Callable, Final

from ska_control_model import CommunicationStatus
from ska_tango_base.poller import PollingComponentManager

from ska_sat_lmc.clocks.comms import UdpSocket
from ska_sat_lmc.component.component_manager import SatComponentManager

__all__ = ["MaserDriver"]


# pylint: disable=too-many-instance-attributes
class MaserDriver(PollingComponentManager[bool, dict[str, Any]], SatComponentManager):
    """Device Server for the T4Science i3000 Maser for SAT.LMC."""

    # pylint: disable=too-many-arguments
    def __init__(
        self: MaserDriver,
        logger: logging.Logger,
        maser_host: str,
        maser_port: int,
        communication_state_changed: Callable,
        component_state_changed: Callable,
        update_rate: float = 1.0,
    ) -> None:
        """
        Initialise the attributes and properties of the SatMaser.

        :param logger: a logger for this object to use
        :param maser_host: the host of the hardware maser
        :param maser_port: the port of the hardware maser
        :param communication_state_changed: callback to be
            called when the status of the communications channel between
            the component manager and its component changes
        :param component_state_changed: callback to be called
            when the component faults (or stops faulting)
        :param update_rate: how often updates to attribute values should
            be provided. This is not necessarily the same as the rate at
            which the instrument is polled. For example, the instrument
            may be polled every 0.1 seconds, thus ensuring that any
            invoked commands or writes will be executed promptly.
            However, if the `update_rate` is 5.0, then routine reads of
            instrument values will only occur every 50th poll (i.e.
            every 5 seconds).

        """
        self._logger = logger
        self._poll_rate: Final = 0.1
        self._max_tick: Final = int(update_rate / self._poll_rate)

        # We'll count ticks upwards, but start at the maximum so that
        # our initial update request occurs as soon as possible.
        self._tick = self._max_tick

        self._communication_state_changed = communication_state_changed
        self._component_state_changed = component_state_changed
        super().__init__(
            logger,
            communication_state_changed,
            component_state_changed,
            poll_rate=self._poll_rate,
        )
        self._logger.info("Initialising Maser device server")
        self._udp: UdpSocket
        self._timeout = 1.0
        self._eol = b"\r\n"
        self._host = maser_host
        self._port = maser_port
        self._logger.info("Initialised maser ")

    def start_communicating(self: MaserDriver) -> None:
        """Establish communication with the maser."""
        super().start_communicating()
        self._connect_to_maser()

    def _connect_to_maser(self: MaserDriver) -> None:
        """Make a UDP connection to the Maser and start monitor thread."""
        self.logger.info(f"Opening udp connection on {self._host}:{self._port}")
        self._udp = UdpSocket(
            self._host,
            self._port,
            eol=self._eol,
            timeout=self._timeout,
            logger=self._logger,
        )
        self._udp.connect()
        if self._udp.is_connected:
            self._communication_state_changed(CommunicationStatus.ESTABLISHED)

    def stop_communicating(self: MaserDriver) -> None:
        """Stop communicating with the maser."""
        super().stop_communicating()

    def get_request(self: MaserDriver) -> bool:
        """
        Check whether the attributes should be read on the next poll.

        :return: true if attributes are read on the next poll.
        """
        poll_request = False
        self._tick += 1
        if self._tick > self._max_tick:
            msg = f"Tick {self._tick} >= {self._max_tick}"
            self.logger.debug(msg)
            poll_request = True
            self._tick = 0
        msg = f"Returning {poll_request} for next poll."
        self.logger.debug(msg)
        return poll_request

    def poll(self: MaserDriver, poll_request: bool) -> dict[str, Any]:
        """
        Poll the hardware.

        Connect to the hardware, write any values that are to be
        written, and then read all values.

        :param poll_request: whether to perform in this poll.

        :return: responses to queries in this poll
        """
        self.logger.info("Poller is initiating next poll.")
        poll_response = {}
        try:
            buff = self._udp.write_readline(
                b"MONIT;\r\n", eol=self._eol, timeout=3
            ).decode()
            msg = f"Readline returns: {buff}"
            self._logger.info(msg)
            # Check the integrity of the received message
            if buff == "":
                self._logger.info("readline() timeout")
            elif len(buff) != 114 or buff[0] != "$":
                msg = f"Received bad message format from {self._host}"
                self._logger.error(msg)
                self._communication_state_changed(CommunicationStatus.NOT_ESTABLISHED)
            else:
                try:
                    poll_response = self._decode_attribute_properties(buff)
                    if poll_response:
                        self._communication_state_changed(
                            CommunicationStatus.ESTABLISHED
                        )
                # pylint: disable=broad-exception-caught
                except Exception as ex:
                    msg = f"Attribute decoding failed {repr(ex)}"
                    self._logger.error(msg)
        # pylint: disable=broad-exception-caught
        except Exception as ex:
            msg = (
                f"Received timeout from {self._host}. Network or power failure?"
                f" {repr(ex)}"
            )
            self._logger.error(msg)

        return poll_response

    def _decode_attribute_properties(self: MaserDriver, buff: str) -> dict[str, Any]:
        """
        Extract the property values from the given buffer and convert to real values.

        :param buff: the maser string

        :return: the decoded maser string as a dict of data values
        """
        decoded_data = {}
        # Read 32 three-byte integers
        int_iter = (
            int(substr, 16) for substr in [buff[i : i + 3] for i in range(1, 97, 3)]
        )
        decoded_data["battery_voltage_a"] = next(int_iter) * 0.024414
        decoded_data["battery_current_a"] = next(int_iter) * 0.0012207
        decoded_data["battery_voltage_b"] = next(int_iter) * 0.024414
        decoded_data["battery_current_b"] = next(int_iter) * 0.0012207
        decoded_data["hydrogen_pressure_setting"] = next(int_iter) * 0.0036621
        decoded_data["hydrogen_pressure_measured"] = next(int_iter) * 0.0012207
        decoded_data["purifier_current"] = next(int_iter) * 0.0012207
        decoded_data["dissociator_current"] = next(int_iter) * 0.0012207
        decoded_data["dissociator_light"] = next(int_iter) * 0.0012207
        decoded_data["internal_top_heater_voltage"] = next(int_iter) * 0.0048828
        decoded_data["internal_bottom_heater_voltage"] = next(int_iter) * 0.0048828
        decoded_data["internal_side_heater_voltage"] = next(int_iter) * 0.0048828
        decoded_data["thermal_control_unit_heater_voltage"] = next(int_iter) * 0.0048828
        decoded_data["external_side_heater_voltage"] = next(int_iter) * 0.0048828
        decoded_data["external_bottom_heater_voltage"] = next(int_iter) * 0.0048828
        decoded_data["isolator_heater_voltage"] = next(int_iter) * 0.0048828
        decoded_data["tube_heater_voltage"] = next(int_iter) * 0.0048828
        decoded_data["boxes_temperature"] = next(int_iter) * 0.024414
        decoded_data["boxes_current"] = next(int_iter) * 0.0012207
        decoded_data["ambient_temperature"] = next(int_iter) * 0.012207
        decoded_data["cfield_voltage"] = next(int_iter) * 0.0024414
        decoded_data["varactor_diode_voltage"] = next(int_iter) * 0.0024414
        decoded_data["external_high_voltage_value"] = next(int_iter) * 0.00195312
        decoded_data["external_high_current_value"] = next(int_iter) * 0.24414
        decoded_data["internal_high_voltage_value"] = next(int_iter) * 0.00195312
        decoded_data["internal_high_current_value"] = next(int_iter) * 0.24414
        decoded_data["hydrogen_storage_pressure"] = next(int_iter) * 0.0048828
        decoded_data["hydrogen_storage_heater_voltage"] = next(int_iter) * 0.0061035
        decoded_data["pirani_heater_voltage"] = next(int_iter) * 0.0061035
        decoded_data["oscillator_100mhz_voltage"] = next(int_iter) * 0.0024414
        decoded_data["amplitude_405khz_voltage"] = next(int_iter) * 0.0036621
        decoded_data["oscillator_voltage"] = next(int_iter) * 0.0024414

        # then read 8 two-byte integers,
        buffer_iter = (
            int(substr, 16) for substr in [buff[i : i + 2] for i in range(97, 113, 2)]
        )
        decoded_data["positive24vdc"] = next(buffer_iter) * 0.097656
        decoded_data["positive15vdc"] = next(buffer_iter) * 0.078125
        decoded_data["negative15vdc"] = next(buffer_iter) * -0.078125
        decoded_data["positive5vdc"] = next(buffer_iter) * 0.03906
        decoded_data["negative5vdc"] = next(buffer_iter) * -0.03906
        decoded_data["positive8vdc"] = next(buffer_iter) * 0.03906
        decoded_data["positive18vdc"] = next(buffer_iter) * 0.07813
        decoded_data["lock100mhz"] = next(buffer_iter) * 0.03906

        # finally read a boolean
        value = bool(buff[113:])
        decoded_data["phase_lock_loop_lockstatus"] = value
        return decoded_data

    def poll_succeeded(self: MaserDriver, poll_response: dict[str, Any]) -> None:
        """
        Handle the receipt of new polling values.

        This is a hook called by the poller when values have been read
        during a poll.

        :param poll_response: response to the poll, including any values
            read.
        """
        msg = f"Result from successful poll: {poll_response} "
        self.logger.info(msg)
        if poll_response:
            self.logger.debug("Pushing updates")
            if self._component_state_changed is not None:
                self._component_state_changed(fault=False, **poll_response)
        else:
            self._logger.debug("Setting fault status true")
            if self._component_state_changed is not None:
                self._component_state_changed(fault=True)

    def polling_stopped(self: MaserDriver) -> None:
        """
        Respond to polling having stopped.

        This is a hook called by the poller when it stops polling.
        """
        self.logger.debug("Polling has stopped.")

        # Set to max here so that if/when polling restarts, an update is
        # requested as soon as possible.
        self._tick = self._max_tick

        # Not calling super().polling_stopped() here,
        # because ska-tango-base inappropriately pushes power=UNKNOWN,
        self._communication_state_changed(CommunicationStatus.DISABLED)
