# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""An implementation of a health model for a SatMaser."""

from ska_sat_lmc.health import HealthModel

__all__ = ["MaserHealthModel"]


class MaserHealthModel(HealthModel):
    """
    A health model for a maser.

    At present this uses the base health model; this is a placeholder
    for a future, better implementation.
    """
