# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module implements a standlone maser simulator."""

from __future__ import annotations  # allow forward references in type hints

import logging
import random
import socket
import threading
import time
from typing import Any

import tango
from tango import DevState
from tango.server import Device, attribute, command, device_property

__all__ = ["MaserSimulator"]


# pylint: disable=too-many-lines,too-many-instance-attributes
class SimulatorServer:
    """Simulator server."""

    def __init__(self: SimulatorServer, *args: Any, **kwargs: Any) -> None:
        """
        Initialise the server.

        :param args: positional arguments
        :param kwargs: named arguments
        """
        # Setup the default values.
        self._lock = threading.Lock()
        self._host = args[0]
        self._port = args[1]
        self._logger = args[2]
        self._stop_server = False
        self._buff = ""
        # Create a UDP socket
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_address = (self._host, self._port)
        self._sock.bind(server_address)
        self._logger.info(f"Server starting up on {self._host} port {self._port}")
        self._thread = threading.Thread(target=self._do_write, args=(), daemon=True)
        self._logger.info("Thread task starting now")
        self._thread.start()

    def stopServer(self: SimulatorServer) -> None:
        """Stop the server."""
        with self._lock:
            self._stop_server = True

    def update(self: SimulatorServer, buff: str) -> None:
        """
        Update the buffer with changed attribute values.

        :param buff: new maser string
        """
        with self._lock:
            self._buff = buff
        self._logger.info(f"Update the local buffer {buff.rstrip()}")

    def _do_write(self: SimulatorServer) -> None:
        """Task to wait for connections."""
        while not self._stop_server:
            self._logger.info("Waiting for a client connection")
            msg, client_address = self._sock.recvfrom(1024)
            if msg.decode().startswith("MONIT;"):
                try:
                    self._logger.info(
                        f"Message {msg.decode()} from client {client_address}"
                    )
                    with self._lock:
                        self._logger.info(
                            f"Sending the buffer {self._buff.rstrip()}"
                            f" to {client_address}"
                        )
                        self._sock.sendto(self._buff.encode(), client_address)
                # pylint: disable=broad-exception-caught
                except Exception as ex:
                    self._logger.info(f"Closing client connection {ex}")
                    self._sock.close()
                finally:
                    # Clean up the connection
                    self._logger.info("finally")
                    self._stop_server = False
            else:
                time.sleep(1)


# pylint: disable=too-many-public-methods
class MaserSimulator(Device):
    """Communication Control device simulator for SAT.LMC."""

    # -----------------
    # Device Properties
    # -----------------
    MaserHost = device_property(dtype=str, default_value="localhost")
    MaserPort = device_property(dtype=int, default_value="45678")

    # ---------------
    # General methods
    # ---------------
    def __init__(self: MaserSimulator, *args: Any, **kwargs: Any) -> None:
        """
        Initialise this device object.

        :param args: positional args to the init
        :param kwargs: keyword args to the init
        """
        # We aren't supposed to define initialisation methods for Tango
        # devices; we are only supposed to define an `init_device` method. But
        # we insist on doing so here, just so that we can define some
        # attributes, thereby stopping the linters from complaining about
        # "attribute-defined-outside-init" etc. We still need to make sure that
        # `init_device` re-initialises any values defined in here.
        super().__init__(*args, **kwargs)
        self._randomise: bool
        self._simserver: SimulatorServer
        self._thread: threading.Thread | None

        self._battery_voltage_a: float
        self._battery_current_a: float
        self._battery_voltage_b: float
        self._battery_current_b: float
        self._hydrogen_pressure_setting: float
        self._meas_h: float
        self._purifier_current: float
        self._dissociator_current: float
        self._dissociator_light: float
        self._internal_top_heater_voltage: float
        self._internal_bottom_heater_voltage: float
        self._internal_side_heater_voltage: float
        self._thermal_control_unit_heater_voltage: float
        self._external_side_heater_voltage: float
        self._external_bottom_heater_voltage: float
        self._isolator_heater_voltage: float
        self._tube_heater_voltage: float
        self._boxes_temperature: float
        self._boxes_current: float
        self._ambient_temperature: float
        self._cfield_voltage: float
        self._varactor_diode_voltage: float
        self._external_high_voltage_value: float
        self._external_high_current_value: float
        self._internal_high_voltage_value: float
        self._internal_high_current_value: float
        self._hydrogen_storage_pressure: float
        self._hydrogen_storage_heater_voltage: float
        self._pirani_heater_voltage: float
        self._oscillator_100mhz_voltage: float
        self._amplitude_405khz_voltage: float
        self._oscillator_voltage: float
        self._positive24vdc: float
        self._positive15vdc: float
        self._negative15vdc: float
        self._positive5vdc: float
        self._negative5vdc: float
        self._positive8vdc: float
        self._positive18vdc: float
        self._lock100mhz: float
        self._phase_lock_loop_lockstatus: bool
        self._eol: str

    # pylint: disable=too-many-statements
    def init_device(self: MaserSimulator) -> None:
        """Initialise the device."""
        Device.init_device(self)
        self.set_state(DevState.INIT)
        logging.basicConfig(level=logging.INFO)
        self._logger = logging.getLogger(self.__class__.__name__)
        # set  default values
        self._battery_voltage_a = 27.66
        self._battery_current_a = 0.08
        self._battery_voltage_b = 28.10
        self._battery_current_b = 3.09
        self._hydrogen_pressure_setting = 5.83
        self._meas_h = 1.53
        self._purifier_current = 0.62
        self._dissociator_current = 0.52
        self._dissociator_light = 4.65
        self._internal_top_heater_voltage = 1.46
        self._internal_bottom_heater_voltage = 0.94
        self._internal_side_heater_voltage = 1.39
        self._thermal_control_unit_heater_voltage = 2.54
        self._external_side_heater_voltage = 1.33
        self._external_bottom_heater_voltage = 1.13
        self._isolator_heater_voltage = 0.84
        self._tube_heater_voltage = 0.57
        self._boxes_temperature = 42.97
        self._boxes_current = 0.26
        self._ambient_temperature = 22.61
        self._cfield_voltage = 0.05
        self._varactor_diode_voltage = 3.62
        self._external_high_voltage_value = 3.50
        self._external_high_current_value = 16.11
        self._internal_high_voltage_value = 3.50
        self._internal_high_current_value = 14.16
        self._hydrogen_storage_pressure = 1.09
        self._hydrogen_storage_heater_voltage = 12.95
        self._pirani_heater_voltage = 11.36
        self._oscillator_100mhz_voltage = 0.63
        self._amplitude_405khz_voltage = 8.72
        self._oscillator_voltage = 4.29
        self._positive24vdc = 24.90
        self._positive15vdc = 15.78
        self._negative15vdc = -15.63
        self._positive5vdc = 5.16
        self._negative5vdc = -0.0
        self._positive8vdc = 8.01
        self._positive18vdc = 0.0
        self._lock100mhz = 4.88
        self._phase_lock_loop_lockstatus = False
        self._eol = "\r\n"
        try:
            self._simserver = SimulatorServer(
                self.MaserHost, self.MaserPort, self._logger
            )
        # pylint: disable=broad-exception-caught
        except Exception as ex:
            self._logger.error(
                "Catch already bound exceptions when trying use the init command"
            )
            self._logger.error(ex)
        self.update_buffer()
        self._lock = threading.Lock()
        self._randomise = False
        self.set_state(DevState.ON)

    def delete_device(self: MaserSimulator) -> None:
        """Delete resources allocated in init_device."""
        self._simserver.stopServer()

    # ----------
    # Commands
    # ----------

    @command
    def StopServer(self: MaserSimulator) -> None:
        """Test for network failure with close connection."""
        if not self._thread:
            with self._lock:
                self._randomise = False
                self._thread = None
        self._simserver.stopServer()

    @command
    def Randomise(self: MaserSimulator) -> None:
        """Periodically randomise the attribute values."""
        if self._thread is None:
            with self._lock:
                self._randomise = True
            self._thread = threading.Thread(
                target=self._generate_random_data, args=(), daemon=True
            )
            self._thread.start()
        else:
            self._randomise = False
            self._thread = None

    # ----------
    # Attributes
    # ----------

    def is_attribute_allowed(
        self: MaserSimulator, attr_req_type: tango.AttReqType
    ) -> bool:
        """
        Check if attribute is allowed to be read.

        :param attr_req_type: tango attribute type READ/WRITE
        :return: True if attribute is allowed else False
        """
        return self.get_state() not in [DevState.FAULT, DevState.OFF]

    @attribute(
        dtype="float",
        label="Ubatt.A [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def battery_voltage_a(self: MaserSimulator) -> float:
        """
        Report the voltage of battery A.

        :return: battery voltage A
        """
        return self._battery_voltage_a

    @battery_voltage_a.write  # type: ignore[no-redef]
    def battery_voltage_a(self: MaserSimulator, value: float) -> None:
        """
        Set the voltage of battery A.

        :param value: battery voltage A
        """
        self._battery_voltage_a = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="Ibatt.A [A]",
        unit="Amps",
        fisallowed=is_attribute_allowed,
    )
    def battery_current_a(self: MaserSimulator) -> float:
        """
        Report the current of battery A.

        :return: battery current A
        """
        return self._battery_current_a

    @battery_current_a.write  # type: ignore[no-redef]
    def battery_current_a(self: MaserSimulator, value: float) -> None:
        """
        Set the current of battery A.

        :param value: battery current A
        """
        self._battery_current_a = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="Ubatt.B [V]",
        unit="Volts",
        max_value=100,
        min_value=0,
        doc="Battery voltage B",
        fisallowed=is_attribute_allowed,
    )
    def battery_voltage_b(self: MaserSimulator) -> float:
        """
        Report the voltage of battery B.

        :return: battery voltage B
        """
        return self._battery_voltage_b

    @battery_voltage_b.write  # type: ignore[no-redef]
    def battery_voltage_b(self: MaserSimulator, value: float) -> None:
        """
        Set the voltage of battery B.

        :param value: battery voltage B
        """
        self._battery_voltage_b = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="Ibatt.B [A]",
        unit="Amps",
        fisallowed=is_attribute_allowed,
    )
    def battery_current_b(self: MaserSimulator) -> float:
        """
        Report the current of battery B.

        :return: battery current B
        """
        return self._battery_current_b

    @battery_current_b.write  # type: ignore[no-redef]
    def battery_current_b(self: MaserSimulator, value: float) -> None:
        """
        Set the current of battery B.

        :param value: battery current B
        """
        self._battery_current_b = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="Set.H [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def hydrogen_pressure_setting(self: MaserSimulator) -> float:
        """
        Report the hydrogen pressure setting.

        :return: hydrogen pressure setting
        """
        return self._hydrogen_pressure_setting

    @hydrogen_pressure_setting.write  # type: ignore[no-redef]
    def hydrogen_pressure_setting(self: MaserSimulator, value: float) -> None:
        """
        Set the hydrogen pressure setting.

        :param value: hydrogen pressure setting
        """
        self._hydrogen_pressure_setting = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="Meas.H [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def meas_h(self: MaserSimulator) -> float:
        """
        Report the measured hydrogen pressure.

        :return: hydrogen pressure measurement
        """
        return self._meas_h

    @meas_h.write  # type: ignore[no-redef]
    def meas_h(self: MaserSimulator, value: float) -> None:
        """
        Set the measured hydrogen pressure.

        :param value: hydrogen pressure measurement
        """
        self._meas_h = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="Ipurifiler [A]",
        unit="Amps",
        fisallowed=is_attribute_allowed,
    )
    def purifier_current(self: MaserSimulator) -> float:
        """
        Report the purifier current.

        :return: purifier current
        """
        return self._purifier_current

    @purifier_current.write  # type: ignore[no-redef]
    def purifier_current(self: MaserSimulator, value: float) -> None:
        """
        Set the purifier current.

        :param value: purifier current
        """
        self._purifier_current = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="dissociator_current [A]",
        unit="Amps",
        fisallowed=is_attribute_allowed,
    )
    def dissociator_current(self: MaserSimulator) -> float:
        """
        Report the dissociator current.

        :return: dissociator current
        """
        return self._dissociator_current

    @dissociator_current.write  # type: ignore[no-redef]
    def dissociator_current(self: MaserSimulator, value: float) -> None:
        """
        Set the dissociator current.

        :param value: dissociator current
        """
        self._dissociator_current = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="dissociator_light [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def dissociator_light(self: MaserSimulator) -> float:
        """
        Report the dissociator light.

        :return: dissociator light
        """
        return self._dissociator_light

    @dissociator_light.write  # type: ignore[no-redef]
    def dissociator_light(self: MaserSimulator, value: float) -> None:
        """
        Set the dissociator light.

        :param value: dissociator light
        """
        self._dissociator_light = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="ITheater [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def internal_top_heater_voltage(self: MaserSimulator) -> float:
        """
        Report the internal top heater voltage.

        :return: internal top heater voltage
        """
        return self._internal_top_heater_voltage

    @internal_top_heater_voltage.write  # type: ignore[no-redef]
    def internal_top_heater_voltage(self: MaserSimulator, value: float) -> None:
        """
        Set the internal top heater voltage.

        :param value: internal top heater voltage
        """
        self._internal_top_heater_voltage = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="IBheater [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def internal_bottom_heater_voltage(self: MaserSimulator) -> float:
        """
        Report the internal bottom heater voltage.

        :return: internal bottom heater voltage
        """
        return self._internal_bottom_heater_voltage

    @internal_bottom_heater_voltage.write  # type: ignore[no-redef]
    def internal_bottom_heater_voltage(self: MaserSimulator, value: float) -> None:
        """
        Set the internal bottom heater voltage.

        :param value: internal bottom heater voltage
        """
        self._internal_bottom_heater_voltage = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="ISheater [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def internal_side_heater_voltage(self: MaserSimulator) -> float:
        """
        Report the internal side heater voltage.

        :return: internal side heater voltage
        """
        return self._internal_side_heater_voltage

    @internal_side_heater_voltage.write  # type: ignore[no-redef]
    def internal_side_heater_voltage(self: MaserSimulator, value: float) -> None:
        """
        Set the internal side heater voltage.

        :param value: internal side heater voltage
        """
        self._internal_side_heater_voltage = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="UTCheater [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def thermal_control_unit_heater_voltage(self: MaserSimulator) -> float:
        """
        Report the thermal control unit heater voltage.

        :return: thermal control unit heater voltage
        """
        return self._thermal_control_unit_heater_voltage

    @thermal_control_unit_heater_voltage.write  # type: ignore[no-redef]
    def thermal_control_unit_heater_voltage(self: MaserSimulator, value: float) -> None:
        """
        Set the thermal control unit heater voltage.

        :param value: thermal control unit heater voltage
        """
        self._thermal_control_unit_heater_voltage = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="ESheater [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def external_side_heater_voltage(self: MaserSimulator) -> float:
        """
        Report the external side heater voltage.

        :return: external side heater voltage
        """
        return self._external_side_heater_voltage

    @external_side_heater_voltage.write  # type: ignore[no-redef]
    def external_side_heater_voltage(self: MaserSimulator, value: float) -> None:
        """
        Set the external side heater voltage.

        :param value: external side heater voltage
        """
        self._external_side_heater_voltage = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="EBheater [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def external_bottom_heater_voltage(self: MaserSimulator) -> float:
        """
        Report the external bottom heater voltage.

        :return: external bottom heater voltage
        """
        return self._external_bottom_heater_voltage

    @external_bottom_heater_voltage.write  # type: ignore[no-redef]
    def external_bottom_heater_voltage(self: MaserSimulator, value: float) -> None:
        """
        Set the external bottom heater voltage.

        :param value: external bottom heater voltage
        """
        self._external_bottom_heater_voltage = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="Iheater [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def isolator_heater_voltage(self: MaserSimulator) -> float:
        """
        Report the isolator heater voltage.

        :return: isolator heater voltage
        """
        return self._isolator_heater_voltage

    @isolator_heater_voltage.write  # type: ignore[no-redef]
    def isolator_heater_voltage(self: MaserSimulator, value: float) -> None:
        """
        Set the isolator heater voltage.

        :param value: isolator heater voltage
        """
        self._isolator_heater_voltage = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="Theater [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def tube_heater_voltage(self: MaserSimulator) -> float:
        """
        Report the tube heater voltage.

        :return: tube heater voltage
        """
        return self._tube_heater_voltage

    @tube_heater_voltage.write  # type: ignore[no-redef]
    def tube_heater_voltage(self: MaserSimulator, value: float) -> None:
        """
        Set the tube heater voltage.

        :param value: tube heater voltage
        """
        self._tube_heater_voltage = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="Boxestemp [DegC]",
        unit="DegC",
        fisallowed=is_attribute_allowed,
    )
    def boxes_temperature(self: MaserSimulator) -> float:
        """
        Report the boxes temperature.

        :return: boxes temperature
        """
        return self._boxes_temperature

    @boxes_temperature.write  # type: ignore[no-redef]
    def boxes_temperature(self: MaserSimulator, value: float) -> None:
        """
        Set the boxes temperature.

        :param value: boxes temperature
        """
        self._boxes_temperature = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="boxes_current [A]",
        unit="Amps",
        fisallowed=is_attribute_allowed,
    )
    def boxes_current(self: MaserSimulator) -> float:
        """
        Report the boxes current.

        :return: boxes current
        """
        return self._boxes_current

    @boxes_current.write  # type: ignore[no-redef]
    def boxes_current(self: MaserSimulator, value: float) -> None:
        """
        Set the boxes current.

        :param value: boxes current
        """
        self._boxes_current = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="AmbTemp [DegC]",
        unit="DegC",
        fisallowed=is_attribute_allowed,
    )
    def ambient_temperature(self: MaserSimulator) -> float:
        """
        Report the ambient temperature.

        :return: ambient temperature
        """
        return self._ambient_temperature

    @ambient_temperature.write  # type: ignore[no-redef]
    def ambient_temperature(self: MaserSimulator, value: float) -> None:
        """
        Set the ambient temperature.

        :param value: ambient temperature
        """
        self._ambient_temperature = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="cfield_voltage [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def cfield_voltage(self: MaserSimulator):
        """
        Report the C-field voltage.

        :return: C-field voltage
        """
        return self._cfield_voltage

    @cfield_voltage.write  # type: ignore[no-redef]
    def cfield_voltage(self: MaserSimulator, value: float) -> None:
        """
        Set the C-field voltage.

        :param value: C-field voltage
        """
        self._cfield_voltage = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="varactor_diode_voltage [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def varactor_diode_voltage(self: MaserSimulator) -> float:
        """
        Report the varactor voltage.

        :return: varactor voltage
        """
        return self._varactor_diode_voltage

    @varactor_diode_voltage.write  # type: ignore[no-redef]
    def varactor_diode_voltage(self: MaserSimulator, value: float) -> None:
        """
        Set the varactor voltage.

        :param value: varactor voltage
        """
        self._varactor_diode_voltage = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="UHText [Kv]",
        unit="KV",
        fisallowed=is_attribute_allowed,
    )
    def external_high_voltage_value(self: MaserSimulator) -> float:
        """
        Report the external high voltage value.

        :return: external high voltage value
        """
        return self._external_high_voltage_value

    @external_high_voltage_value.write  # type: ignore[no-redef]
    def external_high_voltage_value(self: MaserSimulator, value: float) -> None:
        """
        Set the external high voltage value.

        :param value: external high voltage value
        """
        self._external_high_voltage_value = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="IHText [uA]",
        unit="uAmps",
        fisallowed=is_attribute_allowed,
    )
    def external_high_current_value(self: MaserSimulator) -> float:
        """
        Report the external high current value.

        :return: external high current value
        """
        return self._external_high_current_value

    @external_high_current_value.write  # type: ignore[no-redef]
    def external_high_current_value(self: MaserSimulator, value: float) -> None:
        """
        Set the external high current value.

        :param value: external high current value
        """
        self._external_high_current_value = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="UHTint [kV]",
        unit="KV",
        fisallowed=is_attribute_allowed,
    )
    def internal_high_voltage_value(self: MaserSimulator) -> float:
        """
        Report the internal high voltage value.

        :return: internal high voltage value
        """
        return self._internal_high_voltage_value

    @internal_high_voltage_value.write  # type: ignore[no-redef]
    def internal_high_voltage_value(self: MaserSimulator, value: float) -> None:
        """
        Set the internal high voltage value.

        :param value: internal high voltage value
        """
        self._internal_high_voltage_value = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="IHTint [uA]",
        unit="uAmps",
        fisallowed=is_attribute_allowed,
    )
    def internal_high_current_value(self: MaserSimulator) -> float:
        """
        Report the internal high current value.

        :return: internal high current value
        """
        return self._internal_high_current_value

    @internal_high_current_value.write  # type: ignore[no-redef]
    def internal_high_current_value(self: MaserSimulator, value: float) -> None:
        """
        Set the internal high current value.

        :param value: internal high current value
        """
        self._internal_high_current_value = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="hydrogen_storage_pressure [bar]",
        unit="Bar",
        fisallowed=is_attribute_allowed,
    )
    def hydrogen_storage_pressure(self: MaserSimulator) -> float:
        """
        Report the hydrogen storage pressure.

        :return: hydrogen storage pressure
        """
        return self._hydrogen_storage_pressure

    @hydrogen_storage_pressure.write  # type: ignore[no-redef]
    def hydrogen_storage_pressure(self: MaserSimulator, value: float) -> None:
        """
        Set the hydrogen storage pressure.

        :param value: hydrogen storage pressure
        """
        self._hydrogen_storage_pressure = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="hydrogen_storage_heater_voltage [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def hydrogen_storage_heater_voltage(self: MaserSimulator) -> float:
        """
        Report the hydrogen storage heater voltage.

        :return: hydrogen storage heater voltage
        """
        return self._hydrogen_storage_heater_voltage

    @hydrogen_storage_heater_voltage.write  # type: ignore[no-redef]
    def hydrogen_storage_heater_voltage(self: MaserSimulator, value: float) -> None:
        """
        Set the hydrogen storage heater voltage.

        :param value: hydrogen storage heater voltage
        """
        self._hydrogen_storage_heater_voltage = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="pirani_heater_voltage [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def pirani_heater_voltage(self: MaserSimulator) -> float:
        """
        Report the pirani heater voltage.

        :return: pirani heater voltage
        """
        return self._pirani_heater_voltage

    @pirani_heater_voltage.write  # type: ignore[no-redef]
    def pirani_heater_voltage(self: MaserSimulator, value: float) -> None:
        """
        Set the pirani heater voltage.

        :param value: pirani heater voltage
        """
        self._pirani_heater_voltage = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="oscillator_voltage 100MHz",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def oscillator_100mhz_voltage(self: MaserSimulator) -> float:
        """
        Report the oscillator_voltage 100MHz.

        :return: oscillator_voltage 100MHz
        """
        return self._oscillator_100mhz_voltage

    @oscillator_100mhz_voltage.write  # type: ignore[no-redef]
    def oscillator_100mhz_voltage(self: MaserSimulator, value: float) -> None:
        """
        Set the oscillator_voltage 100MHz.

        :param value: oscillator_voltage 100MHz
        """
        self._oscillator_100mhz_voltage = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="amplitude_405khz_voltage [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def amplitude_405khz_voltage(self: MaserSimulator) -> float:
        """
        Report the 405 kHz Amplitude.

        :return: the 405 kHz Amplitude
        """
        return self._amplitude_405khz_voltage

    @amplitude_405khz_voltage.write  # type: ignore[no-redef]
    def amplitude_405khz_voltage(self: MaserSimulator, value: float) -> None:
        """
        Set the 405 kHz Amplitude.

        :param value: the 405 kHz Amplitude
        """
        self._amplitude_405khz_voltage = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="oscillator_voltage [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def oscillator_voltage(self: MaserSimulator) -> float:
        """
        Report the OCXO varicap voltage.

        :return: the OCXO varicap voltage
        """
        return self._oscillator_voltage

    @oscillator_voltage.write  # type: ignore[no-redef]
    def oscillator_voltage(self: MaserSimulator, value: float) -> None:
        """
        Set the OCXO varicap voltage.

        :param value: the OCXO varicap voltage
        """
        self._oscillator_voltage = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="+24Vdc [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def positive24vdc(self: MaserSimulator) -> float:
        """
        Report the +24 V supply voltage.

        :return: +24 V supply voltage
        """
        return self._positive24vdc

    @positive24vdc.write  # type: ignore[no-redef]
    def positive24vdc(self: MaserSimulator, value: float) -> None:
        """
        Set the +24 V supply voltage.

        :param value: +24 V supply voltage
        """
        self._positive24vdc = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="+15Vdc [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def positive15vdc(self: MaserSimulator) -> float:
        """
        Report the +15 V supply voltage.

        :return: +15 V supply voltage
        """
        return self._positive15vdc

    @positive15vdc.write  # type: ignore[no-redef]
    def positive15vdc(self: MaserSimulator, value: float) -> None:
        """
        Set the +15 V supply voltage.

        :param value: +15 V supply voltage
        """
        self._positive15vdc = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="-15Vdc [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def negative15vdc(self: MaserSimulator) -> float:
        """
        Report the -15 V supply voltage.

        :return: -15 V supply voltage
        """
        return self._negative15vdc

    @negative15vdc.write  # type: ignore[no-redef]
    def negative15vdc(self: MaserSimulator, value: float) -> None:
        """
        Set the -15 V supply voltage.

        :param value: -15 V supply voltage
        """
        self._negative15vdc = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="+5Vdc [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def positive5vdc(self: MaserSimulator) -> float:
        """
        Report the +5 V supply voltage.

        :return: +5 V supply voltage
        """
        return self._positive5vdc

    @positive5vdc.write  # type: ignore[no-redef]
    def positive5vdc(self: MaserSimulator, value: float) -> None:
        """
        Set the +5 V supply voltage.

        :param value: +5 V supply voltage
        """
        self._positive5vdc = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="-5Vdc [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def negative5vdc(self: MaserSimulator) -> float:
        """
        Report the -5 V supply voltage.

        :return: -5 V supply voltage
        """
        return self._negative5vdc

    @negative5vdc.write  # type: ignore[no-redef]
    def negative5vdc(self: MaserSimulator, value: float) -> None:
        """
        Set the -5 V supply voltage.

        :param value: -5 V supply voltage
        """
        self._negative5vdc = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="+8Vdc [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def positive8vdc(self: MaserSimulator) -> float:
        """
        Report the +8 V supply voltage.

        :return: +8 V supply voltage
        """
        return self._positive8vdc

    @positive8vdc.write  # type: ignore[no-redef]
    def positive8vdc(self: MaserSimulator, value: float) -> None:
        """
        Set the +8 V supply voltage.

        :param value: +8 V supply voltage
        """
        self._positive8vdc = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="+18Vdc [V]",
        unit="Volts",
        fisallowed=is_attribute_allowed,
    )
    def positive18vdc(self: MaserSimulator) -> float:
        """
        Report the +18 V supply voltage.

        :return: +18 V supply voltage
        """
        return self._positive18vdc

    @positive18vdc.write  # type: ignore[no-redef]
    def positive18vdc(self: MaserSimulator, value: float) -> None:
        """
        Set the +18 V supply voltage.

        :param value: +18 V supply voltage
        """
        self._positive18vdc = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="lock100MHz",
        unit="",
        fisallowed=is_attribute_allowed,
    )
    def lock100mhz(self: MaserSimulator) -> float:
        """
        Report the lock 100MHz status.

        :return: the lock 100MHz status
        """
        return self._lock100mhz

    @attribute(
        dtype="bool",
        label="pllLockStatus",
        unit="",
        doc="Main PLL lock status",
        fisallowed=is_attribute_allowed,
    )
    def phase_lock_loop_lockstatus(self: MaserSimulator) -> bool:
        """
        Report the main PLL lock status.

        :return: main PLL lock status
        """
        return self._phase_lock_loop_lockstatus

    def update_buffer(self: MaserSimulator) -> None:
        """Create a new maser buffer string and update the server."""
        newbuff = (
            "$"
            + f"{int(self._battery_voltage_a / 0.024414):03x}"
            + f"{int(self._battery_current_a / 0.0012207):03x}"
            + f"{int(self._battery_voltage_b / 0.024414):03x}"
            + f"{int(self._battery_current_b / 0.0012207):03x}"
            + f"{int(self._hydrogen_pressure_setting / 0.0036621):03x}"
            + f"{int(self._meas_h / 0.0012207):03x}"
            + f"{int(self._purifier_current / 0.0012207):03x}"
            + f"{int(self._dissociator_current / 0.0012207):03x}"
            + f"{int(self._dissociator_light / 0.0012207):03x}"
            + f"{int(self._internal_top_heater_voltage / 0.0048828):03x}"
            + f"{int(self._internal_bottom_heater_voltage / 0.0048828):03x}"
            + f"{int(self._internal_side_heater_voltage / 0.0048828):03x}"
            + f"{int(self._thermal_control_unit_heater_voltage / 0.0048828):03x}"
            + f"{int(self._external_side_heater_voltage / 0.0048828):03x}"
            + f"{int(self._external_bottom_heater_voltage / 0.0048828):03x}"
            + f"{int(self._isolator_heater_voltage / 0.0048828):03x}"
            + f"{int(self._tube_heater_voltage / 0.0048828):03x}"
            + f"{int(self._boxes_temperature / 0.024414):03x}"
            + f"{int(self._boxes_current / 0.0012207):03x}"
            + f"{int(self._ambient_temperature / 0.012207):03x}"
            + f"{int(self._cfield_voltage / 0.0024414):03x}"
            + f"{int(self._varactor_diode_voltage / 0.0024414):03x}"
            + f"{int(self._external_high_voltage_value / 0.00195312):03x}"
            + f"{int(self._external_high_current_value / 0.24414):03x}"
            + f"{int(self._internal_high_voltage_value / 0.00195312):03x}"
            + f"{int(self._internal_high_current_value / 0.24414):03x}"
            + f"{int(self._hydrogen_storage_pressure / 0.0048828):03x}"
            + f"{int(self._hydrogen_storage_heater_voltage / 0.0061035):03x}"
            + f"{int(self._pirani_heater_voltage / 0.0061035):03x}"
            + f"{int(self._oscillator_100mhz_voltage / 0.0024414):03x}"
            + f"{int(self._amplitude_405khz_voltage / 0.0036621):03x}"
            + f"{int(self._oscillator_voltage / 0.0024414):03x}"
            + f"{int(self._positive24vdc / 0.097656):02x}"
            + f"{int(self._positive15vdc / 0.078125):02x}"
            + f"{int(self._negative15vdc / -0.078125):02x}"
            + f"{int(self._positive5vdc / 0.03906):02x}"
            + f"{int(self._negative5vdc / -0.03906):02x}"
            + f"{int(self._positive8vdc / 0.03906):02x}"
            + f"{int(self._positive18vdc):02x}"
            + f"{int(self._lock100mhz / 0.03906):02x}"
            + f"{int(self._phase_lock_loop_lockstatus):01x}"
        )
        self._simserver.update(newbuff + self._eol)

    def _generate_random_data(self: MaserSimulator) -> None:
        """Generate random noise to add to attribute value."""
        while self._randomise:
            noise = -0.05 + 0.1 * random.random()
            self._battery_voltage_a = 27.66 + noise
            self._battery_current_a = 0.08 + noise / 100
            self._battery_voltage_b = 28.10 + noise
            self._battery_current_b = 3.09 + noise
            self._hydrogen_pressure_setting = 5.83 + noise
            self._meas_h = 1.53 + noise
            self._purifier_current = 0.62 + noise / 10
            self._dissociator_current = 0.52 + noise / 10
            self._dissociator_light = 4.65 + noise
            self._internal_top_heater_voltage = 1.46 + noise
            self._internal_bottom_heater_voltage = 0.94 + noise / 10
            self._internal_side_heater_voltage = 1.39 + noise
            self._thermal_control_unit_heater_voltage = 2.54 + noise
            self._external_side_heater_voltage = 1.33 + noise
            self._external_bottom_heater_voltage = 1.13 + noise
            self._isolator_heater_voltage = 0.84 + noise / 10
            self._tube_heater_voltage = 0.57 + noise / 10
            self._boxes_temperature = 42.97 + noise
            self._boxes_current = 0.26 + noise / 10
            self.update_buffer()
            time.sleep(1.0)


# ----------
# Run server
# ----------


def main(*args: str, **kwargs: str) -> int:
    """
    Entry point for module.

    :param args: positional arguments
    :param kwargs: named arguments
    :return: exit code
    """
    return MaserSimulator.run_server(args=args or None, **kwargs)


if __name__ == "__main__":
    main()
