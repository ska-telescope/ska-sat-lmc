# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module implements the PhaseMicrostepper driver."""

from __future__ import annotations  # allow forward references in type hints

__all__ = ["PhaseMicroStepperDriver"]

import logging
import re
from typing import Any, Callable, Final

from ska_control_model import CommunicationStatus
from ska_tango_base.poller import PollingComponentManager

from ska_sat_lmc.clocks.comms import TcpSocket, UdpSocket
from ska_sat_lmc.component.component_manager import SatComponentManager


# pylint: disable=too-many-instance-attributes
class PhaseMicroStepperDriver(
    PollingComponentManager[bool, dict[str, Any]], SatComponentManager
):
    """Device Server for the SAT.LMC PhaseMicrostepper (100MHz)."""

    # pylint: disable=too-many-arguments
    def __init__(
        self: PhaseMicroStepperDriver,
        logger: logging.Logger,
        host: str,
        udp_port: int,
        tcp_port: int,
        communication_state_changed: Callable,
        component_state_changed: Callable,
        update_rate: float = 5.0,
    ) -> None:
        """
        Initialise the attributes and properties of the PhaseMicroStepper.

        :param logger: a logger for this object to use
        :param host: the hostname of the PhaseMicroStepper
        :param udp_port: the udp port number
        :param tcp_port: the tcp port number
        :param communication_state_changed: callback to be
            called when the status of the communications channel between
            the component manager and its component changes
        :param component_state_changed: callback to be called when the
            component faults (or stops faulting)
        :param update_rate: how often updates to attribute values should
            be provided. This is not necessarily the same as the rate at
            which the instrument is polled. For example, the instrument
            may be polled every 0.1 seconds, thus ensuring that any
            invoked commands or writes will be executed promptly.
            However, if the `update_rate` is 5.0, then routine reads of
            instrument values will only occur every 50th poll (i.e.
            every 5 seconds).
        """
        self._logger = logger
        self._poll_rate: Final = 0.1
        self._max_tick: Final = int(update_rate / self._poll_rate)

        # We'll count ticks upwards, but start at the maximum so that
        # our initial update request occurs as soon as possible.
        self._tick = self._max_tick

        self._communication_state_changed = communication_state_changed
        self._component_state_changed = component_state_changed
        super().__init__(
            logger,
            communication_state_changed,
            component_state_changed,
            poll_rate=self._poll_rate,
        )
        self._logger.info("Initialising PhaseMicroStepper device server")
        self._host: str = host
        self._udp_port: int = udp_port
        self._tcp_port: int = tcp_port
        self._udp: UdpSocket
        self._timeout = update_rate
        self._eol = b"\r\n"

        self._tcp = TcpSocket(
            self._host,
            self._tcp_port,
            eol=self._eol,
            timeout=self._timeout,
            logger=logger,
        )
        self._logger.info("Initialised phasemicrostepper")

    def start_communicating(self: PhaseMicroStepperDriver) -> None:
        """Establish communication with the phasemicrostepper."""
        super().start_communicating()
        self._connect_to_phasemicrostepper()

    def _connect_to_phasemicrostepper(self: PhaseMicroStepperDriver) -> None:
        """Make a UDP connection to the PhaseMicroStepper and start monitor thread."""
        self.logger.info(f"Opening udp connection on {self._host}:{self._udp_port}")
        self._udp = UdpSocket(
            self._host,
            self._udp_port,
            eol=self._eol,
            timeout=self._timeout,
            logger=self._logger,
        )
        self._udp.connect()
        if self._udp.is_connected:
            self._communication_state_changed(CommunicationStatus.ESTABLISHED)

    def stop_communicating(self: PhaseMicroStepperDriver) -> None:
        """Stop communicating with the PhaseMicroStepper."""
        super().stop_communicating()
        self._udp.close()

    def set_offset_frequency(self: PhaseMicroStepperDriver, offset: int) -> str:
        """
        Set the offset frequency.

        Sets the  OUT signals respect to the frequency of 100MHz IN
        signal, in units of 1e-20. Resolution is better than 3e-20. The
        acceptable range is from -1000000000000 to 1000000000000.

        :param offset: the offset frequency
        :return: the command and converted frquency in units of 1e-20
        """
        cmd = f"setF {offset}\r\n".encode()
        buff = self._tcp.write_readline(cmd, eol=self._eol, timeout=3).decode()
        self._tcp.close()
        return buff.rstrip()

    def get_offset_frequency(self: PhaseMicroStepperDriver) -> str:
        """
        Return the offset frequency set in the last setF command.

        :return: the command and offset frequency in units of 1e-20
        """
        cmd = "getF\r\n".encode()
        buff = self._tcp.write_readline(cmd, eol=self._eol, timeout=3).decode()
        self._tcp.close()
        return buff.rstrip()

    def advance_phase(self: PhaseMicroStepperDriver, advance: int) -> str:
        """
        Advance the output phase of all OUT signals.in units of 1e-15s.

        The resolution is about 1e-13. Acceptable range is from -5000000
        to +5000000.

        :param advance: advance by this value
        :return: the command string & value
        """
        cmd = f"advPh {advance}\r\n".encode()
        buff = self._tcp.write_readline(cmd, eol=self._eol, timeout=3).decode()
        self._tcp.close()
        return buff.rstrip()

    def advance_10megahertz(self: PhaseMicroStepperDriver, advance: int) -> str:
        """
        Advance the output phase of 10MHz OUT signals.

        Value in units of 10ns. Acceptable range is from 0 to +9.

        :param advance: advance by this value
        :return: the command string & value
        """
        cmd = f"adv10M {advance}\r\n".encode()
        buff = self._tcp.write_readline(cmd, eol=self._eol, timeout=3).decode()
        self._tcp.close()
        return buff.rstrip()

    def advance_pps(self: PhaseMicroStepperDriver, advance: int) -> str:
        """
        Advance the phase of PPS OUT signals in units of 10ns.

        The acceptable range is limited from -99000000 to 30000000.

        :param advance: advance by this value
        :return: the command string & value
        """
        cmd = f"advPPS {advance}\r\n".encode()
        buff = self._tcp.write_readline(cmd, eol=self._eol, timeout=3).decode()
        self._tcp.close()
        return buff.rstrip()

    def sync_pps(self: PhaseMicroStepperDriver) -> tuple[int, str]:
        """
        Shift the rising edge of 1PPS OUT signals in order to align it with 1PPS IN.

        The value in the second string is 0 if the operation is OK, 1 in
        case of error. In most of the cases, the error is due to the
        absence of a valid signal on the 1PPS IN connector.

        :return: the command string and second string as “syncPPS done,
            Error = 0”
        """
        cmd = "syncPPS\r\n".encode()
        nb_lines = 2
        buff = self._tcp.write_readlines(cmd, nb_lines, eol=self._eol, timeout=3)
        self._tcp.close()
        reply = re.split("\r\n", buff.decode())
        rc = 1 if "Error = 1" in reply[1] else 0
        return (rc, reply[1])

    def set_ip_address(self: PhaseMicroStepperDriver, ip_address: str) -> str:
        """
        Set the IP address of the EOG and closes the TCP connection.

        :param ip_address: the new IP address
        :return: the command string & IP address set
        """
        cmd = f"setIPa {ip_address}\r\n".encode()
        buff = self._tcp.write_readline(cmd, eol=self._eol, timeout=3).decode()
        self._tcp.close()
        return buff.rstrip()

    def set_udp_address(self: PhaseMicroStepperDriver, udp_address: str) -> str:
        """
        Set the UDP destination address.

        If network address is 255.255.255.255, the UDP message is
        broadcast to all address.

        :param udp_address: the new UDP address
        :return: the command string & UDP address set
        """
        cmd = f"setUDPa {udp_address}\r\n".encode()
        buff = self._tcp.write_readline(cmd, eol=self._eol, timeout=3).decode()
        self._tcp.close()
        return buff.rstrip()

    def get_request(self: PhaseMicroStepperDriver) -> bool:
        """
        Check whether the attributes should be read on the next poll.

        :return: true if attributes are read on  the next poll.
        """
        poll_request = False
        self._tick += 1
        if self._tick > self._max_tick:
            self.logger.debug(f"Tick {self._tick} >= {self._max_tick}.")
            poll_request = True
            self._tick = 0
            self.logger.debug(f"Returning {poll_request} for next poll.")
        return poll_request

    def poll(self: PhaseMicroStepperDriver, poll_request: bool) -> dict[str, Any]:
        """
        Poll the hardware.

        Connect to the hardware, write any values that are to be
        written, and then read all values.

        :param poll_request: whether to perform in this poll.

        :return: responses to queries in this poll
        """
        self.logger.info("Poller is initiating next poll.")
        poll_response = {}
        try:
            buff = self._udp.readline(eol=self._eol, timeout=3).decode()
            self._logger.info(f"Readline returns: {buff}")
            # Check the integrity of the received message
            if buff == "":
                self._logger.info("readline() timeout")
                self._communication_state_changed(CommunicationStatus.NOT_ESTABLISHED)
            elif not buff.startswith("EOG"):
                self._logger.error(f"Received bad message format from {self._host}")
                self._communication_state_changed(CommunicationStatus.NOT_ESTABLISHED)
            else:
                poll_response = self._decode_attribute_properties(buff)
                if poll_response:
                    self._communication_state_changed(CommunicationStatus.ESTABLISHED)
        # pylint: disable=broad-exception-caught
        except Exception:
            self._logger.error(
                f"Received timeout from {self._host}. Network or power failure?"
            )
        return poll_response

    def _decode_attribute_properties(
        self: PhaseMicroStepperDriver, buff: str
    ) -> dict[str, Any]:
        """
        Extract the property values from the given buffer and convert to real values.

        :param buff: the PhaseMicroStepper string

        :return: the decoded PhaseMicroStepper string as a dict of data values
        """
        decoded_data: dict[str, Any] = {}
        values = buff.split()
        try:
            decoded_data["serial_number"] = int(values[0][3:])
            decoded_data["first_internal_oscillator"] = float(values[1].strip(","))
            decoded_data["second_internal_oscillator"] = float(values[2].strip(","))
            decoded_data["internal_temperature"] = float(values[3].strip(","))
        except ValueError as ex:
            self._logger.error(f"Attribute decoding failed {repr(ex)}")
        return decoded_data

    def poll_succeeded(
        self: PhaseMicroStepperDriver, poll_response: dict[str, Any]
    ) -> None:
        """
        Handle the receipt of new polling values.

        This is a hook called by the poller when values have been read
        during a poll.

        :param poll_response: response to the poll, including any values
            read.
        """
        if poll_response:
            self.logger.info(f"Result from successful poll: {poll_response} ")
            if self._component_state_changed is not None:
                self.logger.debug("Pushing updates.............")
                self._component_state_changed(fault=False, **poll_response)
        else:
            self._logger.debug("Null response, setting fault status true")
            if self._component_state_changed is not None:
                self._component_state_changed(fault=True)

    def polling_stopped(self: PhaseMicroStepperDriver) -> None:
        """
        Respond to polling having stopped.

        This is a hook called by the poller when it stops polling.
        """
        self.logger.debug("Polling has stopped.")

        # Set to max here so that if/when polling restarts, an update is
        # requested as soon as possible.
        self._tick = self._max_tick

        # Not calling super().polling_stopped() here,
        # because ska-tango-base inappropriately pushes power=UNKNOWN,
        self._communication_state_changed(CommunicationStatus.DISABLED)
