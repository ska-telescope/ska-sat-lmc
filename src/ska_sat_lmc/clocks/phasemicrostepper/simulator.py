# type: ignore
# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module implements a standlone phasemicrostepper simulator."""

from __future__ import annotations  # allow forward references in type hints

import logging
import random
import socket
import threading
import time
from typing import Any, Callable

import tango
from tango import DevState
from tango.server import Device, attribute, command, device_property

__all__ = ["PhaseMicroStepperSimulator"]


# pylint: disable=too-many-instance-attributes,too-few-public-methods
class TcpSimulatorServer:
    """Simulator server."""

    def __init__(self: TcpSimulatorServer, *args: Any, **kwargs: Any) -> None:
        """
        Initialise the server.

        :param args: positional arguments
        :param kwargs: named arguments
        """
        # Setup the default values.
        self._lock = threading.Lock()
        self._host = args[0]
        self._port = args[1]
        self._logger = args[2]
        self._streaming = False
        self._stop_server = False
        self._offset_frequency = 0
        self._buff = ""
        self._eol = b"\r\n"
        # Create a TCP/IP socket
        self._listen_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._listen_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # Bind the socket to the port
        server_address = (self._host, self._port)
        self._logger.info(f"Server starting up on {self._host} port {self._port}")
        self._listen_sock.bind(server_address)
        # Listen for incoming connections
        self._listen_sock.listen(12)
        self._thread = threading.Thread(target=self._do_write, args=(), daemon=True)
        self._logger.info("Thread task starting now")
        self._thread.start()

    def _do_write(self: TcpSimulatorServer) -> None:
        """Task to wait for connections."""
        while True:
            self._logger.info("Waiting for a client connection")
            connection, client_address = self._listen_sock.accept()
            try:
                self._logger.info(f"Connection from client {client_address}")
                msg = connection.recvmsg(1024)
                cmd = msg[0].decode().strip()
                if "syncPPS" in cmd:
                    reply1 = cmd + "\r\n"
                    connection.sendall(reply1.encode())
                    cmd = "syncPPS done, Error = 0"
                elif "setF" in cmd:
                    tokens = cmd.split()
                    self._offset_frequency = int(tokens[1])
                elif "getF" in cmd:
                    tokens = cmd.split()
                    self._offset_frequency = int(tokens[1])
                    cmd = tokens[0] + " " + str(self._offset_frequency)
                elif cmd not in [
                    "setIPa",
                    "setUDPa",
                    "advPh",
                    "adv10M",
                    "advPPS",
                ]:
                    self._logger.error("Invalid command")
                    break
                reply1 = cmd + "\r\n"
                connection.sendall(reply1.encode())
            # pylint: disable=broad-exception-caught
            except Exception as ex:
                self._logger.info(f"Closing client connection {ex}")
                connection.close()
            finally:
                # Clean up the connection
                self._logger.info("finally")
                connection.close()
                self._stop_server = False


class UdpSimulatorServer:
    """Simulator server."""

    def __init__(self: UdpSimulatorServer, *args: Any, **kwargs: Any) -> None:
        """
        Initialise the server.

        :param args: positional arguments
        :param kwargs: named arguments
        """
        # Setup the default values.
        self._lock = threading.Lock()
        self._host = args[0]
        self._port = int(args[1])
        self._logger = args[2]
        self._streaming = False
        self._stop_server = False
        self._buff = ""
        # Create a UDP/IP socket
        self._listen_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._logger.info(f"Server starting up on {self._host} port {self._port}")
        self._thread = threading.Thread(target=self._do_write, args=(), daemon=True)
        self._logger.info("Thread task starting now")
        self._thread.start()

    def stop_streaming(self: UdpSimulatorServer) -> None:
        """Stop sending the buffer to connected device."""
        with self._lock:
            self._streaming = False

    def start_streaming(self: UdpSimulatorServer) -> None:
        """Start sending the buffer to connected device."""
        with self._lock:
            self._streaming = True

    def stop_server(self: UdpSimulatorServer) -> None:
        """Stop the server."""
        with self._lock:
            self._stop_server = True

    def update(self: UdpSimulatorServer, buff: str) -> None:
        """
        Update the buffer with changed attribute values.

        :param buff: new phasemicrostepper string
        """
        with self._lock:
            self._buff = buff
        self._logger.info(f"Update the local buffer {buff.rstrip()}")

    def _do_write(self: UdpSimulatorServer) -> None:
        """Task to wait for connections."""
        while True:
            self._logger.info("Waiting for a client connection")
            try:
                while not self._stop_server:
                    with self._lock:
                        if self._streaming:
                            self._logger.info(
                                f"Sending the buffer {self._buff.rstrip()}"
                            )
                            self._listen_sock.sendto(
                                self._buff.encode(), (self._host, self._port)
                            )
                            self._logger.info(
                                f"Sent buffer to {self._host}:{self._port}"
                            )
                    time.sleep(1)
            # pylint: disable=broad-exception-caught
            except Exception as ex:
                self._logger.info("Closing client connection", ex)
            finally:
                self._logger.info("finally")
                self._stop_server = False


class PhaseMicroStepperSimulator(Device):
    """Communication Control device simulator for SAT.LMC."""

    # -----------------
    # Device Properties
    # -----------------
    Host = device_property(dtype=str, default_value="localhost")
    UDPport = device_property(dtype=int, default_value=3100)
    TCPport = device_property(dtype=int, default_value=5000)

    # ---------------
    # General methods
    # ---------------
    def __init__(self: PhaseMicroStepperSimulator, *args: Any, **kwargs: Any) -> None:
        """
        Initialise this device object.

        :param args: positional args to the init
        :param kwargs: keyword args to the init
        """
        # We aren't supposed to define initialisation methods for Tango
        # devices; we are only supposed to define an `init_device` method. But
        # we insist on doing so here, just so that we can define some
        # attributes, thereby stopping the linters from complaining about
        # "attribute-defined-outside-init" etc. We still need to make sure that
        # `init_device` re-initialises any values defined in here.
        super().__init__(*args, **kwargs)
        self._generate_randomise_task: Callable
        self._randomise: bool
        self._thread: threading.Thread | None
        self._simserver: UdpSimulatorServer | None
        self._first_internal_oscillator: float
        self._second_internal_oscillator: float
        self._internal_temperature: float
        self._offset_frequency: float
        self._eol: str

    def init_device(self: PhaseMicroStepperSimulator) -> None:
        """Initialise the device."""
        Device.init_device(self)
        self.set_state(DevState.INIT)
        logging.basicConfig(level=logging.INFO)
        self._logger = logging.getLogger(self.__class__.__name__)
        # set  default values
        self._first_internal_oscillator = self._default_oscillator1 = 0.96
        self._second_internal_oscillator = self._default_oscillator2 = 0.96
        self._internal_temperature = self._default_temperature = 45.67
        self._offset_frequency = 0
        self._eol = "\r\n"
        try:
            self._simserver = UdpSimulatorServer(self.Host, self.UDPport, self._logger)
        # pylint: disable=broad-exception-caught
        except Exception as ex:
            self._logger.error(
                "Catch already bound exceptions when trying use the init command"
            )
            self._logger.error(repr(ex))
        try:
            self._tcpsimserver = TcpSimulatorServer(
                self.Host, self.TCPport, self._logger
            )
        # pylint: disable=broad-exception-caught
        except Exception as ex:
            self._logger.error(
                "Catch already bound exceptions when trying use the init command"
            )
            self._logger.error(repr(ex))

        self.update_buffer()
        self.startStreaming()
        self._lock = threading.Lock()
        self._generate_randomise_task = None
        self._randomise = False
        self.set_state(DevState.ON)

    def delete_device(self: PhaseMicroStepperSimulator) -> None:
        """Delete resources allocated in init_device."""
        self._simserver.stop_server()
        self._simserver = None

    # ----------
    # Commands
    # ----------

    @command
    def stopStreaming(self: PhaseMicroStepperSimulator) -> None:
        """Test stop streaming."""
        self.set_state(DevState.ON)
        if not self._generate_randomise_task:
            with self._lock:
                self._randomise = False
                self._generate_randomise_task = None
        self._simserver.stop_streaming()

    @command
    def startStreaming(self: PhaseMicroStepperSimulator) -> None:
        """Test if recovers after streaming stopped."""
        self.set_state(DevState.RUNNING)
        self._simserver.start_streaming()

    @command
    def stopServer(self: PhaseMicroStepperSimulator) -> None:
        """Test for network failure with close connection."""
        if not self._generate_randomise_task:
            with self._lock:
                self._randomise = False
                self._generate_randomise_task = None
        self._simserver.stop_server()

    @command
    def Randomise(self: PhaseMicroStepperSimulator) -> None:
        """Periodically randomise the attribute values."""
        if not self._generate_randomise_task:
            with self._lock:
                self._randomise = True
            self._thread = threading.Thread(
                target=self._generate_random_data, args=(), daemon=True
            )
            self._thread.start()
        else:
            self._randomise = False
            self._generate_randomise_task = None

    # ----------
    # Attributes
    # ----------

    def is_attribute_allowed(
        self: PhaseMicroStepperSimulator, attr_req_type: tango.AttReqType
    ) -> bool:
        """
        Check if attribute is allowed to be read.

        :param attr_req_type: tango attribute type READ/WRITE
        :return: True if attribute is allowed else False
        """
        return self.get_state() not in [DevState.FAULT, DevState.OFF]

    @attribute(
        dtype="float",
        label="oscillator 1",
        fisallowed=is_attribute_allowed,
    )
    def first_internal_oscillator(self: PhaseMicroStepperSimulator) -> float:
        """
        Return the value of the first internal oscillator.

        :return: the value of the first internal oscillator.
        """
        return self._first_internal_oscillator

    @first_internal_oscillator.write  # type: ignore[no-redef]
    def first_internal_oscillator(
        self: PhaseMicroStepperSimulator, value: float
    ) -> None:
        """
        Set the value of the first internal oscillator.

        :param value: the first internal oscillator
        """
        self._first_internal_oscillator = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="oscillator 2",
        fisallowed=is_attribute_allowed,
    )
    def second_internal_oscillator(self: PhaseMicroStepperSimulator) -> float:
        """
        Return the value of the second internal oscillator.

        :return: the value of the second internal oscillator.
        """
        return self._second_internal_oscillator

    @second_internal_oscillator.write  # type: ignore[no-redef]
    def second_internal_oscillator(
        self: PhaseMicroStepperSimulator, value: float
    ) -> None:
        """
        Set the value of the second internal oscillator.

        :param value: the second internal oscillator
        """
        self._second_internal_oscillator = value
        self.update_buffer()

    @attribute(
        dtype="float",
        label="temperature",
        fisallowed=is_attribute_allowed,
    )
    def internal_temperature(self: PhaseMicroStepperSimulator) -> float:
        """
        Return the value of the internal temperature.

        :return: the value of the internal temperature.
        """
        return self._internal_temperature

    @internal_temperature.write  # type: ignore[no-redef]
    def internal_temperature(self: PhaseMicroStepperSimulator, value: float) -> None:
        """
        Set the value of the internal temperature.

        :param value: the internal temperature
        """
        self._internal_temperature = value
        self.update_buffer()

    def update_buffer(self: PhaseMicroStepperSimulator) -> None:
        """Create a new phasemicrostepper buffer string and update the server."""
        newbuff = (
            "EOG008 "
            + f"{self._first_internal_oscillator}, "
            + f"{self._second_internal_oscillator}, "
            + f"{self._internal_temperature}"
        )
        self._simserver.update(newbuff + self._eol)

    def _generate_random_data(self: PhaseMicroStepperSimulator) -> None:
        """Generate random noise to add to attribute value."""
        while self._randomise:
            noise = -0.05 + 0.1 * random.random()
            self._first_internal_oscillator = self._default_oscillator1 + noise / 100
            self._second_internal_oscillator = self._default_oscillator2 + noise / 100
            self._internal_temperature = self._default_temperature + noise
            self.update_buffer()
            time.sleep(1.0)


# ----------
# Run server
# ----------


def main(*args: str, **kwargs: str) -> int:
    """
    Entry point for module.

    :param args: positional arguments
    :param kwargs: named arguments
    :return: exit code
    """
    return PhaseMicroStepperSimulator.run_server(args=args or None, **kwargs)


if __name__ == "__main__":
    main()
