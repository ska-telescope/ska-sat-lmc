# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module implements the SATPhaseMicroStepper device."""

from __future__ import annotations  # allow forward references in type hints

import logging
import sys
from typing import Any, cast

import tango
from ska_control_model import CommunicationStatus, HealthState, ResultCode
from ska_tango_base.base import SKABaseDevice
from ska_tango_base.commands import DeviceInitCommand, FastCommand
from tango.server import attribute, command, device_property

from ska_sat_lmc.clocks.phasemicrostepper.phasemicrostepper_driver import (
    PhaseMicroStepperDriver,
)
from ska_sat_lmc.clocks.phasemicrostepper.phasemicrostepper_health_model import (
    PhaseMicroStepperHealthModel,
)

DevVarLongStringArrayType = tuple[list[ResultCode], list[str | None]]

__all__ = ["SatPhaseMicroStepper", "main"]


# pylint: disable=too-many-public-methods
class SatPhaseMicroStepper(SKABaseDevice[PhaseMicroStepperDriver]):
    """An implementation of a PhaseMicroStepper Tango device for SatLmc."""

    # -----------------
    # Device Properties
    # -----------------
    PhaseHost = device_property(
        dtype=str, mandatory=True, doc="The phasemicrostepper host"
    )
    UdpPort = device_property(
        dtype=int, mandatory=True, doc="The phasemicrostepper UDP port"
    )
    TcpPort = device_property(
        dtype=int, mandatory=True, doc="The phasemicrostepper TCP port"
    )
    UpdateRate = device_property(dtype=float, default_value=5.0)

    # ---------------
    # Initialisation
    # ---------------
    def __init__(
        self: SatPhaseMicroStepper,
        *args: Any,
        **kwargs: Any,
    ) -> None:
        """
        Initialise a new instance.

        :param args: positional arguments.
        :param kwargs: keyword arguments.
        """
        # This __init__ method is created for type-hinting purposes only.
        # Tango devices are not supposed to have __init__ methods,
        # And they have a strange __new__ method,
        # that calls __init__ when you least expect it.
        # So don't put anything executable in here
        # (other than the super() call).
        self._health_state: HealthState
        self._health_model: PhaseMicroStepperHealthModel
        self._component_manager = PhaseMicroStepperDriver
        self._version = sys.modules["ska_sat_lmc"].__version__
        self._attribute_info: dict[str, Any]

        super().__init__(*args, **kwargs)

    def init_device(self: SatPhaseMicroStepper) -> None:
        """Initialise the device."""
        self._attribute_info = {
            "serial_number": None,
            "first_internal_oscillator": None,
            "second_internal_oscillator": None,
            "internal_temperature": None,
        }
        try:
            super().init_device()

            device_name = f'{str(self.__class__).rsplit(".", maxsplit=1)[-1][0:-2]}'
            version = f"{device_name} Software Version: {self._version}"
            props = (
                f"Initialised {device_name} UDP on: {self.PhaseHost}:{self.UdpPort}"
                f"\nTCP on: {self.PhaseHost}:{self.TcpPort}"
                f"\nand update rate: {self.UpdateRate}"
            )
            version_info = f"{self.__class__.__name__}, {self._build_state}"
            self.logger.info("\n%s\n%s\n%s", version_info, version, props)
            self.logger.info("Device initialisation complete")

        # pylint: disable=broad-exception-caught
        except Exception as ex:
            self.logger.error(repr(ex))

    def _init_state_model(self: SatPhaseMicroStepper) -> None:
        """Initialise the state model."""
        super()._init_state_model()
        self._health_state = HealthState.UNKNOWN
        self._health_model = PhaseMicroStepperHealthModel(
            self._component_state_changed, self.logger
        )
        for attr_name, value in self._attribute_info.items():
            self.set_change_event(attr_name, True, False)
            self.set_archive_event(attr_name, True, False)

        self.set_change_event("healthState", True, False)
        self.set_archive_event("healthState", True, False)

    def create_component_manager(
        self: SatPhaseMicroStepper,
    ) -> PhaseMicroStepperDriver:
        """
        Create and return a component manager for this device.

        :return: a component manager for this device.
        """
        return PhaseMicroStepperDriver(
            self.logger,
            self.PhaseHost,
            self.UdpPort,
            self.TcpPort,
            self._communication_state_changed,
            self._component_state_changed,
            self.UpdateRate,
        )

    def init_command_objects(self: SatPhaseMicroStepper) -> None:
        """Initialise the command handlers for commands supported by this device."""
        super().init_command_objects()
        for command_name, command_object in [
            ("SetOffsetFrequency", self.SetOffsetFrequencyCommand),
            ("GetOffsetFrequency", self.GetOffsetFrequencyCommand),
            ("AdvanceAllPhase", self.AdvanceAllPhaseCommand),
            ("Advance10M", self.Advance10MCommand),
            ("AdvancePPS", self.AdvancePPSCommand),
            ("SyncPPS", self.SyncPPSCommand),
            ("SetIPaddress", self.SetIPAddressCommand),
            ("SetUDPaddress", self.SetUDPAddressCommand),
        ]:
            self.register_command_object(
                command_name,
                command_object(self.component_manager, self.logger),
            )

    class InitCommand(DeviceInitCommand):
        """Implement the device initialisation for the PhaseMicroStepper device."""

        def do(
            self: SatPhaseMicroStepper.InitCommand, *args: Any, **kwargs: Any
        ) -> tuple[ResultCode, str]:
            """
            Initialise the attributes and properties.

            :param args: additional positional arguments; unused here
            :param kwargs: additional keyword arguments; unused here

            :return: a resultcode, message tuple
            """
            assert (
                not args and not kwargs
            ), f"do method has unexpected arguments {args}, {kwargs}"
            self._device._health_state = HealthState.UNKNOWN

            return (ResultCode.OK, "Init command completed OK")

    # ----------
    # Callbacks
    # ----------
    def _communication_state_changed(
        self: SatPhaseMicroStepper,
        communication_state: CommunicationStatus,
    ) -> None:
        """
        Handle change in communications status between component manager and component.

        This is a callback hook, called by the component manager when
        the communications status changes. It is implemented here to
        drive the op_state.

        :param communication_state: the status of communications between
            the component manager and its component.
        """
        action_map = {
            CommunicationStatus.DISABLED: "component_disconnected",
            CommunicationStatus.NOT_ESTABLISHED: "component_disconnected",
            CommunicationStatus.ESTABLISHED: "component_on",
        }

        action = action_map[communication_state]
        if action is not None:
            self.op_state_model.perform_action(action)

        self._health_model.update_state(
            communicating=(communication_state == CommunicationStatus.ESTABLISHED)
        )

    # pylint: disable=arguments-differ
    def _component_state_changed(  # type: ignore[override]
        self: SatPhaseMicroStepper,
        **kwargs: Any,
    ) -> None:
        """
        Handle change in the state of the component.

        This is a callback hook, called by the component manager when
        the state of the component changes.

        :param kwargs: state change parameters.
        """
        if "fault" in kwargs:
            fault = kwargs.pop("fault")
            super()._component_state_changed(fault=fault)
            self._health_model.update_state(fault=fault)

        if "health" in kwargs:
            health = kwargs.pop("health")
            if self._health_state != health:
                self._health_state = cast(HealthState, health)
                self.push_change_event("healthState", health)
                self.push_archive_event("healthState", health)

        for attr_name, value in kwargs.items():
            self._attribute_info[attr_name] = value
            self.push_change_event(attr_name, value)
            self.push_archive_event(attr_name, value)

        if self.dev_state() == tango.DevState.ALARM:
            self._health_model.update_state(fault=True)

    # ----------
    # Attributes
    # ----------
    def is_attribute_allowed(
        self: SatPhaseMicroStepper, attr_req_type: tango.AttReqType
    ) -> bool:
        """
        Protect attribute access before being updated otherwise it reports alarm.

        :param attr_req_type: tango attribute type READ/WRITE

        :return: True if the attribute can be read else False
        """
        rc = self.get_state() in [
            tango.DevState.ON,
            tango.DevState.ALARM,
        ]
        return rc

    @attribute(
        dtype=int,
        label="serial number",
        doc="Returns the serial number of the EOG.",
        fisallowed=is_attribute_allowed,
    )
    def serial_number(self: SatPhaseMicroStepper) -> int:
        """
        Report the serial number of the device.

        :return: the serial number
        """
        return self._attribute_info["serial_number"]

    @attribute(
        dtype=float,
        label="first internal oscillator",
        max_alarm=24.95,
        min_alarm=0.1,
        fisallowed=is_attribute_allowed,
    )
    def first_internal_oscillator(self: SatPhaseMicroStepper) -> float:
        """
        Report the status of the first internal oscillator.

        :return: oscillator 1 value (fixed point 6 decimal digits)
        """
        return self._attribute_info["first_internal_oscillator"]

    @attribute(
        dtype=float,
        label="second internal oscillator",
        max_value=25,
        min_value=0,
        max_alarm=24.95,
        min_alarm=0.1,
        fisallowed=is_attribute_allowed,
    )
    def second_internal_oscillator(self: SatPhaseMicroStepper) -> float:
        """
        Report the status of the second internal oscillator.

        :return: oscillator 2 value (fixed point 6 decimal digits)
        """
        return self._attribute_info["second_internal_oscillator"]

    @attribute(
        dtype=float,
        label="internal temperature",
        unit="DegC",
        standard_unit="DegC",
        max_value=52.0,
        min_value=0.0,
        max_alarm=50.0,
        min_alarm=0.1,
        fisallowed=is_attribute_allowed,
    )
    def internal_temperature(self: SatPhaseMicroStepper) -> float:
        """
        Report the internal temperature of the device.

        :return: temperature in celcius (fixed point 2 decimal digits)
        """
        return self._attribute_info["internal_temperature"]

    # --------
    # Commands
    # --------
    class SetOffsetFrequencyCommand(FastCommand):
        """Class for handling the SetOffsetFrequency(argin) command."""

        def __init__(
            self: SatPhaseMicroStepper.SetOffsetFrequencyCommand,
            component_manager: PhaseMicroStepperDriver,
            logger: logging.Logger | None = None,
        ) -> None:
            """
            Initialise a new Command instance.

            :param component_manager: a component manager instance
            :param logger: a logger for this command to use.
            """
            self._component_manager = component_manager
            super().__init__(logger)

        def do(
            self: SatPhaseMicroStepper.SetOffsetFrequencyCommand,
            *args: Any,
            **kwargs: Any,
        ) -> tuple[ResultCode, str]:
            """
            Implement SetOffsetFrequency command functionality.

            :param args: positional args, the offset frequency is [0]
            :param kwargs: unspecified keyword arguments. This should be
                empty and is provided for typehinting purposes only.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            """
            offset = int(args[0])
            msg = self._component_manager.set_offset_frequency(offset)
            return (ResultCode.OK, msg)

    @command(dtype_in="DevLong", dtype_out="DevVarLongStringArray")
    def SetOffsetFrequency(
        self: SatPhaseMicroStepper, offset: int
    ) -> DevVarLongStringArrayType:
        """
        Set the offset frequency.

        Set the offset frequency of OUT signals respect to the frequency
        of 100MHz IN signal, in units of 1e-20. 7 Resolution is better
        than 3e-20. The acceptable range is from -1000000000000 to
        1000000000000

        :param offset: the offset frequency

        :return: the command + the converted offset frequency
        """
        handler = self.get_command_object("SetOffsetFrequency")
        (return_code, message) = handler(offset)
        return ([return_code], [message])

    class GetOffsetFrequencyCommand(FastCommand):
        """Class for handling the GetOffsetFrequency() command."""

        def __init__(
            self: SatPhaseMicroStepper.GetOffsetFrequencyCommand,
            component_manager: PhaseMicroStepperDriver,
            logger: logging.Logger | None = None,
        ) -> None:
            """
            Initialise a new Command instance.

            :param component_manager: a component manager instance
            :param logger: a logger for this command to use.
            """
            self._component_manager = component_manager
            super().__init__(logger)

        def do(
            self: SatPhaseMicroStepper.GetOffsetFrequencyCommand,
            *args: Any,
            **kwargs: Any,
        ) -> str:
            """
            Implement GetOffsetFrequency() command functionality.

            :param args: positional arguments. This should be empty and
                is provided for typehinting purposes only.
            :param kwargs: unspecified keyword arguments. This should be
                empty and is provided for typehinting purposes only.

            :return: the offset frequency
            """
            return self._component_manager.get_offset_frequency()

    @command(dtype_out="str")
    def GetOffsetFrequency(self: SatPhaseMicroStepper) -> str:
        """
        Get the offset frequency, in units of 1e-20.

        :return: the offset frequency send in the last
            setOffsetFrequencu command.
        """
        handler = self.get_command_object("GetOffsetFrequency")
        return handler()

    class AdvanceAllPhaseCommand(FastCommand):
        """Class for handling the AdvanceAllPhase command."""

        def __init__(
            self: SatPhaseMicroStepper.AdvanceAllPhaseCommand,
            component_manager: PhaseMicroStepperDriver,
            logger: logging.Logger | None = None,
        ) -> None:
            """
            Initialise a new Command instance.

            :param component_manager: a component manager instance
            :param logger: a logger for this command to use.
            """
            self._component_manager = component_manager
            super().__init__(logger)

        def do(
            self: SatPhaseMicroStepper.AdvanceAllPhaseCommand,
            *args: Any,
            **kwargs: Any,
        ) -> tuple[ResultCode, str]:
            """
            Implement AdvanceAllPhase command functionality.

            :param args: positional arguments.
            :param kwargs: unspecified keyword arguments. This should be
                empty and is provided for typehinting purposes only.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            """
            advance = int(args[0])
            msg = self._component_manager.advance_phase(advance)
            return (ResultCode.OK, msg)

    @command(dtype_in="int", dtype_out="DevVarLongStringArray")
    def AdvanceAllPhase(
        self: SatPhaseMicroStepper, argin: int
    ) -> DevVarLongStringArrayType:
        """
        Advance the output phase of all OUT signals, in units of 1e-15s.

        The resolution is about 1e-13. Acceptable range is from -5000000
        to +5000000.

        :param argin: the advance in units of 1e-15s

        :return: A tuple containing a return code and a string message
            indicating status. The message is for information purpose
            only.
        """
        handler = self.get_command_object("AdvanceAllPhase")
        (return_code, message) = handler(argin)
        return ([return_code], [message])

    def is_AdvanceAllPhase_allowed(self: SatPhaseMicroStepper) -> bool:
        """
        Check if command AdvanceAllPhase is allowed in the current device state.

        :return: ``True`` if the command is allowed
        """
        return self.get_state() == tango.DevState.ON

    class Advance10MCommand(FastCommand):
        """Class for handling the Advance10M command."""

        def __init__(
            self: SatPhaseMicroStepper.Advance10MCommand,
            component_manager: PhaseMicroStepperDriver,
            logger: logging.Logger | None = None,
        ) -> None:
            """
            Initialise a new Command instance.

            :param component_manager: a component manager instance
            :param logger: a logger for this command to use.
            """
            self._component_manager = component_manager
            super().__init__(logger)

        def do(
            self: SatPhaseMicroStepper.Advance10MCommand,
            *args: Any,
            **kwargs: Any,
        ) -> tuple[ResultCode, str]:
            """
            Implement Advance10M command functionality.

            :param args: positional arguments. [0] the advance in units of 10ns
            :param kwargs: unspecified keyword arguments. This should be
                empty and is provided for typehinting purposes only.
            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            """
            advance = int(args[0])
            msg = self._component_manager.advance_10megahertz(advance)
            return (ResultCode.OK, msg)

    @command(dtype_in="int", dtype_out="DevVarLongStringArray")
    def Advance10M(self: SatPhaseMicroStepper, argin: int) -> DevVarLongStringArrayType:
        """
        Advance the output phase of 10MHz OUT signals, in units of 10ns.

        Acceptable range is from 0 to +9.

        :param argin: the phase advance in units of 10ns

        :return: A tuple containing a return code and a string message
            indicating status. The message is for information purpose
            only.
        """
        handler = self.get_command_object("Advance10M")
        (return_code, message) = handler(argin)
        return ([return_code], [message])

    def is_Advance10M_allowed(self: SatPhaseMicroStepper) -> bool:
        """
        Check if command Advance10M is allowed in the current device state.

        :return: ``True`` if the command is allowed
        """
        return self.get_state() == tango.DevState.ON

    class AdvancePPSCommand(FastCommand):
        """Class for handling the AdvancePPS command."""

        def __init__(
            self: SatPhaseMicroStepper.AdvancePPSCommand,
            component_manager: PhaseMicroStepperDriver,
            logger: logging.Logger | None = None,
        ) -> None:
            """
            Initialise a new Command instance.

            :param component_manager: a component manager instance
            :param logger: a logger for this command to use.
            """
            self._component_manager = component_manager
            super().__init__(logger)

        def do(
            self: SatPhaseMicroStepper.AdvancePPSCommand,
            *args: Any,
            **kwargs: Any,
        ) -> tuple[ResultCode, str]:
            """
            Implement AdvancePPS command functionality.

            :param args: positional arguments. [0] the advance in units
                  of 10ns
            :param kwargs: unspecified keyword arguments. This should be
                empty and is provided for typehinting purposes only.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            """
            advance = int(args[0])
            msg = self._component_manager.advance_pps(advance)
            return (ResultCode.OK, msg)

    @command(dtype_in="int", dtype_out="DevVarLongStringArray")
    def AdvancePPS(self: SatPhaseMicroStepper, argin: int) -> DevVarLongStringArrayType:
        """
        Advance the phase of PPS OUT signals in units of 10ns.

        The acceptable range is limited from -99000000 to
        30000000.AdvancePPS.

        :param argin: the advance in units of 10ns

        :return: A tuple containing a return code and a string message
            indicating status. The message is for information purpose
            only.
        """
        handler = self.get_command_object("AdvancePPS")
        (return_code, message) = handler(argin)
        return ([return_code], [message])

    def is_AdvancePPS_allowed(self: SatPhaseMicroStepper) -> bool:
        """
        Check if command `AdvancePPS` is allowed in the current device state.

        :return: ``True`` if the command is allowed
        """
        return self.get_state() == tango.DevState.ON

    class SyncPPSCommand(FastCommand):
        """Class for handling the SyncPPS(argin) command."""

        def __init__(
            self: SatPhaseMicroStepper.SyncPPSCommand,
            component_manager: PhaseMicroStepperDriver,
            logger: logging.Logger | None = None,
        ) -> None:
            """
            Initialise a new Command instance.

            :param component_manager: a component manager instance
            :param logger: a logger for this command to use.
            """
            self._component_manager = component_manager
            super().__init__(logger)

        def do(
            self: SatPhaseMicroStepper.SyncPPSCommand,
            *args: Any,
            **kwargs: Any,
        ) -> tuple[ResultCode, str]:
            """
            Implement SyncPPS command functionality.

            :param args: positional arguments. This should be empty and
                is provided for typehinting purposes only.
            :param kwargs: unspecified keyword arguments. This should be
                empty and is provided for typehinting purposes only.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            """
            rc, msg = self._component_manager.sync_pps()
            if rc == 1:
                return (ResultCode.FAILED, msg)
            return (ResultCode.OK, msg)

    @command(dtype_out="DevVarLongStringArray")
    def SyncPPS(self: SatPhaseMicroStepper) -> DevVarLongStringArrayType:
        """
        External sync of the 1PPS outputs.

        :return: A tuple containing a return code and a string message
            indicating status. The message is for information purpose
            only.
        """
        handler = self.get_command_object("SyncPPS")
        (return_code, message) = handler()
        return ([return_code], [message])

    def is_SyncPPS_allowed(self: SatPhaseMicroStepper) -> bool:
        """
        Check if command `SyncPPS` is allowed in the current device state.

        :return: ``True`` if the command is allowed
        """
        return self.get_state() == tango.DevState.ON

    class SetIPAddressCommand(FastCommand):
        """Class for handling the SetIPAddress command."""

        def __init__(
            self: SatPhaseMicroStepper.SetIPAddressCommand,
            component_manager: PhaseMicroStepperDriver,
            logger: logging.Logger | None = None,
        ) -> None:
            """
            Initialise a new Command instance.

            :param component_manager: a component manager instance
            :param logger: a logger for this command to use.
            """
            self._component_manager = component_manager
            super().__init__(logger)

        def do(
            self: SatPhaseMicroStepper.SetIPAddressCommand,
            *args: Any,
            **kwargs: Any,
        ) -> tuple[ResultCode, str]:
            """
            Implement SetIPAddress command functionality.

            :param args: positional arguments. [0] the new IP address
            :param kwargs: unspecified keyword arguments. This should be
                empty and is provided for typehinting purposes only.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            """
            address = str(args[0])
            msg = self._component_manager.set_ip_address(address)
            return (ResultCode.OK, msg)

    @command(dtype_in="str", dtype_out="DevVarLongStringArray")
    def SetIPaddress(
        self: SatPhaseMicroStepper, argin: str
    ) -> DevVarLongStringArrayType:
        """
        Set the IP address of the EOG and closes the TCP connection.

        :param argin: the new IP address

        :return: A tuple containing a return code and a string message
            indicating status. The message is for information purpose
            only.
        """
        handler = self.get_command_object("SetIPaddress")
        (return_code, message) = handler(argin)
        return ([return_code], [message])

    def is_SetIPaddress_allowed(self: SatPhaseMicroStepper) -> bool:
        """
        Check if command `SetIPaddress` is allowed in the current device state.

        :return: ``True`` if the command is allowed
        """
        return self.get_state() == tango.DevState.ON

    class SetUDPAddressCommand(FastCommand):
        """Class for handling the SetUDPAddress command."""

        def __init__(
            self: SatPhaseMicroStepper.SetUDPAddressCommand,
            component_manager: PhaseMicroStepperDriver,
            logger: logging.Logger | None = None,
        ) -> None:
            """
            Initialise a new Command instance.

            :param component_manager: a component manager instance
            :param logger: a logger for this command to use.
            """
            self._component_manager = component_manager
            super().__init__(logger)

        def do(
            self: SatPhaseMicroStepper.SetUDPAddressCommand,
            *args: Any,
            **kwargs: Any,
        ) -> tuple[ResultCode, str]:
            """
            Implement SetUDPAddress command functionality.

            :param args: positional arguments. [0] the new UDP address
            :param kwargs: unspecified keyword arguments. This should be
                empty and is provided for typehinting purposes only.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            """
            address = str(args[0])
            msg = self._component_manager.set_udp_address(address)
            return (ResultCode.OK, msg)

    @command(dtype_in="str", dtype_out="DevVarLongStringArray")
    def SetUDPaddress(
        self: SatPhaseMicroStepper, argin: str
    ) -> DevVarLongStringArrayType:
        """
        Set the UDP address destination.

        :param argin: the str

        :return: A tuple containing a return code and a string message
            indicating status. The message is for information purpose
            only.
        """
        handler = self.get_command_object("SetUDPaddress")
        (return_code, message) = handler(argin)
        return ([return_code], [message])

    def is_SetUDPaddress_allowed(self: SatPhaseMicroStepper) -> bool:
        """
        Check if command `SetUDPaddress` is allowed in the current device state.

        :return: ``True`` if the command is allowed
        """
        return self.get_state() == tango.DevState.ON


# ----------
# Run server
# ----------


def main(*args: str, **kwargs: str) -> int:
    """
    Entry point for module.

    :param args: positional arguments
    :param kwargs: named arguments

    :return: exit code
    """
    return SatPhaseMicroStepper.run_server(args=args or None, **kwargs)


if __name__ == "__main__":
    main()
