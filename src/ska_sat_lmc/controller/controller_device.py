# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module contains the ska_sat_lmc Controller device prototype."""

from __future__ import annotations  # allow forward references in type hints

import sys
from typing import Any

from ska_control_model import CommunicationStatus, HealthState, PowerState
from ska_tango_base.base import SKABaseDevice
from tango.server import attribute, device_property

from ska_sat_lmc.component import SatComponentManager
from ska_sat_lmc.controller.controller_component_manager import (
    ControllerComponentManager,
)
from ska_sat_lmc.controller.controller_health_model import ControllerHealthModel

__all__ = ["SatController", "main"]


class SatController(SKABaseDevice[SatComponentManager]):
    """An implementation of a controller Tango device for SatLMC."""

    # Device Properties
    # -----------------
    SatClockTrl = device_property(dtype="DevString", default_value=None)
    SatFrqTrl = device_property(dtype="DevString", default_value=None)
    SatUtcTrl = device_property(dtype="DevString", default_value=None)

    # ---------------
    # Initialisation
    # ---------------
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        """
        Initialise this device object.

        :param args: positional args to the init
        :param kwargs: keyword args to the init
        """
        # We aren't supposed to define initialisation methods for Tango
        # devices; we are only supposed to define an `init_device` method. But
        # we insist on doing so here, just so that we can define some
        # attributes, thereby stopping the linters from complaining about
        # "attribute-defined-outside-init" etc. We still need to make sure that
        # `init_device` re-initialises any values defined in here.
        super().__init__(*args, **kwargs)
        self._health_state: HealthState
        self._health_model: ControllerHealthModel
        self._communication_state: CommunicationStatus | None
        self.component_manager: ControllerComponentManager  # type: ignore[assignment]

    def init_device(self: SatController) -> None:
        """
        Initialise the device.

        :raises Exception: When device initialisation fails
        """
        try:
            super().init_device()
            self._health_state = HealthState.UNKNOWN
            self._communication_state = None

            self._version_id = sys.modules["ska_sat_lmc"].__version__
            device_name = f'{str(self.__class__).rsplit(".", maxsplit=1)[-1][0:-2]}'
            self._version = f"{device_name} Software Version: {self._version_id}"
            properties = (
                f"Initialised {device_name} device with properties:\n"
                f"\tSatClockTrl: {self.SatClockTrl}\n"
                f"\tSatFrqTrl: {self.SatFrqTrl}\n"
                f"\tSatUtcTrl: {self.SatUtcTrl}\n"
            )
            version_info = f"{self.__class__.__name__}, {self._build_state}"
            self.logger.info("\n%s\n%s\n%s", version_info, self._version, properties)
            self.logger.info("Init device complete")
        except Exception as ex:
            self.logger.error(f"Initialise failed: Incomplete server: {repr(ex)}")
            raise

    def _init_state_model(self: SatController) -> None:
        """Initialise the state model."""
        super()._init_state_model()
        self._health_state = HealthState.UNKNOWN  # InitCommand.do() does this too late.
        self._health_model = ControllerHealthModel(
            self._component_state_callback,
            [self.SatClockTrl, self.SatFrqTrl, self.SatUtcTrl],
            self.logger,
        )
        self.set_change_event("healthState", True, False)
        self.set_archive_event("healthState", True, False)

    def create_component_manager(
        self: SatController,
    ) -> ControllerComponentManager:
        """
        Create and return a component manager for this device.

        :return: a component manager for this device.
        """
        return ControllerComponentManager(
            self.SatClockTrl,
            self.SatFrqTrl,
            self.SatUtcTrl,
            self.logger,
            self._communication_state_callback,
            self._component_state_callback,
        )

    # ----------
    # Callbacks
    # ----------
    def _communication_state_callback(
        self: SatController,
        communication_state: CommunicationStatus,
    ) -> None:
        """
        Handle change in communications status between component manager and component.

        This is a callback hook, called by the component manager when
        the communications status changes. It is implemented here to
        drive the op_state.

        :param communication_state: the status of communications
            between the component manager and its component.
        """
        action_map = {
            CommunicationStatus.DISABLED: "component_disconnected",
            CommunicationStatus.NOT_ESTABLISHED: "component_disconnected",
            CommunicationStatus.ESTABLISHED: "component_on",
        }

        action = action_map[communication_state]
        if action is not None:
            self.op_state_model.perform_action(action)

        self._health_model.update_state(
            communicating=communication_state == CommunicationStatus.ESTABLISHED
        )

    def _component_state_callback(
        self: SatController,
        fault: bool | None = None,
        power: PowerState | None = None,
        health: HealthState | None = None,
        trl: str | None = None,
    ) -> None:
        """
        Call this method whenever the state changes.

        Responsible for updating the tango side of things i.e. making
        sure the attribute is up to date, and events are pushed.

        :param fault: whether the component has faulted or not
        :param power: unused in this implementation
        :param health: An optional parameter with the new health state of the device.
        :param trl: The TRL of the device.
        """
        self.logger.debug(
            f"component state changed for {trl if trl is not None else 'self'}"
            f" with health {health} and fault {fault}"
        )
        if health is not None:
            if trl is None:
                self._health_changed(health)
            else:
                self._health_model.health_changed(trl, health)

        if fault is not None:
            if fault:
                self.op_state_model.perform_action("component_fault")

            self._health_model.update_state(fault=fault)

    def _health_changed(self: SatController, health: HealthState) -> None:
        """
        Handle change in this device's health state.

        This is a callback hook, called whenever the HealthModel's
        evaluated health state changes. It is responsible for updating
        the tango side of things i.e. making sure the attribute is up to
        date, and events are pushed.

        :param health: the new health value
        """
        if health is not None:
            if self._health_state != health:
                self._health_state = health
                self.push_change_event("healthState", health)

    # ----------
    # Attributes
    # ----------

    @attribute(dtype="str")
    def buildState(self: SatController) -> str:
        """
        Read the Build State of the device.

        :return: the build state of the device
        """
        return self._version

    @attribute(dtype="str")
    def versionId(self: SatController) -> str:
        """
        Read the Version Id of the device.

        :return: the version id of the device
        """
        return self._version_id

    @attribute(dtype=HealthState)
    def utcHealth(self: SatController) -> HealthState | None:
        """
        Read the health of utc subdevice.

        :return: health of utc subdevice
        """
        return self.component_manager.get_health(self.SatUtcTrl)

    @attribute(dtype=HealthState)
    def clockHealth(self: SatController) -> HealthState | None:
        """
        Read the health of clock subdevice.

        :return: health of clock subdevice
        """
        return self.component_manager.get_health(self.SatClockTrl)

    @attribute(dtype=HealthState)
    def frqHealth(self: SatController) -> HealthState | None:
        """
        Read the health of frq subdevice.

        :return: health of frq subdevice
        """
        return self.component_manager.get_health(self.SatFrqTrl)

    @attribute(dtype="DevString")
    def healthReport(self: SatController) -> str:
        """
        Get the health report.

        :return: the health report.
        """
        return self._health_model.health_report


# ----------
# Run server
# ----------


def main(*args: str, **kwargs: str) -> int:
    """
    Entry point for module.

    :param args: positional arguments
    :param kwargs: named arguments

    :return: exit code
    """
    return SatController.run_server(args=args or None, **kwargs)


if __name__ == "__main__":
    main()
