# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""An implementation of a health model for a controller."""

from __future__ import annotations

import logging
from typing import Any, Callable, Sequence

from ska_control_model import HealthState

from ska_sat_lmc.health import HealthModel

__all__ = ["ControllerHealthModel"]


class ControllerHealthModel(HealthModel):
    """A health model for a controller."""

    def __init__(
        self: ControllerHealthModel,
        component_state_changed: Callable[[Any], None],
        trls: Sequence[str],
        logger: logging.Logger,
    ) -> None:
        """
        Initialise a new instance.

        :param component_state_changed: callback to be called whenever
            there is a change to this health model's evaluated health
            state.
        :param trls: sat device tango resource locators
        :param logger: logging
        """
        self._trls = []
        super().__init__(component_state_changed, logger)
        for trl in trls:
            if trl:
                self._trls.append(trl)
                self._state.update({trl: HealthState.UNKNOWN})
        self._logger = logger

    def health_changed(
        self: ControllerHealthModel,
        trl: str,
        health: HealthState | None,
    ) -> None:
        """
        Handle a change in sat health.

        :param trl: the TRL of the sat subdevice whose health has changed
        :param health: the health state of the subdevice, or
            None if the admin mode indicates that its health
            should not be rolled up.
        """
        if trl in self._state and self._state[trl] != health:
            self._logger.info(
                f"health change for {trl} to "
                f"{HealthState(health).name if health is not None else health}"
            )
            self._state[trl] = health
            self.update_health()

    def evaluate_health(
        self: ControllerHealthModel,
    ) -> tuple[HealthState, str]:
        """
        Compute overall health of the controller.

        The overall health is based on the fault and communication
        status of the controller overall, together with the health of
        the subservient devices that it manages.

        This implementation simply sets the health of the controller to
        the health of its least healthy component.

        :return: an overall health of the controller
        """
        self._logger.info("Evaluating health")
        controller_health, report = super().evaluate_health()
        if controller_health != HealthState.OK:
            return controller_health, report

        report = ""
        overall_health = HealthState.OK
        for health in [
            HealthState.UNKNOWN,
            HealthState.DEGRADED,
            HealthState.FAILED,
        ]:
            for trl in self._trls:
                ctrl_health = self._state[trl]
                if health == ctrl_health:
                    report += f"{trl} reports health {ctrl_health.name}, "
                    overall_health = health
        if report:
            return overall_health, report[:-2]
        return HealthState.OK, "Health is OK"
