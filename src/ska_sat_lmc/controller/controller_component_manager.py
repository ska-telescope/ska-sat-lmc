# -*- coding: utf-8 -*-
#
# This file is part of the SKA SAT.LMC project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module implements component management for the SAT.LMC controller."""

from __future__ import annotations

import functools
import logging
import threading
from typing import Callable

from ska_control_model import CommunicationStatus, HealthState

from ska_sat_lmc.component import DeviceComponentManager, SatComponentManager

__all__ = ["ControllerComponentManager"]


class ControllerComponentManager(SatComponentManager):
    """A component manager for an SAT LMC controller."""

    # pylint: disable=too-many-arguments
    def __init__(
        self: ControllerComponentManager,
        clock_trl: str | None,
        frq_trl: str | None,
        utc_trl: str | None,
        logger: logging.Logger,
        communication_state_callback: Callable[[CommunicationStatus], None],
        component_state_callback: Callable[..., None],
    ) -> None:
        """
        Initialise a new instance.

        :param clock_trl: the TRL of the sat clock subsystem
        :param frq_trl: the TRL of the sat frq subsystem
        :param utc_trl: the TRL of the sat utc subsystem
        :param logger: the logger to be used by this object.
        :param communication_state_callback: callback to be
            called when the state of the communications channel between
            the component manager and its component changes
        :param component_state_callback: callback to be called
            when state changes
        """
        self._communication_state_callback = communication_state_callback
        self._component_state_callback = component_state_callback
        self.__communication_lock = threading.Lock()
        self._logger = logger
        self._sub_devices: dict[str, DeviceComponentManager] = {}

        self._device_communication_states: dict[str, CommunicationStatus] = {}
        if clock_trl:
            self._device_communication_states[clock_trl] = CommunicationStatus.DISABLED
            self._sub_devices[clock_trl] = DeviceComponentManager(
                clock_trl,
                logger,
                functools.partial(self._device_communication_state_changed, clock_trl),
                functools.partial(component_state_callback, trl=clock_trl),
            )
        if frq_trl:
            self._device_communication_states[frq_trl] = CommunicationStatus.DISABLED
            self._sub_devices[frq_trl] = DeviceComponentManager(
                frq_trl,
                logger,
                functools.partial(self._device_communication_state_changed, frq_trl),
                functools.partial(component_state_callback, trl=frq_trl),
            )
        if utc_trl:
            self._device_communication_states[utc_trl] = CommunicationStatus.DISABLED
            self._sub_devices[utc_trl] = DeviceComponentManager(
                utc_trl,
                logger,
                functools.partial(self._device_communication_state_changed, utc_trl),
                functools.partial(component_state_callback, trl=utc_trl),
            )
        super().__init__(
            logger,
            communication_state_callback,
            component_state_callback,
            fault=None,
        )

    def start_communicating(self: ControllerComponentManager) -> None:
        """Establish communication with its components."""
        if self.communication_state == CommunicationStatus.ESTABLISHED:
            return
        if self.communication_state == CommunicationStatus.DISABLED:
            self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)

        if not self._device_communication_states:
            self._update_communication_state(CommunicationStatus.ESTABLISHED)
        else:
            for proxy in self._sub_devices.values():
                proxy.start_communicating()

    def stop_communicating(self: ControllerComponentManager) -> None:
        """Break off communication with its components."""
        if self.communication_state == CommunicationStatus.DISABLED:
            return
        self._update_communication_state(CommunicationStatus.DISABLED)
        self._update_component_state(fault=None)

        for proxy in self._sub_devices.values():
            proxy.stop_communicating()

    def _device_communication_state_changed(
        self: ControllerComponentManager,
        trl: str,
        communication_state: CommunicationStatus,
    ) -> None:
        """
        Handle communication changes.

        :param trl: TRL of changed device
        :param communication_state: new status
        """
        if trl not in self._device_communication_states:
            self._logger.warning(
                f"Received a communication status changed event for device {trl} "
                "which is not managed by this controller. "
                "Probably it was released just a moment ago. "
                "The event will be discarded."
            )
            return

        self._device_communication_states[trl] = communication_state
        if self.communication_state == CommunicationStatus.DISABLED:
            return
        self._evaluate_communication_state()

    def _evaluate_communication_state(
        self: ControllerComponentManager,
    ) -> None:
        # Many callback threads could be hitting this method at the same time, so it's
        # possible (likely) that the GIL will suspend a thread between checking if it
        # need to update, and actually updating. This leads to callbacks appearing out
        # of order, which breaks tests. Therefore we need to serialise access.
        with self.__communication_lock:
            if (
                CommunicationStatus.DISABLED
                in self._device_communication_states.values()
            ):
                self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)
            elif (
                CommunicationStatus.NOT_ESTABLISHED
                in self._device_communication_states.values()
            ):
                self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)
            else:
                self._update_communication_state(CommunicationStatus.ESTABLISHED)
                self._update_component_state(fault=False)

    def _health_changed(
        self: ControllerComponentManager,
        trl: str,
        health: HealthState | None,
    ) -> None:
        """
        Handle a change in the health of a sat subsystem.

        :param trl: the TRL of the subsystem.
        :param health: the new health state of the subsystem, or None if
            the health could not be determined.
        """
        if self._component_state_callback is not None:
            self._component_state_callback(trl=trl, health=health)

    def get_health(self: ControllerComponentManager, trl: str) -> HealthState | None:
        """
        Return the health of subdevice.

        :param trl: the TRL of the subsystem.

        :return: health of subsystem given by TRL
        """
        if trl in self._sub_devices:
            if (
                self._device_communication_states[trl]
                == CommunicationStatus.ESTABLISHED
            ):
                return self._sub_devices[trl].health
            return HealthState.UNKNOWN
        return None
