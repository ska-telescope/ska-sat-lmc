[tool.poetry]
name = "ska-sat-lmc"
version = "0.4.0"
description = "SAT LMC Monitoring and Control"
authors = ["MCCS Team"]
license = "BSD-Clause-3"
documentation = "https://developer.skatelescope.org/projects/ska-sat-lmc"

[[tool.poetry.source]]
name = 'ska-nexus'
url = 'https://artefact.skao.int/repository/pypi-internal/simple'
[[tool.poetry.source]]
name = "PyPI-public"
url = 'https://pypi.org/simple'

[tool.poetry.dependencies]
python = "~3.10"
backoff = "^1.11.1"
future = "^0.18.3"
jsonschema = "4.17.3"
importlib_resources = "^6.1.0"
numpy = "^1.23.0"
pysnmp = "^4.4.12"
pyasn1 = "0.4.8"
pytango = "^9.5.1"
pyyaml = "^6.0.1"
requests = "2.31.0"
ska-control-model = { version = "^1.2.0", extras = ["health_rollup"] }
ska-tango-base = "1.3.1"
ska-tango-testing = "0.6.1"
pysnmp-lextudio = "^5.0.27"
ska-ser-snmp = "^0.4.2"
types-setuptools = "^75.1.0.20240917"
click = "^8.1.7"
pathspec = "^0.12.1"
typing-extensions = "^4.12.2"

[tool.poetry.dev-dependencies]
black = "^24.2.0"
coverage = "^5.5"
darglint = "^1.8.1"
flake8 = "^6.0.0"
flake8-formatter-junit-xml = "^0.0.6"
flake8-builtins = "^2.1.0"
flake8-docstrings = "^1.7.0"
flake8-fixme = "^1.1.1"
flake8-use-fstring = "^1.4"
flake8-rst-docstrings = "^0.3.0"
isort = "^5.10.1"
mock = "^4.0.3"
mypy = "^1.10.0"
pep8-naming = "^0.13.3"
pre-commit = "^2.14.0"
pylint = "^2.17.2"
pylint-junit = "^0.3.2"
pytest = "^6.2"
pytest-bdd = "^6.1.1"
pytest-cov = "^4.1.0"
pytest-forked = "^1.6.0"
pytest-json-report = "^1.5.0"
pytest-mock = "^3.11.1"
pytest-repeat = "^0.9.1"
pytest-timeout = "^2.1.0"
pytest-xdist = "~1.34.0"
pytest-report = "^0.2.1"
snmpsim-lextudio = "^1.0.1"
types-requests = "^2.28.11.2"
types-pyyaml = "^6.0.12"

[tool.poetry.group.docs.dependencies]
attrs = "^23.2.0"
sphinx = "^6.2.1"
sphinx-autodoc-typehints = "^1.23.0"
ska-ser-sphinx-theme = "^0.1.2"
sphinxcontrib-plantuml = "^0.25"
ska-control-model = { version = "^1.2.0", extras = ["health_rollup"] }
ska-ser-snmp = "^0.4.2"
backoff = "^1.11.1"
jsonschema = "4.17.3"
importlib_resources = "^6.1.0"
pysnmp = "^4.4.12"
pyyaml = "^6.0"
typing-extensions = "^4.6.3"
referencing = "^0.35.1"

[tool.poetry.scripts]
SatController = "ska_sat_lmc.controller.controller_device:main"
SatUtc = "ska_sat_lmc.utc.utc_device:main"
SatWhiteRabbit = "ska_sat_lmc.whiterabbit.wr_device:main"
WhiteRabbitSimulator = "ska_sat_lmc.whiterabbit.simulator:main"

[build-system]
requires = ["poetry-core>=1.2.1"]
build-backend = "poetry.core.masonry.api"
